# 光维FHP2A04光功率计
## APP+Bootloader 

## 需要完成
* ### ~~串口升级 `（ Ymoode ）`~~

* ### ~~蓝牙串口升级 `（Ymodem）`~~

* ### ~~电池标志  `(ADC电池电压)`~~

* ### ~~功耗降低 ` ( 降低主频，关闭无效IO) `~~

* ### ~~一拖七测试 ` ( 测量线性 ) `~~

* ### ~~协议兼容 `（ 先完成HEX，继续完善字符串部分 ）`~~

* ### 选配蓝牙

* ### ~~波长可以进行配置（比如980nm/1270nm/1577nm/1650nm等）~~

* ### TWIN模式可配置功能

* ### 具备红光源功能软硬件可配置

* ### 增加支持功率偏差自校准和恢复出厂设置功能（组合按键0.05dB步进）

* ### ~~增加开机波长记忆功能~~

* ### 具备参考值可以清零功能

* ### 全面测试


# FHP2A04 的程序记录
> ### 2020年11月19日 
> #### ` 初次创建，转存已建立工程的代码 `
***
> ### 2020年11月23日
> #### `修复蓝牙的数据丢包的BUG，采用补包的方法，只能说暂时可以解决这个问题。`
***
> ### 2020年11月24日
> #### `编写hex协议部分，以及测试了twin的模式。`
***
> ### 2020年11月25日
> #### `编写协议，对协议的串口处理进行了优化.感觉协议没有校验的话有点问题 `
***
> ### 2020年11月26日
> #### `完成Hex的协议，还没有进行测试。尝试兼容字符串的协议`
***
> ### 2020年11月27日
> #### `兼容协议完成，现在还有设置蓝牙名称的协议需要增加`
***
> ### 2020年11月30日
> #### `蓝牙协议完成，但是有些丢包的问题`
***
> ### 2020年12月1日
> #### `测试功耗，正常运行的时候是10mA,开启蓝牙会到45mA,开灯会增加3mA左右的功耗`
***
> ### 2020年12月2日
> #### `解决蓝牙丢包问题，是由于主频降低之后导致波特率115200会偶现丢包现象，降低串口波特率之后正常`
***
> ### 2020年12月15日
> #### `完成蓝牙的Ymodem功能，一拖七以及电池问题需要解决`
> #### `解决一拖七以及矫正整偏错误的问题`
***
> ### 2020年12月21日
> #### `完善电池电量部分的代码，等待测试数据`
***
> ### 2020年12月23日
> #### `完善蓝牙Ymodem 现在可以在手机断开 或者 设备断电的情况下进行 断点续传`
***
> ### 2021年1月3日
> #### `优化 Twin 模式 ，ADC采集电压值电量优化`
***
> ### 2021年1月31日
> #### `修复时间字符串溢出风险`
***
> ### 2021年2月4日
> #### `增加EEPROM的等待时间，发现可能有风险`
***
> ### 2021年2月5日
> #### `验证风险点，确实增加等待时间没有用。更改EEPROM的地址之后保存和读取没有了问题。可能是扇区的分割导致。`
> #### `此版本可以是最终版本 计划版本为V1.0`

***
> ### 2021年12月16日
> #### `通过逻辑分析仪查找问题，发现时在AT24C256进行数据按页写入的时候可能不满足一页，此时由于换页的等待时间可能不满足，产生丢包。现在针对修正数据以及整偏值增加了校验操作，确保数据不会在中途变换。`

---

> ### 2022年3月11日
> #### `增加模式：波长可配置（980nm 1270nm 1577nm 1650nm）`
>
> #### `增加关机波长保存功能`

---

