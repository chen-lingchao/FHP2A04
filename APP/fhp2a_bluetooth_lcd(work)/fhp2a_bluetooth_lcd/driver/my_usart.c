/**
* \file my_usart.c
* \brief 串口
* \author  mirlee
* \version 0.1
* \date
*/
#include "my_usart.h"
#include "my_data.h"
#include "my_thread.h"
#include "my_agreement.h"

//u8 s_rx_buf[4];
volatile u8 g_msg_buf[50];
volatile u8 g_rx_buf[50];
volatile Msg_Info_Type g_msg_rt_info;//数据发送接收标志

/**
 * @function:my_uart1_init
 * @description:串口1 初始化
 * @param {*}
 * @return {*}
 * @author: lzc
 */
// UART1 蓝牙串口
bool baud_result = false;
static void my_uart1_init(void)
{
    LL_USART_InitTypeDef USART_InitStruct = {0};
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    LL_USART_DeInit(USART1);// 用于清除之前的初始化状态
    memset(&USART_InitStruct, 0, sizeof(USART_InitStruct));
    memset(&GPIO_InitStruct, 0, sizeof(GPIO_InitStruct));
    /* Peripheral clock enable */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_USART1);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

    /**USART1 GPIO Configuration
    PA9   ------> USART1_TX
    PA10   ------> USART1_RX
    */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_9 | LL_GPIO_PIN_10;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    NVIC_SetPriority(USART1_IRQn, 3);
    NVIC_EnableIRQ(USART1_IRQn);
    USART_InitStruct.BaudRate = 115200;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
    LL_USART_Init(USART1, &USART_InitStruct);
    LL_USART_DisableIT_CTS(USART1);
    LL_USART_ConfigAsyncMode(USART1);
    LL_USART_EnableIT_RXNE(USART1);

    LL_USART_Enable(USART1);
}

/**
 * @function:change_bt_uart1_9600_baud
 * @brief:改变串口1的波特率到9600
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void change_bt_uart1_9600_baud(void)
{
    // 先关闭串口
    LL_USART_Disable(USART1);
    // 重新初始化
    LL_USART_InitTypeDef USART_InitStruct = {0};
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    LL_USART_DeInit(USART1);// 用于清除之前的初始化状态
    memset(&USART_InitStruct, 0, sizeof(USART_InitStruct));
    memset(&GPIO_InitStruct, 0, sizeof(GPIO_InitStruct));
    /* Peripheral clock enable */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_USART1);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

    /**USART1 GPIO Configuration
    PA9   ------> USART1_TX
    PA10   ------> USART1_RX
    */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_9 | LL_GPIO_PIN_10;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    NVIC_SetPriority(USART1_IRQn, 3);
    NVIC_EnableIRQ(USART1_IRQn);

    USART_InitStruct.BaudRate = 9600;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
    LL_USART_Init(USART1, &USART_InitStruct);
    LL_USART_DisableIT_CTS(USART1);
    LL_USART_ConfigAsyncMode(USART1);
    LL_USART_EnableIT_RXNE(USART1);

    LL_USART_Enable(USART1);

}

/**
 * @function: change_bt_baud
 * @brief: 改变蓝牙的波特率
 * @param {*}
 * @return {true 更改成功 ，false 更改失败}
 * @author: lzc
 */
bool change_bt_baud(const char *buf)
{
    char num, timeout = 0;
    // 先进行判断
    if (buf != 0x00)
    {
        // 切换到9600波特率
        putstr("AT+BAUD=9600\r\n");
        // 等待切换完毕
        rt_thread_mdelay(10);
        // 同时切换STM32串口的波特率
        change_bt_uart1_9600_baud();
        // 等待切换完毕
        rt_thread_mdelay(10);
        // 置位标志位
        record_baud_set(RT_TRUE);
        // 等待切换完毕
        rt_thread_mdelay(10);
        // 发送测试指令如果回复正常返回true
        //测试
        putstr("AT\r\n");
        while (!g_msg_rt_info.end)
        {
            num++;
            if (num >= 10)
            {
                num = 0;
                timeout ++;
                putstr("AT\r\n");
            }
            rt_thread_mdelay(10);
            // 否则超时则返回错误
            if (timeout >= 100)
            {
                return false;
            }
        }
        g_msg_rt_info.num = 0;
        g_msg_rt_info.end = 0;
        rt_thread_mdelay(10);
        return true;
    }
    return true;
}

/**
 * @function:change_bt_uart1_115200_baud
 * @brief:改变串口1的波特率到115200
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void change_bt_uart1_115200_baud(void)
{
    // 先关闭串口
    LL_USART_Disable(USART1);
    // 重新初始化
    LL_USART_InitTypeDef USART_InitStruct = {0};
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    LL_USART_DeInit(USART1);// 用于清除之前的初始化状态
    memset(&USART_InitStruct, 0, sizeof(USART_InitStruct));
    memset(&GPIO_InitStruct, 0, sizeof(GPIO_InitStruct));
    /* Peripheral clock enable */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_USART1);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

    /**USART1 GPIO Configuration
    PA9   ------> USART1_TX
    PA10   ------> USART1_RX
    */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_9 | LL_GPIO_PIN_10;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    NVIC_SetPriority(USART1_IRQn, 3);
    NVIC_EnableIRQ(USART1_IRQn);

    USART_InitStruct.BaudRate = 115200;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
    LL_USART_Init(USART1, &USART_InitStruct);
    LL_USART_DisableIT_CTS(USART1);
    LL_USART_ConfigAsyncMode(USART1);
    LL_USART_EnableIT_RXNE(USART1);

    LL_USART_Enable(USART1);

}

/**
 * @function:my_uart1_init
 * @description:串口2 初始化
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static void my_uart2_init(void)
{
    LL_USART_InitTypeDef USART_InitStruct = {0};
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    LL_USART_DeInit(USART2);// 用于清除之前的初始化状态
    memset(&USART_InitStruct, 0, sizeof(USART_InitStruct));
    memset(&GPIO_InitStruct, 0, sizeof(GPIO_InitStruct));
    /* Peripheral clock enable */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART2);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

    /**USART1 GPIO Configuration
    PA2   ------> USART2_TX
    PA3   ------> USART2_RX
    */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_2 | LL_GPIO_PIN_3 ;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    NVIC_SetPriority(USART2_IRQn, 4);
    NVIC_EnableIRQ(USART2_IRQn);

    USART_InitStruct.BaudRate = 9600;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
    LL_USART_Init(USART2, &USART_InitStruct);
    LL_USART_DisableIT_CTS(USART2);
    LL_USART_ConfigAsyncMode(USART2);
    LL_USART_EnableIT_RXNE(USART2);

    LL_USART_Enable(USART2);
}

void my_usart_init(void)
{
    my_uart1_init();
    my_uart2_init();
    if (0)
    {

//
//    LL_USART_InitTypeDef USART_InitStruct = {0};
//    LL_USART_InitTypeDef USART_InitStruct_2 = {0};
//    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
//    memset(&USART_InitStruct, 0, sizeof(USART_InitStruct));
//    memset(&USART_InitStruct, 0, sizeof(USART_InitStruct_2));
//    memset(&GPIO_InitStruct, 0, sizeof(GPIO_InitStruct));
//    /* Peripheral clock enable */
//    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_USART1);
//    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART2);
//    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
//
//    /* Init with LL driver */
//    /* DMA controller clock enable */
//    //LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);
//
//    /* DMA interrupt init */
//    /* DMA1_Channel2_3_IRQn interrupt configuration */
//    //NVIC_SetPriority(DMA1_Channel2_3_IRQn, 3);
//    //NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
//
//    /**USART1 GPIO Configuration
//    PA9   ------> USART1_TX
//    PA10   ------> USART1_RX
//    PA2   ------> USART2_TX
//    PA3   ------> USART2_RX
//    */
//    GPIO_InitStruct.Pin = LL_GPIO_PIN_2 | LL_GPIO_PIN_3 | LL_GPIO_PIN_9 | LL_GPIO_PIN_10;
//    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
//    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
//    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
//    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
//    GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
//    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//
//
//    /* USART1 DMA Init */
//    /* USART1_RX Init */
//    //LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
//    //LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PRIORITY_LOW);
//    //LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MODE_NORMAL);
//    //LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PERIPH_NOINCREMENT);
//    //LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MEMORY_INCREMENT);
//    //LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PDATAALIGN_BYTE);
//    //LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MDATAALIGN_BYTE);
//    //LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_3, (u32)&g_msg_buf[0]);
//    //LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_3, (u32)USART1->RDR);
//    /* USART1_TX Init */
//    //LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_2, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
//    //LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PRIORITY_LOW);
//    //LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MODE_NORMAL);
//    //LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PERIPH_NOINCREMENT);
//    //LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MEMORY_INCREMENT);
//    //LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PDATAALIGN_BYTE);
//    //LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MDATAALIGN_BYTE);
//    /* USART1 interrupt Init */
//    NVIC_SetPriority(USART1_IRQn, 3);
//    NVIC_EnableIRQ(USART1_IRQn);
//    NVIC_SetPriority(USART2_IRQn, 4);
//    NVIC_EnableIRQ(USART2_IRQn);
//
//    USART_InitStruct.BaudRate = 115200;
//    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
//    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
//    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
//    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
//    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
//    USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
//    LL_USART_Init(USART1, &USART_InitStruct);
//    LL_USART_DisableIT_CTS(USART1);
//    LL_USART_ConfigAsyncMode(USART1);
//    LL_USART_EnableIT_RXNE(USART1);
//    //DMA
//    //LL_USART_EnableDMAReq_RX(USART1);
//    //LL_USART_EnableDMAReq_TX(USART1);
//
//    LL_USART_Enable(USART1);
//
//    //LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
//    //LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_3);
//
//    /**********USART2***********/
//    USART_InitStruct_2.BaudRate = 9600;
//    USART_InitStruct_2.DataWidth = LL_USART_DATAWIDTH_8B;
//    USART_InitStruct_2.StopBits = LL_USART_STOPBITS_1;
//    USART_InitStruct_2.Parity = LL_USART_PARITY_NONE;
//    USART_InitStruct_2.TransferDirection = LL_USART_DIRECTION_TX_RX;
//    USART_InitStruct_2.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
//    USART_InitStruct_2.OverSampling = LL_USART_OVERSAMPLING_16;
//
//    LL_USART_Init(USART2, &USART_InitStruct_2);
//    LL_USART_DisableIT_CTS(USART2);
//    LL_USART_ConfigAsyncMode(USART2);
//    LL_USART_EnableIT_RXNE(USART2);
//    //LL_USART_EnableDMAReq_RX(USART2);
//    //LL_USART_EnableDMAReq_TX(USART2);
//    LL_USART_Enable(USART2);


    }
}
 
// TODO: DMA+UART1 蓝牙会丢字节，随机
// 已解决： 通过补包,关键是波特率

// TODO:增加对串口溢出中断的处理
/**
 * @function: USART1_IRQHandler
 * @description: 串口1 的中断服务函数
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void USART1_IRQHandler(void)
{
    u8 temp = 0;
    rt_interrupt_enter();
    // 判断是否开启了溢出中断
    if (LL_USART_IsActiveFlag_ORE(USART1))
    {
        // 禁止溢出中断
        LL_USART_DisableIT_ERROR(USART1);
    }
    if (LL_USART_IsActiveFlag_RXNE(USART1))
    {
        temp = LL_USART_ReceiveData8(USART1);
        //if ((g_sys_flag.work.module.bt != 1))
        //{
        //    //初始化蓝牙,接收蓝牙返回信息
        //    if (g_msg_rt_info.dev == _msg_dev_bt)
        //    {
        //        if (g_msg_rt_info.end == 0)
        //        {
        //            g_msg_buf[g_msg_rt_info.num] = temp;
//
        //            if ((g_msg_buf[g_msg_rt_info.num] == '\n') || (g_msg_buf[g_msg_rt_info.num] == '\r'))
        //            {
        //                if (g_msg_rt_info.num == 0)
        //                {
        //                    goto USART1_THERE;
        //                }
        //                else
        //                {
        //                    g_msg_rt_info.end = 1;
        //                    g_msg_buf[g_msg_rt_info.num] = '\0';
        //                    my_thread_resume_msg();
        //                    goto USART1_THERE;
        //                }
        //            }
        //            g_msg_rt_info.num++;
        //            if (g_msg_rt_info.num >= 50)
        //            {
        //                g_msg_rt_info.end = 1;
        //                g_msg_buf[g_msg_rt_info.num - 1] = '\0';
        //            }
        //        }
        //    }
        //    else
        //        goto USART1_THERE;
        //}
        if (g_msg_rt_info.dev != _msg_dev_bt)
        {
            goto USART1_THERE;
        }
        else
        {
            if (g_msg_rt_info.end == 0)
            {
                g_msg_buf[g_msg_rt_info.num] = temp;
                if ((g_msg_buf[g_msg_rt_info.num] == '\n') || (g_msg_buf[g_msg_rt_info.num] == '\r'))
                {
                    if (g_msg_rt_info.num == 0)
                    {
                        goto USART1_THERE;
                    }
                    g_msg_rt_info.end = 1;
                    g_msg_buf[g_msg_rt_info.num] = '\0';
                    my_thread_resume_msg();
                }
                g_msg_rt_info.num++;
                if (g_msg_rt_info.num >= 50)
                {
                    g_msg_rt_info.num = 0;
                }
            }
            else
            {
                // 如果此处的状态是挂起状态，那么意味着虽然g_msg_rt_info.end 状态是 1
                // 但是线程状态是挂起，也就是永远无法清除标志，结果是蓝牙死机。
                if (Get_communicate_thread_State() == RT_THREAD_SUSPEND)
                {
                    // 此处防止蓝牙死机，原因是线程的处理，挂起之后没有唤醒
                    my_thread_resume_msg();
                }
            }
        }
    }
USART1_THERE:
    if (LL_USART_IsActiveFlag_ORE(USART1))
    {
        LL_USART_ClearFlag_ORE(USART1);
    }
    rt_interrupt_leave();
}

// TODO:增加对串口溢出中断的处理(TOFIX)
/**
 * @function: USART2_IRQHandler
 * @description: 串口2 的中断服务函数
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void USART2_IRQHandler(void)
{
    u8 temp = 0;
    rt_interrupt_enter();
    // 判断是否开启了溢出中断
    if (LL_USART_IsActiveFlag_ORE(USART2))
    {
        // 禁止溢出中断
        LL_USART_DisableIT_ERROR(USART2);
    }
    if (LL_USART_IsActiveFlag_RXNE(USART2))
    {
        temp = LL_USART_ReceiveData8(USART2);
        if (g_msg_rt_info.dev != _msg_dev_us)
        {
            goto USART2_THERE;
        }
        else
        {
            // 协议处理
            if (g_msg_rt_info.end == 0)
            {
                g_rx_buf[g_msg_rt_info.num] = temp;
                if ((g_rx_buf[g_msg_rt_info.num] == '\n') || (g_rx_buf[g_msg_rt_info.num] == '\r'))
                {
                    g_msg_rt_info.end = 1;
                    g_rx_buf[g_msg_rt_info.num] = '\0';
                    my_thread_resume_msg();
                    agreement_parse_change_type(_agr_str_type);
                    goto USART2_THERE;
                }
                // TODO: 串口协议处理
                // 数据接收时由于协议只会发 最多 9个有效字节 ，所以后面的就不接了，提高速度
                else if (g_rx_buf[0] == 0xaa && g_msg_rt_info.num >= 9) // hex 协议接收完成
                {
                    g_msg_rt_info.end = 1; g_msg_rt_info.num++;
                    g_rx_buf[g_msg_rt_info.num] = '\0';
                    my_thread_resume_msg();
                    agreement_parse_change_type(_agr_hex_type);
                    goto USART2_THERE;
                }
                /*  字符串的兼容  */
                //else if (g_rx_buf[0] != 0xaa)
                //{
                //    // 字符串协议
                //    g_msg_rt_info.end = 1; g_msg_rt_info.num++;
                //    g_rx_buf[g_msg_rt_info.num] = '\0';
                //    my_thread_resume_msg();
                //    goto USART2_THERE;
                //}
                g_msg_rt_info.num++;

                // 错误的串口数据
                // 不是hex线程
                if (g_rx_buf[0] != 0xaa)
                {
                    // 也不是字符串协议
                    // 解析失败 清除数据
                    if (g_rx_buf[0] != '*' && g_rx_buf[0] !=  'g'
                            && g_rx_buf[0] !=  's' && g_rx_buf[0] !=  'c'
                            && g_rx_buf[0] !=  'k')
                    {
                        memset((void *)g_rx_buf, 0, sizeof((const char *)g_rx_buf));
                        g_msg_rt_info.num = 0;
                    }
                }
                if (g_msg_rt_info.num >= 50)
                {
                    g_msg_rt_info.num = 0;
                }
            }

            else
            {
                /* code */
            }

        }
    }
USART2_THERE:
    if (LL_USART_IsActiveFlag_ORE(USART2))
    {
        LL_USART_ClearFlag_ORE(USART2);
    }
    rt_interrupt_leave();
}


void putchar_usart2(u8 ch)
{
    LL_USART_TransmitData8(USART2, ch);
    while (!LL_USART_IsActiveFlag_TC(USART2));
}

void putchar_usart1(u8 ch)
{
    LL_USART_TransmitData8(USART1, ch);
    while (!LL_USART_IsActiveFlag_TC(USART1));
}

void putstr(const char *str)
{
    u8 i = 0;
    switch (g_msg_rt_info.dev)
    {
    case _msg_dev_us://
        while (str[i] != '\0')
        {
            putchar_usart2(str[i]);
            i++;
        }
        break;
    case _msg_dev_bt://
        while (str[i] != '\0')
        {
            putchar_usart1(str[i]);
            i++;
        }
        break;
    }
}


void rt_hw_console_output(const char *str)
{
    putstr(str);
}


