/**
* \file my_lcd.c
* \brief lcd 引脚初始化,操作时序
* \author  mirlee
* \version 0.1
* \date
*/
#include "my_lcd.h"
#include "my_data.h"


/*lcd驱动芯片地址,有时地址会有所变化*/
static const u8 s_lcd_seg[28] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                                 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                                 20, 21, 22, 23, 24, 25, 26, 31
                                };

/*以 1 为例
* P1 0   1D 0
  4E 0   1C 1
  4G 0   1B 1
  4F 0   1A 0
  0x0    0x6
    低     高
*/
//                                0    1    2    3    4    5    6    7     8    9
static const u8 s_lcd_dis_0_9[] = {0xfa, 0x60, 0xd6, 0xf4, 0x6c, 0xbc, 0xbe, 0xe0, 0xfe, 0xfc};


static  Lcd_Mem_Typedef s_lcd_dis_mem;  //lcd 显存
static  u8 s_lcd_dis_mem_temp[LCD_MEM_BUF_NUM];//lcd临时缓存，用于和缓存比较是否有内容变化
/**
 * @brief lcd 断码屏引脚初始化
 * @param
 * @return
 */
static void lcd_gpio_init(void)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    memset(&GPIO_InitStruct, 0, sizeof(GPIO_InitStruct));
    /*开时钟*/
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
    /*重置GPIO*/
    /**************************LCD的IO初始化**********************************/
    // DATA_HT -- GPIO15  LCD_WR -- GPIO14 LCD_CS -- GPIO13
    GPIO_InitStruct.Pin = LL_GPIO_PIN_13 | LL_GPIO_PIN_14 | LL_GPIO_PIN_15;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    //背光   LDC_BL -- PB10
    GPIO_InitStruct.Pin = LL_GPIO_PIN_10;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}
/**
 * @brief 发送头，3位，先发高位
 * @param
 * @return
 */
static void send_head_ht1621(u8 head)
{
    for (int i = 0; i < 3; i++)
    {
        LCD_WR_PIN_LOW;
        rt_hw_us_delay(LCD_CLK_US);
        if (head & 0x04)
        {
            LCD_DATA_PIN_HIGH;
        }
        else
        {
            LCD_DATA_PIN_LOW;
        }
        rt_hw_us_delay(LCD_CLK_US);
        head = head << 1;
        LCD_WR_PIN_HIGH;
    }
}
/**
 * @brief 发送命令，9位，先发高位
 * @param
 * @return
 */
static void send_command_ht1621(u8 command)
{
    for (int i = 0; i < 9; i++)
    {
        LCD_WR_PIN_LOW;
        rt_hw_us_delay(LCD_CLK_US);
        if (command & 0x80)
        {
            LCD_DATA_PIN_HIGH;
        }
        else
        {
            LCD_DATA_PIN_LOW;
        }
        rt_hw_us_delay(LCD_CLK_US);
        command = command << 1;
        LCD_WR_PIN_HIGH;
    }
}
/**
 * @brief 发送地址，六位，先发高位
 * @param
 * @return
 */
static void send_address_ht1621(u8 addr)
{
    for (int i = 0; i < 6; i++)
    {
        LCD_WR_PIN_LOW;
        rt_hw_us_delay(LCD_CLK_US);
        if (addr & 0x20)
        {
            LCD_DATA_PIN_HIGH;
        }
        else
        {
            LCD_DATA_PIN_LOW;
        }
        rt_hw_us_delay(LCD_CLK_US);
        addr = addr << 1;
        LCD_WR_PIN_HIGH;
    }
}
/**
 * @brief 发送数据，四位，先发低位
 * @param
 * @return
 */
static void send_data_ht1621(u8 data)
{
    for (int i = 0; i < 4; i++)
    {
        LCD_WR_PIN_LOW;
        rt_hw_us_delay(LCD_CLK_US);
        if (data & 0x01)
        {
            LCD_DATA_PIN_HIGH;
        }
        else
        {
            LCD_DATA_PIN_LOW;
        }
        rt_hw_us_delay(LCD_CLK_US);
        data = data >> 1;
        LCD_WR_PIN_HIGH;
    }
}


/**
 * @brief ht1621初始化
 * @param
 * @return
 */
static void ht1621_init(void)
{
    LCD_CS_PIN_LOW;
    send_head_ht1621(HEAD_COMMAND_MODE);         //command mode
    send_command_ht1621(SYS_EN);
    send_command_ht1621(LCD_ON);
    send_command_ht1621(0x29);                  //系统时钟源晶振
    LCD_CS_PIN_HIGH;
    //清屏
    for (int i = 0; i < 28; i++)
    {
        LCD_CS_PIN_LOW;
        rt_hw_us_delay(LCD_CLK_US * 10);
        send_head_ht1621(HEAD_WRITE_MODE);       // write mode
        send_address_ht1621(s_lcd_seg[i]);
        send_data_ht1621(0);
        LCD_CS_PIN_HIGH;
        rt_hw_us_delay(LCD_CLK_US * 10);
    }
}



/**
 * @brief 显示波长
 * @param wavelength: 0:空,other:波长为四位整数，如1310,1490
 *         最高位只能为1或不显示,如: 850,1310,等
 *        nm: 显示nm标志
 * @return
 */
void lcd_display_wavelength(u32 wavelength, bool nm)
{
    u8 a = 0, b = 0, c = 0, d = 0;
    //清
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_3_4 = 0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_5_6 = 0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_7_8 = 0;
    a = wavelength / 1000;
    if (a == 1)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_3_4 = 0x01;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_3_4 = 0;
    }
    b = wavelength % 1000 / 100;
    if (b > 0 || a == 1)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_3_4 |= s_lcd_dis_0_9[b];
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_3_4 = 0;
    }
    c = wavelength % 1000 % 100 / 10;
    if (c > 0 || b > 0 || a == 1)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_5_6 |= s_lcd_dis_0_9[c];
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_5_6 &= ~0xfE;
    }
    d = wavelength % 1000 % 100 % 10;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_7_8 |= s_lcd_dis_0_9[d];
    //nm
    if (nm)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_7_8 |= 0x01;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_7_8 &= ~0x01;
    }
}
/**
 * @brief 显示usb
 * @param flag: false:空,true:显示
 * @return
 */
void lcd_display_usb(bool flag)
{
    if (flag)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 |= 0x10;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 &= ~0x10;
    }
}
/**
 * @brief 显示关机标志
 * @param flag: false:空,true:显示
 * @return
 */
void lcd_display_shutdown(bool flag)
{
    if (flag)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 |= 0x80;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 &= ~0x80;
    }
}
/**
 * @brief 显示LED标志
 * @param flag: false:空,true:显示
 * @return
 */
void lcd_display_led(bool flag)
{
    if (flag)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 |= 0x40;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 &= ~0x40;
    }
}
/**
 * @brief 显示buzz标志
 * @param flag: false:空,true:显示
 * @return
 */
void lcd_display_buzz(bool flag)
{
    if (flag)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 |= 0x20;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 &= ~0x20;
    }
}

/**
 * @brief 显示bluetooth标志
 * @param flag: false:空,true:显示
 * @return
 */
void lcd_display_bluetooth(bool flag)
{
    if (flag)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 |= 0x20;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 &= ~0x20;
    }
}
/**
 * @brief 显示wifi标志
 * @param flag: 1:z1, 2:z2, 3:z3
 * @return
 */
void lcd_display_wifi(u8 flag)
{
    //清
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 &= ~0x0e;
    switch (flag)
    {
    case 1:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 |= 0x02;
        break;
    case 2:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 |= 0x06;
        break;
    case 3:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 |= 0x0e;
        break;
    default:
        break;
    }
}
/**
 * @brief 显示红光标志
 * @param flag: false:空,true:显示
 * @return
 */
void lcd_display_vfl(bool flag)
{
    if (flag)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 |= 0x40;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 &= ~0x40;
    }
}
/**
 * @brief 显示电池
 * @param level: _bat_max:空,_bat_empty:0格,1格,2格,3格
 * @return
 */
void lcd_display_battery(u8 level)
{
    //清
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 &= ~0x0f;
    switch (level)
    {
    case _bat_max:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 &= ~0x0f;
        break;
    case _bat_empty:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 |= 0x08;
        break;
    case _bat_one:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 |= 0x0a;
        break;
    case _bat_two:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 |= 0x0b;
        break;
    case _bat_three:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_1_2 |= 0x0f;
        break;
    }
}

/**
 * @brief 显示计数，最多四位
 * @param count:最多四位无符号整数,如1234;为0时,清除不显示
 *           hz:显示HZ标志
 *         save:显示SAVE标志
 * @return
 */
void lcd_display_count(u32 count, bool hz, bool save)
{
    u8 a = 0, b = 0, c = 0, d = 0;
    a = count / 1000;
    //清
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_19_20 = 0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_21_22 = 0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_23_24 = 0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_25_26 = 0;
    if (count == 0)
        return;
    if (a > 0)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_19_20 |= s_lcd_dis_0_9[a];
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_19_20 &= ~0xfe;
    }
    b = count % 1000 / 100;
    if (b > 0 || a > 0)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_21_22 |= s_lcd_dis_0_9[b];
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_21_22 &= ~0xfe;
    }
    c = count % 1000 % 100 / 10;
    if (c > 0 || b > 0 || a > 0)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_23_24 |= s_lcd_dis_0_9[c];
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_23_24 &= ~0xfe;
    }
    d = count % 1000 % 100 % 10;
    if (d > 0 || c > 0 || b > 0 || a > 0)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_25_26 |= s_lcd_dis_0_9[d];
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_25_26 &= ~0xfe;
    }

    if (hz)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_25_26 |= 0x01;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_25_26 &= ~0x01;
    }
    if (save)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_19_20 |= 0x01;
    }
    else
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_19_20 &= ~0x01;
    }
}

/**
 * @brief 显示功率,两位整数,两位小数
 * @param unit: _unit_max:空,_unit_mw:mw,_unit_db:db,_unit_dbm:dbm
 *        status: _optical_max:空,_optical_high:HI,_optical_low:LO,_optical_normal:正常
 *              work: 0:空, 1:CAL, 2:REF, 3:TWIN, 4:SINGLE,
 * @return
 */
void lcd_display_power(float value, u8 unit, u8 status, u8 work)
{
    u8 a = 0, b = 0, c = 0, d = 0;
    int temp = 0;
    //清单位
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 = 0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 &= ~0x10;
    //清功率和左侧符号
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10  = 0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 &= ~0xff;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 &= ~0xff;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 &= ~0xff;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 &= ~0xf0;
    /*清负号*/
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 &= ~0x81;
    /*显示单位*/
    switch (unit)
    {
    case _unit_max:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 &= ~0xf0;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 &= ~0x01;
        break;
    case _unit_mw://mw
        if (value >= (float)0.01)
        {
            s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 |= 0x80;
        }
        else
        {
            value *= (float)1000.0;
            if (value >= (float)0.01)
            {
                //uw
                s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 |= 0x80;
            }
            else
            {
                value *= (float)1000.0;
                //nw
                s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 |= 0x40;
            }
        }
        break;
    case _unit_db://db
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 |= 0x10;
        break;
    case _unit_dbm://dbm
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 |= 0x30;
        break;
    }
    switch (status)
    {
    case _optical_high:
        /*功率显示区域*/
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10  &= ~0xf0;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 |= 0xe0;  //HI
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 |= 0x06;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 |= 0x06;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 &= ~0x0f;
        break;
    case _optical_low:
        /*功率显示区域*/
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10  &= ~0xf0;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 |= 0xa0;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 |= 0xa1;//LO
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 |= 0x0f;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 &= ~0x0f;
        break;
    case _optical_normal:
        /*显示示数*/
        temp = value * 100;
        if (temp >= 0) //负号
        {
            s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 &= ~0x01;
        }
        else
        {
            s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 |= 0x01;
            temp = 0 - temp;
        }
        a = temp / 1000; //十位
        if (a > 0)
        {
            s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10  |= (s_lcd_dis_0_9[a] & 0x0f) << 4;
            s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 |= (s_lcd_dis_0_9[a] & 0xf0) >> 4;
        }
        b = temp % 1000 / 100; //个位
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 |= (s_lcd_dis_0_9[b] & 0x0f) << 4;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 |= (s_lcd_dis_0_9[b] & 0xf0) >> 4;
        //小数点
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 |= 0x10;
        c = temp % 1000 % 100 / 10; //小数点后一位
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 |= (s_lcd_dis_0_9[c] & 0x0f) << 4;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 |= (s_lcd_dis_0_9[c] & 0xf0) >> 4;
        d = temp % 1000 % 100 % 10; //小数点后两位
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 |= (s_lcd_dis_0_9[d] & 0x0f) << 4;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 |= (s_lcd_dis_0_9[d] & 0xf0) >> 4;
        break;
    }
    switch (work)
    {
    case 1:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10 |= 0x08;
        break;
    case 2:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10 |= 0x04;
        break;
    case 3:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10 |= 0x02;
        break;
    case 4:
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10 |= 0x01;
        break;
    default:
        break;
    }
}

/**
 * @brief 显示功率和计数位置的横线
 * @param line1:功率位置,line2:计数位置
 * @return
 */
void lcd_display_line(bool line1, bool line2)
{
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_27_28 &= ~0x01;//清除负号
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10  &= ~0xf0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 = 0x0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 = 0x0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 = 0x0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 &= ~0x0f;
    if (line1)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10  |= 0x40;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 |= 0x40;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 |= 0x40;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 |= 0x40;
    }
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_19_20   = 0x0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_21_22   = 0x0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_23_24   = 0x0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_25_26   = 0x0;
    if (line2)
    {
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_19_20   = 0x04;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_21_22   = 0x04;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_23_24   = 0x04;
        s_lcd_dis_mem.lcd_mem_item.lcd_mem_25_26   = 0x04;
    }
}

void lcd_start_sign(void)
{
    my_lcd_clear();
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_9_10  |= 0xe0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 |= 0x0f;
    lcd_refresh_buf();
    rt_delay_ms(200);
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_11_12 |= 0xe0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 |= 0x0f;
    lcd_refresh_buf();
    rt_delay_ms(200);
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_13_14 |= 0xe0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 |= 0x0f;
    lcd_refresh_buf();
    rt_delay_ms(200);
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_15_16 |= 0xe0;
    s_lcd_dis_mem.lcd_mem_item.lcd_mem_17_18 |= 0x0f;
    lcd_refresh_buf();
    rt_delay_ms(200);
}

void lcd_back_light(bool flag)
{
    if (flag)
    {
        LL_GPIO_TogglePin(GPIOB, LL_GPIO_PIN_10);
    }
    else
    {
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_10);
    }
}


void my_lcd_clear(void)
{
    for (int i = 0; i < LCD_MEM_BUF_NUM; i++)
    {
        s_lcd_dis_mem.lcd_dis_mem[i] = 0;
    }
}
/**
 * @brief ht1621显存刷新
 * @param
 * @return
 */
void lcd_refresh_buf(void)
{
    rt_enter_critical();
    for (int i = 0; i < LCD_MEM_BUF_NUM; i++)
    {
        if (s_lcd_dis_mem.lcd_dis_mem[i] != s_lcd_dis_mem_temp[i])
        {
            s_lcd_dis_mem_temp[i] = s_lcd_dis_mem.lcd_dis_mem[i];
            LCD_CS_PIN_LOW;
            rt_hw_us_delay(LCD_CLK_US * 10);
            send_head_ht1621(HEAD_WRITE_MODE);
            send_address_ht1621(s_lcd_seg[i * 2]);
            send_data_ht1621(s_lcd_dis_mem.lcd_dis_mem[i] & 0x0f);
            LCD_CS_PIN_HIGH;
            rt_hw_us_delay(LCD_CLK_US * 10);

            LCD_CS_PIN_LOW;
            rt_hw_us_delay(LCD_CLK_US * 10);
            send_head_ht1621(HEAD_WRITE_MODE);
            send_address_ht1621(s_lcd_seg[i * 2 + 1]);
            send_data_ht1621((s_lcd_dis_mem.lcd_dis_mem[i] & 0xf0) >> 4);
            LCD_CS_PIN_HIGH;
            rt_hw_us_delay(LCD_CLK_US * 10);

        }
    }
    rt_exit_critical();
}


void my_lcd_init(void)
{
    lcd_gpio_init();
    ht1621_init();
}

