/**
* \file my_gpio.c
* \brief 初始化相关io
* \author  mirlee
* \version 0.1
* \date
*/
#include "my_gpio.h"
#include "my_lcd.h"
#include "my_data.h"
#include "my_adc.h"
#include "my_record.h"
#include "my_tim.h"
#include "my_display.h"

/***************开关机键***********************/
#define DEAL_MS       (u32)(20)    //函数执行周期
#define POWER_ON_TS   (u32)(200)    //上电到执行到此函数的时间
#define POWER_ON_TIME (u32)(1000)   //开机时间，从按键到屏幕亮

static u16 s_time_tick_power = 0;
static u8 s_flag_power = 0;

void my_gpio_init(void)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    memset(&GPIO_InitStruct, 0, sizeof(GPIO_InitStruct));
//  LL_EXTI_InitTypeDef EXTI_InitStruct = {0};
    /*开时钟*/
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF);
    /*重置GPIO*/
    //LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0|LL_GPIO_PIN_1|LL_GPIO_PIN_2|LL_GPIO_PIN_10);

    /**************************档位的IO初始化**********************************/
    // 0档 PB0  A3
    // A2 A1 A0 -- PA5 PA4 PC15
    //负电压 -3.3V_CT PA8
    GPIO_InitStruct.Pin = LL_GPIO_PIN_4 | LL_GPIO_PIN_5 | LL_GPIO_PIN_8;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = LL_GPIO_PIN_15;
    LL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    //放电
    GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /**************************按键的IO初始化*****************************/
    GPIO_InitStruct.Pin = LL_GPIO_PIN_8 | LL_GPIO_PIN_9;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = LL_GPIO_PIN_13;
    LL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = LL_GPIO_PIN_3 | LL_GPIO_PIN_4 | LL_GPIO_PIN_5;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    /**************************usb检测IO初始化*****************************/
    GPIO_InitStruct.Pin = LL_GPIO_PIN_15;//PA15
    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;//上拉,插入usb时是低电平
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    /**************************蜂鸣器IO初始化*****************************/
    GPIO_InitStruct.Pin = LL_GPIO_PIN_6;//PF6
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;//蜂鸣器高电平导通
    LL_GPIO_Init(GPIOF, &GPIO_InitStruct);
    /**************************蓝牙*****************************/
    //BUV_CT  PA11  高有效，电源
    GPIO_InitStruct.Pin = LL_GPIO_PIN_11;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_11);

    //CH_DONE PF7  充电完成标志
    GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOF, &GPIO_InitStruct);
    /**************************红光*****************************/
    //电压芯片:
    //LD3.3_CT   PB11        //使能红光源电源
    //LD_CT      PA12                //红光源
    GPIO_InitStruct.Pin = LL_GPIO_PIN_11;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = LL_GPIO_PIN_12;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    /**************************开机键*****************************/
    //KEYT  PA1
//  GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
//  GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
//  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
//  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
//  GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;
//  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    //ON  PC14
    GPIO_InitStruct.Pin = LL_GPIO_PIN_14;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;
    LL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    // VBT_CT  PB2
    // 电池电源检测
    GPIO_InitStruct.Pin = LL_GPIO_PIN_2;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    // 开启红光源的电源
    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_11);

    //PA8  频率检测引脚初始化
    /**/
//  LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTA, LL_SYSCFG_EXTI_LINE8);
//  LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_8, LL_GPIO_PULL_NO);
//  LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_8, LL_GPIO_MODE_INPUT);
//
//  EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_8;
//  EXTI_InitStruct.LineCommand = ENABLE;
//  EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
//  EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_RISING;
//  LL_EXTI_Init(&EXTI_InitStruct);
//  /* EXTI interrupt init*/
//  NVIC_SetPriority(EXTI4_15_IRQn, 0);
//  NVIC_EnableIRQ(EXTI4_15_IRQn);

}

void set_pin_channel(u8 channel)
{
    // 0档 PB0  A3
    // A2 A1 A0 -- PA5 PA4 PC15
    // 负电压 -3.3V_CT PA8
    switch (channel)
    {
    case 0:
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_8);   //负电压
        LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_0);   //0 channel
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5); //A2
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_4); //A1
        LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_15); //A0
        break;
    case 1:
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_8);   //负电压
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0);   //0 channel
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5);
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_4);
        LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_15);
        break;
    case 2:
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_8);   //负电压
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0);   //0 channel
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5);
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_4);
        LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_15);
        break;
    case 3:
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_8);   //负电压
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0);  //0 channel
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5);
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_4);
        LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_15);
        break;
    case 4:
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_8);   //负电压
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0);  //0 channel
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_5);
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_4);
        LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_15);
        break;
    case 5:
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_8);   //负电压
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0);  //0 channel
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_5);
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_4);
        LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_15);
        break;
    case 6:
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_8);   //负电压
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0);  //0 channel
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_5);
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_4);
        LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_15);
        break;
    case 7:
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_8);   //负电压
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0);  //0 channel
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_5);
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_4);
        LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_15);
        break;
    }
    /*放电*/
    LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_6);
    rt_thread_mdelay(40);
    LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_6);
}

/**
 * @brief 获取按键电平
 * @param
 * @return
 */
u16 scan_key_x_y(void)
{
    u8 key_x_num = 1; //x轴，开始时跳过开机键位置
    u8 key_press_num = 0; //按下的按键，从k2开始
    u16 CC = 0; //用于标记按键信息
    //KEY X1 PC13      X2 B9       X3 PB8
    //KEY Y1 PB5       Y2 PB4      Y3 PB3
    /*X轴提供高电平*/
    while (1)
    {
        switch (key_x_num)
        {
        case 1:
            LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_13);//KEYB0
            LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_9);//KEYB1
            LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_8);//KEYB2
            break;
        case 2:
            LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_13);//KEYB0
            LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_9);//KEYB1
            LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_8);//KEYB2
            break;
        case 3:
            LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_13);//KEYB0
            LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_9);//KEYB1
            LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_8);//KEYB2
            break;
        default:
            break;
        }
        for (int i = 0; i < 10; i++) {}
        /*read the state of Y1*/
        if (LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_5) == 1)
        {
            CC |= 0x1 << key_press_num;
        }
        key_press_num++;
        if (LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_4) == 1)
        {
            CC |= 0x1 << key_press_num;
        }
        key_press_num++;
        if (LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_3) == 1)
        {
            CC |= 0x1 << key_press_num;
        }
        key_x_num++;
        key_press_num++;
        if (key_x_num >= 4)
        {
            LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_13);//KEYB0
            LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_9);//KEYB1
            LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_8);//KEYB2
            return CC;
        }
    }
}

bool get_pin_usb(void)
{
    return !LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_15);
}

bool get_pin_ch_done(void)
{
    return !LL_GPIO_IsInputPinSet(GPIOF, LL_GPIO_PIN_7);
}

bool get_pin_power(void)
{

    return LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_1);
}

void set_pin_power(bool flag)
{
    if (flag == true)
    {
        LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_14);
    }
    else if (flag == false)
    {
        LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_14);
    }
}
void set_pin_bt(bool flag)
{
    if (flag == true)
    {
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_11);
    }
    else if (flag == false)
    {
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_11);
    }
}
void set_pin_buzz(bool flag)
{
    if (flag == true)
    {
        LL_GPIO_SetOutputPin(GPIOF, LL_GPIO_PIN_6);
    }
    else if (flag == false)
    {
        LL_GPIO_ResetOutputPin(GPIOF, LL_GPIO_PIN_6);
    }
}

//电池电压检测使能(三极管)
void set_pin_vbt(bool flag)
{
    if (flag == true)
    {
        LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_2);
    }
    else if (flag == false)
    {
        LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_2);
    }
}
//充电使能,VB_ACT
void set_pin_cbt(bool flag)
{
    if (flag == true)
    {
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_12);
    }
    else if (flag == false)
    {
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_12);
    }
}


void set_pin_vfl(bool flag)
{
    if (flag == true)
    {
        //LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_12);
        LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_12);
    }
    else if (flag == false)
    {
        //LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_12);
        LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_12);
    }
}

void buzz_on(int ms)
{
    if (g_sys_flag.work.module.buzz)
    {
        return;
    }
    set_pin_buzz(true);
    rt_thread_mdelay(ms);
    set_pin_buzz(false);
}

//参数：com 为采样的原始数值
//返回值：iData 经过一阶滤波后的采样值
unsigned int low_Filter( unsigned int com )
{
    static unsigned int iLastData;    //上一次值
    unsigned int iData;               //本次计算值
    float dPower = 0.88;               //滤波系数
    iData = ( com * dPower ) + ( 1 - dPower ) * iLastData; //计算
    iLastData = iData;                                     //存贮本次数据
    return iData;                                         //返回数据
}

/**
 * @brief 1.充电,150ms循环
 *        2.充电和检测电池电压,都需要使能相应引脚
 *        3.
 *
 * @param usb:usb状态, bat:电池状态
 * @return
 */
static u16 charge_tick;
static short bat_temp;
static u8 charge_dis;
static u8 charge_status;
static u8 charge_step;
static u8 usb_re;
static u8 bat_re;
static u8 bat_temp_Val = 0;
void scan_charge_battery(u8 *usb, u8 *bat)
{
    u16 vol_temp = 0;
    *usb = 0xf;
    *bat = 0xf;
    charge_tick++;
    switch (charge_step)
    {
    case 0:
        set_pin_vbt(true);
        if (get_pin_usb())
        {
            *usb = 1;
            //usb存在时,检测电池电压要关闭充电
            //set_pin_cbt(false);
        }
        else
        {
            charge_status = 0;
            *usb = 0;
        }
        charge_step = 1;
        charge_tick = 0;
        break;
    case 1://测量电池电压
        if (charge_tick >= 4)
        {
            vol_temp = get_vol_battery();
            vol_temp = low_Filter(vol_temp);
            set_pin_vbt(false);
            if (get_pin_usb())
            {
                *usb = 1;
                //旧版本的硬件需要进行充电
                //set_pin_cbt(true);//打开充电开关
            }
            if (vol_temp > 1665) //1.385
            {
                bat_temp = 4;
            }
            else if (vol_temp >= 1660) //1.332
            {
                bat_temp = 3;
            }
            else if (vol_temp >= 1605) //1.328
            {
                bat_temp = 2;
            }
            else if (vol_temp >= 1544) //1.324
            {
                bat_temp = 1;
            }
            else if (vol_temp >= 1500) //1.20V
            {
                bat_temp = 0;
            }
            else//关机
            {
                bat_temp = 0;
            }
            charge_step = 2;
            charge_tick = 0;
        }

        break;
    case 2://充电
        if (charge_tick >= 3)
        {
            if (get_pin_usb())
            {
                if (charge_status)
                {
                    //set_pin_cbt(false);
                    //充满之后
                    *bat = _bat_three;
                    charge_step = 0;
                    charge_tick = 0;
                    break;
                }
                if (bat_temp < 4) //未充满
                {
                    if (charge_dis >= _bat_three)
                    {
                        if (bat_temp > 0)
                        {
                            charge_dis = bat_temp - 1;
                        }
                        else
                        {
                            charge_dis = _bat_empty;
                        }
                    }
                    else
                    {
                        charge_dis++;
                    }
                    *bat = charge_dis;
                }
                else
                {
                    *bat = _bat_three;
                    charge_status = 1;//充满标记
                }
                if (get_pin_ch_done())
                {
                    *bat = _bat_three;
                    charge_status = 1;//充满标记
                }
                else
                {
                    bat_temp_Val ++;
                    *bat = bat_temp_Val;
                    if (bat_temp_Val == 3)
                    {
                        bat_temp_Val = 0;
                    }
                    charge_status = 0;//充满标记
                }
            }
            else
            {
                *bat = bat_temp >= 4 ? _bat_three : bat_temp;
            }
            charge_step = 0;
            charge_tick = 0;
        }
        break;
    }

    if (*usb == 0xf)
        *usb = usb_re;
    else
        usb_re = *usb;
    if (*bat == 0xf)
        *bat = bat_re;
    else
        bat_re = *bat;
    g_sys_flag.data.bat = (Bat_Enum) * bat;
}



/**
 * @brief 1.处理电源键，和自动关机时间
 *        2.如果上电时没有按power键，应该阻塞到系统执行前.
 *        3.
 *
 * @param
 * @return
 */
void sys_off_fun(void)
{
    /*断电*/
    buzz_on(200);
    record_off_time_set(AUTO_OFF_TIME);
    lcd_back_light(false);
    set_pin_power(false);
    g_sys_flag.work.mode = _mode_max;
}

void sys_start_fun(void)
{
    /*初始化自动关机时间*/
    refresh_second_tick();
    set_pin_power(true);
    buzz_on(200);
    lcd_start_sign();
    g_sys_flag.work.mode = _mode_single;
}

void scan_off_key(void)
{
    u32 keyt_tick = 0;
    switch (s_flag_power)
    {
    case 0://初次上电，检测开机
        if (get_vol_keyt() == true)
        {
            s_time_tick_power++;
            if (get_pin_usb() == true)
            {
                if (s_time_tick_power > (POWER_ON_TIME) / DEAL_MS)
                {
                    s_time_tick_power = 0;
                    //开机
                    s_flag_power = 1;
                    goto POWER_STATUS_1;
                }
            }
            else
            {
                if (s_time_tick_power > (POWER_ON_TIME - POWER_ON_TS) / DEAL_MS)
                {
                    s_time_tick_power = 0;
                    //开机
                    s_flag_power = 1;
                    goto POWER_STATUS_1;
                }
            }
        }
        else
        {
            //没有按下开机键
        }
        break;
    case 1://执行开机
POWER_STATUS_1:
        sys_start_fun();
        keyt_tick = 0;
        rt_thread_mdelay(100);
        while (1) //等待按键松开
        {
            keyt_tick++;
            if (keyt_tick > 5)
            {
                keyt_tick = 0;
                record_off_time_set(AUTO_OFF_FLAG);
                buzz_on(100);
            }
            if (get_vol_keyt() == false)
            {
                s_flag_power = 2;
                break;
            }
            rt_thread_mdelay(100);
        }
        break;
    case 2://已经开机，检测关机
        if (get_vol_keyt() == true)
        {
            s_time_tick_power++;
            if (s_time_tick_power > (POWER_ON_TIME) / DEAL_MS)
            {
                s_time_tick_power = 0;
                s_flag_power = 3;
                goto POWER_STATUS_3;
            }
        }
        break;
    case 3://执行关机
POWER_STATUS_3:
        // TODO:增加波长记忆功能（保存关机波长）
        record_PowerON_Wave_set(g_sys_flag.data.wave);

        sys_off_fun();
        rt_thread_mdelay(100);
        while (1)
        {
            if (get_vol_keyt() == false)
            {
                s_flag_power = 0;
                break;
            }
            rt_thread_mdelay(100);
        }
        break;
    }
}

/**
 * @brief 关机时间扫描
 * @param
 * @return
 */
void scan_off_time()
{
    u16 temp = 0;
    temp = record_off_time_get();
    if (temp == AUTO_OFF_FLAG)
    {
        g_auto_off_flag = false;
        return;
    }
    else
    {
        g_auto_off_flag = true;
    }
    if (g_sys_flag.work.mode == _mode_max)
        return;
    if ((get_second_tick() - get_second_tick_re()) > temp)
    {
        //关机时间到
        s_flag_power = 3;
    }
}

