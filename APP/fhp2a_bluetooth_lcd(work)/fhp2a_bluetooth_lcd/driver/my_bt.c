/*
 * @Description:
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-17 09:54:11
 * @LastEditors: lzc
 * @LastEditTime: 2021-02-05 14:00:14
 */
#include "my_bt.h"
#include "my_usart.h"
#include "my_gpio.h"
#include "my_record.h"
#include "my_data.h"
#include "my_usart.h"
#define BT_DEBUG 0
void my_bt_init(void)
{
    int num = 0;
    //打开供电
    // 清除标志位

    // DEBUG 如果设置为9600 则还原为115200
#if BT_DEBUG
    // 此处为还原波特率到115200
    record_bt_set(RT_FALSE);
    record_baud_set(RT_FALSE);
    //change_bt_uart1_9600_baud();
    change_bt_uart1_115200_baud();
#else
    // record_bt_set(RT_FALSE);
#endif
    if (!g_sys_flag.work.module.bt)
    {
        set_pin_bt(true);
        g_msg_rt_info.dev = _msg_dev_bt;
        // 如果波特率是9600
        if (record_baud_get() == true)
        {
            change_bt_uart1_9600_baud();
        }
        if (record_bt_get() != true)
        {
            //测试
            putstr("AT\r\n");
            while (!g_msg_rt_info.end)
            {
                num++;
                if (num >= 10)
                {
                    num = 0;
                    putstr("AT\r\n");
                }
                rt_thread_mdelay(10);
            }
            g_msg_rt_info.num = 0;
            g_msg_rt_info.end = 0;
            rt_thread_mdelay(10);

            //确认波特率
#if BT_DEBUG
            putstr("AT+BAUD=115200\r\n");
#else
            putstr("AT+BAUD\r\n");
#endif
            while (!g_msg_rt_info.end)
            {
                num++;
                if (num >= 10)
                {
                    num = 0;
#if BT_DEBUG
                    putstr("AT+BAUD=115200\r\n");
#else
                    putstr("AT+BAUD\r\n");
#endif
                }
                rt_thread_mdelay(10);
            }
#if BT_DEBUG
            record_baud_set(RT_FALSE);
#else
// 如果更改失败，则清除记录
            if (change_bt_baud((const char *)g_msg_buf) == false)
            {
                record_bt_set(RT_FALSE);
                record_baud_set(RT_FALSE);
            }
#endif

            g_msg_rt_info.num = 0;
            g_msg_rt_info.end = 0;
            rt_thread_mdelay(10);

            //改名称,可配置
            putstr("AT+NAME=FHP2A04\r\n");
            while (!g_msg_rt_info.end)
            {
                num++;
                if (num >= 10)
                {
                    num = 0;
                    putstr("AT+NAME=FHP2A04\r\n");
                }
                rt_thread_mdelay(10);
            }
            g_msg_rt_info.num = 0;
            g_msg_rt_info.end = 0;
            rt_thread_mdelay(10);

            //开启简易配对
            putstr("AT+SSP=1\r\n");
            while (!g_msg_rt_info.end)
            {
                num++;
                if (num >= 10)
                {
                    num = 0;
                    putstr("AT+SSP=1\r\n");
                }
                rt_thread_mdelay(10);
            }
            g_msg_rt_info.num = 0;
            g_msg_rt_info.end = 0;
            rt_thread_mdelay(10);

            //获取MAC地址
            putstr("AT+ADDR\r\n");
            while (!g_msg_rt_info.end)
            {
                num++;
                if (num >= 10)
                {
                    num = 0;
                    putstr("AT+ADDR\r\n");
                }
                rt_thread_mdelay(10);
            }
            record_bt_set_mac((const u8 *)g_msg_buf);
            g_msg_rt_info.num = 0;
            g_msg_rt_info.end = 0;
            rt_thread_mdelay(10);
            record_bt_set(true);
        }
    }
    else
    {
        set_pin_bt(false);
        g_msg_rt_info.dev = _msg_dev_us;
    }
    g_sys_flag.work.module.bt = !g_sys_flag.work.module.bt;
}

void my_bt_pin(void)
{

}

void my_bt_reset(void)
{

}
//简易配对
void my_bt_ssp(void)
{

}
//修改波特率
void my_bt_baud(void)
{

}

