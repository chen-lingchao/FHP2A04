/*
 * @Description: 
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2021-12-10 12:31:04
 * @LastEditors: lzc
 * @LastEditTime: 2022-03-10 15:29:04
 */
#ifndef MY_24CXX_H
#define MY_24CXX_H

#include "app_config.h"
#ifdef __cplusplus
 extern "C" {
#endif

/*预定义*/
#define AT24C01		127
#define AT24C02		255
#define AT24C04		511
#define AT24C08		1023
#define AT24C16		2047
#define AT24C32		4095
#define AT24C64	    8191
#define AT24C128	16383
#define AT24C256	32767  
#define AT24C512    12345
#define EE_TYPE AT24C256

#if ((EE_TYPE == AT24C128) ||(EE_TYPE == AT24C256))
#define EEP_PAGE_SIZE ((u16)64)
#elif (EE_TYPE == AT24C512)
#define EEP_PAGE_SIZE ((u16)128)
#endif




void eeprom_init(void);
u8 eeprom_read_byte(u16 addr);
void eeprom_write_byte(u16 addr, u8 data);
void eeprom_write_bytes(u16 addr, u8 data[], u16 num);
void eeprom_read_bytes(u16 addr, u8 data[], u16 num);
void eeprom_write_page_bytes(u16 addr, u8 *data, u16 num);

#ifdef __cplusplus
}
#endif

#endif




