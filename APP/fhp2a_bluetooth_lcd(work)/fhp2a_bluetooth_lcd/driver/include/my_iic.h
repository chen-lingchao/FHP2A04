#ifndef _MY_IIC_H
#define _MY_IIC_H
#include "app_config.h"
#ifdef __cplusplus
 extern "C" {
#endif

//IO方向设置
#define SDA_OUT() {GPIOB->MODER  &= ~(GPIO_MODER_MODER0 << (7 * 2));\
                                GPIOB->MODER |= (((uint32_t)LL_GPIO_MODE_OUTPUT) << (7 * 2));}

#define SDA_IN() {GPIOB->MODER  &= ~(GPIO_MODER_MODER0 << (7 * 2));\
                                GPIOB->MODER |= (((uint32_t)LL_GPIO_MODE_INPUT) << (7 * 2));}
#define IIC_SCL_SET   LL_GPIO_SetOutputPin(GPIOB,LL_GPIO_PIN_6)
#define IIC_SCL_RESET LL_GPIO_ResetOutputPin(GPIOB,LL_GPIO_PIN_6)
#define IIC_SDA_SET   LL_GPIO_SetOutputPin(GPIOB,LL_GPIO_PIN_7)
#define IIC_SDA_RESET LL_GPIO_ResetOutputPin(GPIOB,LL_GPIO_PIN_7)
#define IIC_READ_SDA  LL_GPIO_IsInputPinSet(GPIOB,LL_GPIO_PIN_7)

/*声明函数*/
void my_iic_init(void);
void my_iic_start(void);
void my_iic_stop(void);
u8 my_iic_wait_ack(void);
void my_iic_ack(void);
void my_iic_nack(void);
void my_iic_send_byte(u8 txd);
u8 my_iic_read_byte(u8 ack);


#ifdef __cplusplus
}
#endif

#endif



