/*
 * @Description: 
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-17 09:54:11
 * @LastEditors: lzc
 * @LastEditTime: 2020-12-02 09:38:32
 */
#ifndef MY_USART_H
#define MY_USART_H

#include "app_config.h"
#ifdef __cplusplus
 extern "C" {
#endif
enum dev {
	_msg_dev_us,
	_msg_dev_bt,
};
typedef struct {
	u16 num;//接收字节数
	u8 dev;//设备,
	bool start;
	bool end;//接收状态
}Msg_Info_Type;

extern volatile u8 g_msg_buf[50];
extern volatile u8 g_rx_buf[50];
extern volatile Msg_Info_Type g_msg_rt_info;

void my_usart_init(void);
void putstr(const char * str);
void putchar_usart1(u8 ch);
void putchar_usart2(u8 ch);
void change_bt_uart1_9600_baud(void);
void change_bt_uart1_115200_baud(void);
bool change_bt_baud(const char *buf);
#ifdef __cplusplus
}
#endif

#endif

