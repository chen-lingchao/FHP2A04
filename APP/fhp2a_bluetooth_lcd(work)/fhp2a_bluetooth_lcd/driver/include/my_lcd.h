#ifndef _APP_LCD_H
#define _APP_LCD_H
#include "app_config.h"
#ifdef __cplusplus
 extern "C" {
#endif

/*********************************************预定义和变量类型*****************************************/
/*电平控制*/
#define LCD_CS_PIN_HIGH  		LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_13)
#define LCD_CS_PIN_LOW  		LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_13)
#define LCD_WR_PIN_HIGH  		LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_14)
#define LCD_WR_PIN_LOW 			LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_14)
#define LCD_DATA_PIN_HIGH 	LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_15)
#define LCD_DATA_PIN_LOW 		LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_15)

/*head*/
#define HEAD_WRITE_MODE   0x05
#define HEAD_COMMAND_MODE 0x04

/*command */
#define SYS_DIS 0
#define SYS_EN  1
#define LCD_OFF 2
#define LCD_ON  3


#define LCD_CLK_US 5   //

#define LCD_MEM_BUF_NUM 14
  
 typedef struct {
  u8 lcd_mem_1_2;
  u8 lcd_mem_3_4;
  u8 lcd_mem_5_6;
  u8 lcd_mem_7_8;
  u8 lcd_mem_9_10;
  u8 lcd_mem_11_12;
  u8 lcd_mem_13_14;
  u8 lcd_mem_15_16;
  u8 lcd_mem_17_18;
  u8 lcd_mem_19_20;
  u8 lcd_mem_21_22;
  u8 lcd_mem_23_24;
  u8 lcd_mem_25_26;
  u8 lcd_mem_27_28;
}Lcd_Mem_Typedef_Item;
typedef union {
  Lcd_Mem_Typedef_Item lcd_mem_item;
  u8 lcd_dis_mem[LCD_MEM_BUF_NUM];
}Lcd_Mem_Typedef;


/*********************************************声明函数*****************************************/
void my_lcd_init(void);
void lcd_refresh_buf(void);
void lcd_display_wavelength(u32 wavelength, bool nm);
void lcd_display_usb(bool flag);
void lcd_display_power(float value, u8 unit, u8 status, u8 work);
void lcd_back_light(bool flag);
void lcd_display_line(bool line1, bool line2);
void lcd_display_count(u32 count, bool hz, bool save);
void lcd_display_battery(u8 level);
void lcd_display_shutdown(bool flag);

void lcd_display_bluetooth(bool flag);
void lcd_display_wifi(u8 flag);
void lcd_display_vfl(bool flag);
void lcd_display_buzz(bool flag);

void lcd_start_sign(void);

void my_lcd_clear(void);
#ifdef __cplusplus
}
#endif

#endif






