#ifndef MY_ADC_H
#define MY_ADC_H
#include "app_config.h"
#ifdef __cplusplus
 extern "C" {
#endif

void my_adc_init(void);
u32 get_vol_data(void);
u32 get_vol_battery(void);
bool get_vol_keyt(void);
#ifdef __cplusplus
}
#endif

#endif

