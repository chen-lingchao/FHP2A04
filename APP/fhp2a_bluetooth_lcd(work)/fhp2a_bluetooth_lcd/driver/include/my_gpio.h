#ifndef MY_GPIO_H
#define MY_GPIO_H
#include "app_config.h"
#ifdef __cplusplus
 extern "C" {
#endif

void set_pin_channel(u8 channel);
u16 scan_key_x_y(void);
void my_gpio_init(void);

void scan_charge_battery(u8 *usb, u8 *bat);
void scan_off_time(void);
void scan_off_key(void);
void set_pin_bt(bool flag);
void set_pin_vfl(bool flag);
void set_pin_cbt(bool flag);
void buzz_on(int ms);
#ifdef __cplusplus
}
#endif

#endif


