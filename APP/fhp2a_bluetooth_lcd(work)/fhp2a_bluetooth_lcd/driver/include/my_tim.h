/** 
* \file my_tim.c
* \brief 定时器初始化
* \author  mirlee
* \version 0.1
* \date    
*/

#ifndef MY_TIM_H
#define MY_TIM_H

#include "app_config.h"
#ifdef __cplusplus
 extern "C" {
#endif
void my_tim_init(void);

void set_vlf_hz(bool flag);

u32 get_second_tick(void);
u32 get_second_tick_re(void);
void refresh_second_tick(void);

//外部变量，频率
extern volatile u16 g_optical_freq_num;

#ifdef __cplusplus
}
#endif

#endif




