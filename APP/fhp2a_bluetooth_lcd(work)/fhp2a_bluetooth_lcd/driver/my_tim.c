#include "my_tim.h"
#include "my_data.h"
#include "my_gpio.h"
#include "my_lcd.h"

static volatile u8 s_capture_flag;
static volatile u16 s_tim1_overflow;
static volatile u16 s_break_num;
static volatile u16 s_tim1_counter_num1;
static volatile u16 s_tim1_counter_num2;
volatile u16 g_optical_freq_num;

static volatile u32 my_second_tick;
static volatile u32 my_second_tick_re;

static volatile bool s_vlf_status;
/**
 * @brief tim6,用于产生1s的中断
 * @param
 * @return
 */
void my_tim6_init(void)
{
    LL_TIM_InitTypeDef TIM_InitStruct = {0};
    memset(&TIM_InitStruct, 0, sizeof(TIM_InitStruct));

    /* Peripheral clock enable */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM6);

    /* TIM6 interrupt Init */
    NVIC_SetPriority(TIM6_IRQn, 5);
    NVIC_EnableIRQ(TIM6_IRQn);

    TIM_InitStruct.Prescaler = 16000 - 1;
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
    TIM_InitStruct.Autoreload = 250 - 1;  //1秒
    LL_TIM_Init(TIM6, &TIM_InitStruct);
    LL_TIM_EnableARRPreload(TIM6);

    LL_TIM_EnableIT_UPDATE(TIM6);
    LL_TIM_EnableCounter(TIM6);
}


void TIM6_IRQHandler(void)
{
    rt_interrupt_enter();
    if (LL_TIM_IsActiveFlag_UPDATE(TIM6))
    {
        LL_TIM_ClearFlag_UPDATE(TIM6);
        if (g_sys_flag.work.module.vfl.on)
        {
            if (g_sys_flag.work.module.vfl.hz)
            {
                if (s_vlf_status)
                    set_pin_vfl(true);
                else
                    set_pin_vfl(false);

                s_vlf_status = !s_vlf_status;
            }
            else
            {
                set_pin_vfl(true);
                s_vlf_status = 0;
            }
        }
        my_second_tick++;
    }
    rt_interrupt_leave();
}

u32 get_second_tick(void)
{
    return my_second_tick / 4;
}

u32 get_second_tick_re(void)
{
    return my_second_tick_re / 4;
}
void refresh_second_tick(void)
{
    my_second_tick_re = my_second_tick;
}
///**
// * @brief tim1,用于识别频率
// * @param
// * @return
// */
//void my_tim1_init(void)
//{
//  LL_TIM_InitTypeDef TIM_InitStruct = {0};
//  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
//  /* Peripheral clock enable */
//  LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_TIM1);
//  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
//  /**TIM1 GPIO Configuration
//  PA8   ------> TIM1_CH1
//  */
//  GPIO_InitStruct.Pin = LL_GPIO_PIN_8;
//  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
//  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
//  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
//  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
//  GPIO_InitStruct.Alternate = LL_GPIO_AF_2;
//  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//
//  /* TIM1 interrupt Init */
//  NVIC_SetPriority(TIM1_BRK_UP_TRG_COM_IRQn, 1);
//  NVIC_EnableIRQ(TIM1_BRK_UP_TRG_COM_IRQn);
//  NVIC_SetPriority(TIM1_CC_IRQn, 2);
//  NVIC_EnableIRQ(TIM1_CC_IRQn);
//
//  TIM_InitStruct.Prescaler = 15;
//  TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
//  TIM_InitStruct.Autoreload = 0xffff;
//  TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
//  TIM_InitStruct.RepetitionCounter = 0;
//  LL_TIM_Init(TIM1, &TIM_InitStruct);
//  LL_TIM_EnableARRPreload(TIM1);
//  LL_TIM_SetClockSource(TIM1, LL_TIM_CLOCKSOURCE_INTERNAL);
//  LL_TIM_SetTriggerOutput(TIM1, LL_TIM_TRGO_RESET);
//  LL_TIM_DisableMasterSlaveMode(TIM1);
//  LL_TIM_IC_SetActiveInput(TIM1, LL_TIM_CHANNEL_CH1, LL_TIM_ACTIVEINPUT_DIRECTTI);
//  LL_TIM_IC_SetPrescaler(TIM1, LL_TIM_CHANNEL_CH1, LL_TIM_ICPSC_DIV1);
//  LL_TIM_IC_SetFilter(TIM1, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV1);
//  LL_TIM_IC_SetPolarity(TIM1, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_RISING);
//
//  LL_TIM_CC_EnableChannel(TIM1,LL_TIM_CHANNEL_CH1);
//  LL_TIM_EnableIT_CC1(TIM1);
//  LL_TIM_EnableIT_UPDATE(TIM1);
//  LL_TIM_EnableCounter(TIM1);
//}
//
//
//
//
///**
//  * @brief This function handles TIM1 break, update, trigger and commutation interrupts.
//  */
//void TIM1_BRK_UP_TRG_COM_IRQHandler(void)
//{
//  rt_interrupt_enter();
//  if(LL_TIM_IsActiveFlag_UPDATE(TIM1))
//  {
//      LL_TIM_ClearFlag_UPDATE(TIM1);
//      s_tim1_overflow++;
//  }
//  rt_interrupt_leave();
//}
//
///**
//  * @brief This function handles TIM1 capture compare interrupt.
//  */
//void TIM1_CC_IRQHandler(void)
//{
//  rt_base_t level=0;
//  u32 capture_temp = 0;
//  rt_interrupt_enter();
//  if(LL_TIM_IsActiveFlag_CC1(TIM1))
//  {
//    LL_TIM_ClearFlag_CC1(TIM1);
//
//    switch(s_capture_flag)
//    {
//      case 0:
//              level = rt_hw_interrupt_disable();
//        s_tim1_counter_num1 = LL_TIM_GetCounter(TIM1);
//        s_capture_flag = 1;
//              s_tim1_overflow=0;
//              rt_hw_interrupt_enable(level);
//      break;
//      case 1:
//              level = rt_hw_interrupt_disable();
//        s_tim1_counter_num2 = LL_TIM_GetCounter(TIM1);
//        /* Capture computation */
//        capture_temp = 0xFFFF * s_tim1_overflow+s_tim1_counter_num2 - s_tim1_counter_num1;
//
//        /* Frequency computation */
//        g_optical_freq_num = 1000000 / capture_temp;
//        s_capture_flag = 0;
//              s_tim1_overflow=0;
//              rt_hw_interrupt_enable(level);
//      break;
//      case 2:
//      break;
//    }
//  }
//  rt_interrupt_leave();
//}

void my_tim3_init(void)
{
    LL_TIM_InitTypeDef TIM_InitStruct = {0};
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    /* Peripheral clock enable */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
    /**TIM1 GPIO Configuration
    PA8   ------> TIM1_CH1
    */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* TIM1 interrupt Init */
    NVIC_SetPriority(TIM3_IRQn, 2);
    NVIC_EnableIRQ(TIM3_IRQn);

    TIM_InitStruct.Prescaler = 8 - 1;
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
    TIM_InitStruct.Autoreload = 0xffff;
    TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
    TIM_InitStruct.RepetitionCounter = 0;
    LL_TIM_Init(TIM3, &TIM_InitStruct);
    LL_TIM_EnableARRPreload(TIM3);
    LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
    LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_RESET);
    LL_TIM_DisableMasterSlaveMode(TIM3);
    LL_TIM_IC_SetActiveInput(TIM3, LL_TIM_CHANNEL_CH4, LL_TIM_ACTIVEINPUT_DIRECTTI);
    LL_TIM_IC_SetPrescaler(TIM3, LL_TIM_CHANNEL_CH4, LL_TIM_ICPSC_DIV1);
    LL_TIM_IC_SetFilter(TIM3, LL_TIM_CHANNEL_CH4, LL_TIM_IC_FILTER_FDIV1);
    LL_TIM_IC_SetPolarity(TIM3, LL_TIM_CHANNEL_CH4, LL_TIM_IC_POLARITY_RISING);

    LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH4);
    LL_TIM_EnableIT_CC4(TIM3);
    LL_TIM_EnableIT_UPDATE(TIM3);
    LL_TIM_EnableCounter(TIM3);
}

void TIM3_IRQHandler(void)
{
    rt_base_t level = 0;
    float capture_temp = 0;
    rt_interrupt_enter();
    if (data_ch_get() == 6 || data_ch_get() == 7)
    {
        if (LL_TIM_IsActiveFlag_UPDATE(TIM3))
        {
            LL_TIM_ClearFlag_UPDATE(TIM3);
        }
        if (LL_TIM_IsActiveFlag_CC4(TIM3))
        {
            LL_TIM_ClearFlag_CC4(TIM3);
        }
    }
    else
    {
        if (LL_TIM_IsActiveFlag_UPDATE(TIM3))
        {
            LL_TIM_ClearFlag_UPDATE(TIM3);
            s_tim1_overflow++;
            if (s_tim1_overflow > 10)
            {
                s_tim1_overflow = 0;
                g_optical_freq_num = 0;
            }
        }
        if (LL_TIM_IsActiveFlag_CC4(TIM3))
        {
            LL_TIM_ClearFlag_CC4(TIM3);
            switch (s_capture_flag)
            {
            case 0:
                level = rt_hw_interrupt_disable();
                LL_TIM_SetCounter(TIM3, 0);
                s_tim1_overflow = 0;
                s_tim1_counter_num1 = 0;
                s_capture_flag = 1;
                rt_hw_interrupt_enable(level);
                break;
            case 1:
                level = rt_hw_interrupt_disable();
                s_break_num++;
                if (s_break_num >= 10)
                {
                    s_tim1_counter_num2 = LL_TIM_GetCounter(TIM3);
                    /* Capture computation */
                    capture_temp = (u32)(65535) * s_tim1_overflow + s_tim1_counter_num2 - s_tim1_counter_num1;
                    /* Frequency computation */
                    g_optical_freq_num = (float)1000000.0 / (capture_temp / 10.0);
                    if (g_optical_freq_num == 0)
                    {
                        s_break_num = 1;
                    }
                    s_capture_flag = 0;
                    s_tim1_overflow = 0;
                    s_break_num = 0;
                }
                rt_hw_interrupt_enable(level);
                break;
            }
        }
    }
    rt_interrupt_leave();
}

void my_tim_init(void)
{
    my_tim6_init();
    my_tim3_init();
}


