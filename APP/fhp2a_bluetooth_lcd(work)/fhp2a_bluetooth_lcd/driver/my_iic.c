/**
* \file my_iic.c
* \brief io模拟iic
* \author  mirlee
* \version 0.1
* \date
*/

#include "my_iic.h"



void my_iic_init(void)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    memset(&GPIO_InitStruct, 0, sizeof(GPIO_InitStruct));
    /*开时钟*/
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
    /*SCL PB6, SDA PB7*/
    GPIO_InitStruct.Pin = LL_GPIO_PIN_6 | LL_GPIO_PIN_7;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void my_iic_start(void)
{
    SDA_OUT();     //sda线输出
    IIC_SDA_SET;
    IIC_SCL_SET;
    rt_hw_us_delay(1);
    IIC_SDA_RESET;//START:when CLK is high,DATA change form high to low
    rt_hw_us_delay(1);
    IIC_SCL_RESET;//钳住I2C总线，准备发送或接收数据
}

void my_iic_stop(void)
{
    SDA_OUT();//sda线输出
    IIC_SCL_RESET;
    IIC_SDA_RESET;//STOP:when CLK is high DATA change form low to high
    rt_hw_us_delay(1);
    IIC_SCL_SET;
    rt_hw_us_delay(1);
    IIC_SDA_SET;//发送I2C总线结束信号
}
u8 my_iic_wait_ack(void)
{
    u8 ucErrTime = 0;
    SDA_IN();      //SDA设置为输入
    IIC_SDA_SET; rt_hw_us_delay(1);
    IIC_SCL_SET; rt_hw_us_delay(1);
    while (IIC_READ_SDA)
    {
        ucErrTime++;
        if (ucErrTime > 250)
        {
            my_iic_stop();
            return 1;
        }
    }
    IIC_SCL_RESET;//时钟输出0
    return 0;
}

void my_iic_ack(void)
{
    IIC_SCL_RESET;
    SDA_OUT();
    IIC_SDA_RESET;
    rt_hw_us_delay(1);
    IIC_SCL_SET;
    rt_hw_us_delay(1);
    IIC_SCL_RESET;
}
//不产生ACK应答
void my_iic_nack(void)
{
    IIC_SCL_RESET;
    SDA_OUT();
    IIC_SDA_SET;
    rt_hw_us_delay(2);
    IIC_SCL_SET;
    rt_hw_us_delay(1);
    IIC_SCL_RESET;
}


//IIC发送一个字节
//返回从机有无应答
//1，有应答
//0，无应答
void my_iic_send_byte(u8 txd)
{
    u8 t;
    SDA_OUT();
    IIC_SCL_RESET;//拉低时钟开始数据传输
    for (t = 0; t < 8; t++)
    {
        if ((txd & 0x80) >> 7)
        {
            IIC_SDA_SET;
        }
        else
            IIC_SDA_RESET;
        txd <<= 1;
        rt_hw_us_delay(1);   //对TEA5767这三个延时都是必须的
        IIC_SCL_SET;
        rt_hw_us_delay(1);
        IIC_SCL_RESET;
        // rt_hw_us_delay(1);
    }
}
//读1个字节，ack=1时，发送ACK，ack=0，发送nACK
u8 my_iic_read_byte(u8 ack)
{
    unsigned char i, receive = 0;
    SDA_IN();//SDA设置为输入
    for (i = 0; i < 8; i++ )
    {
        IIC_SCL_RESET;
        rt_hw_us_delay(1);
        IIC_SCL_SET;
        receive <<= 1;
        if (IIC_READ_SDA)receive++;
        rt_hw_us_delay(1);
    }
    if (!ack)
        my_iic_nack();//发送nACK
    else
        my_iic_ack(); //发送ACK
    return receive;
}




