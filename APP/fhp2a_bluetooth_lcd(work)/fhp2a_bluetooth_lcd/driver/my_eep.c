/**
 * \file my_eep.c
 * \brief eeprom相关操作
 * \author  mirlee
 * \version 0.1
 * \date
 */
#include "my_eep.h"
#include "my_iic.h"

void eeprom_init(void)
{
	my_iic_init();
}
// stop 之前可以连续读
//读可以连续读，写不能连续写
u8 eeprom_read_byte(u16 addr)
{
	u8 temp = 0;
	my_iic_start();
	if (EE_TYPE > AT24C16)
	{
		my_iic_send_byte(0XA0); //发送写命令
		my_iic_wait_ack();
		my_iic_send_byte(addr >> 8);
	}
	else
	{
		my_iic_send_byte(0XA0 + ((addr >> 8) << 1));
	}
	my_iic_wait_ack();
	my_iic_send_byte(addr % 256); //发送低地址
	my_iic_wait_ack();
	my_iic_start();
	my_iic_send_byte(0xa1); //进入接收模式
	my_iic_wait_ack();
	temp = my_iic_read_byte(0);
	my_iic_stop();
	return temp;
}

void eeprom_write_byte(u16 addr, u8 data)
{
	my_iic_start();
	if (EE_TYPE > AT24C16)
	{
		my_iic_send_byte(0XA0); //发送写命令
		my_iic_wait_ack();
		my_iic_send_byte(addr >> 8);
	}
	else
	{
		my_iic_send_byte(0XA0 + ((addr >> 8) << 1));
	}
	my_iic_wait_ack();
	my_iic_send_byte(addr % 256); //发送低地址
	my_iic_wait_ack();
	my_iic_send_byte(data);
	my_iic_wait_ack();
	my_iic_stop();
	rt_thread_mdelay(5);
}

void eeprom_write_bytes(u16 addr, u8 data[], u16 num)
{
	for (u32 i = 0; i < num; i++)
	{
		eeprom_write_byte(addr + i, data[i]);
	}
}

void eeprom_read_bytes(u16 addr, u8 data[], u16 num)
{
	int i = 0;
	rt_enter_critical();
	my_iic_start();
	if (EE_TYPE > AT24C16)
	{
		my_iic_send_byte(0XA0); //发送写命令
		my_iic_wait_ack();
		my_iic_send_byte(addr >> 8);
	}
	else
	{
		my_iic_send_byte(0XA0 + ((addr >> 8) << 1));
	}
	my_iic_wait_ack();
	my_iic_send_byte(addr % 256); //发送低地址
	my_iic_wait_ack();
	my_iic_start();
	my_iic_send_byte(0xa1); //进入接收模式
	my_iic_wait_ack();
	for (i = 0; i < num - 1; i++)
	{
		data[i] = my_iic_read_byte(1);
	}
	data[i] = my_iic_read_byte(0);
	my_iic_stop();
	rt_exit_critical();
}

static __inline void eeprom_write_page(u16 addr, u8 *data, u16 num)
{
	rt_enter_critical();
	my_iic_start();
	if (EE_TYPE > AT24C16)
	{
		my_iic_send_byte(0XA0); //发送写命令
		my_iic_wait_ack();
		my_iic_send_byte(addr >> 8);
	}
	else
	{
		my_iic_send_byte(0XA0 + ((addr >> 8) << 1));
	}
	my_iic_wait_ack();
	my_iic_send_byte(addr % 256); //发送低地址
	my_iic_wait_ack();
	for (u16 i = 0; i < num; i++)
	{
		my_iic_send_byte(*(data + i)); //发送字节
		my_iic_wait_ack();
	}
	my_iic_stop(); //产生一个停止条件
	rt_exit_critical();
	rt_thread_mdelay(10);
}

#define Debug_EEPROM 1 //主要用于进行存储时数据是否需要进行校验
void eeprom_write_page_bytes(u16 addr, u8 *data, u16 num)
{
	u16 offsetW;
	u16 numBytes;
	u8 result = 0;
#if Debug_EEPROM
	u16 wr_num = 0;
	u16 wr_addr = 0;
	u8 check_buffer[64];
	u8 *ptr;
	u8 Err_Count = 0;

	ptr = data;
	wr_num = num;
	wr_addr = addr;
	rt_memset(check_buffer, 0, sizeof(check_buffer));
#endif
	while (!result)
	{
#if Debug_EEPROM
		while (wr_num > 0)
		{
			offsetW = EEP_PAGE_SIZE - wr_addr % EEP_PAGE_SIZE;
			numBytes = (offsetW > wr_num) ? wr_num : offsetW;
			eeprom_write_page(wr_addr, ptr, numBytes);
			wr_num -= numBytes;
			if (wr_num > 0)
			{
				ptr = ptr + numBytes;
				wr_addr = wr_addr + numBytes;
			}
		}
		// DONE: 增加校验机制 删除历史记录时 999 * 2 不进行读取
		// 此处主要针对校准值以及整偏，还有历史记录的存储。
		if (num < 128)
		{
			eeprom_read_bytes(addr, check_buffer, num);
			rt_thread_mdelay(5); // 换页延时
			if (rt_strcmp((const char *)data, (const char *)check_buffer) == 0)
			{
				result = 1;
			}
			else
			{
				Err_Count++;
				ptr = data;
				wr_num = num;
				wr_addr = addr; 
				rt_memset(check_buffer, 0, sizeof(check_buffer));
				//错误次数过多
				if (Err_Count >= 10)
					break;
			}
		}
		else
		{
			result = 1;
		}

#else
		while (num > 0)
		{
			offsetW = EEP_PAGE_SIZE - addr % EEP_PAGE_SIZE;
			numBytes = (offsetW > num) ? num : offsetW;
			eeprom_write_page(addr, data, numBytes);
			num -= numBytes;
			if (num > 0)
			{
				data = data + numBytes;
				addr = addr + numBytes;
			}
		}
		result = 1;
#endif
	}
}
