/*
 * @Description: 
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-27 13:17:53
 * @LastEditors: lzc
 * @LastEditTime: 2020-12-01 11:13:25
 */
#ifndef MY_MSG_H
#define MY_MSG_H

#include "app_config.h"
#ifdef __cplusplus
extern "C"
{
#endif

/******************************************************************
 * @brief  通讯处理,外部调用      
 * @param  data 串口数据
 * @return enum MS
 */
	int msg_parse(const char *data);
/*****************************************************************/

//putstr 不发送'\0'
#define MSG_OUTPUT(x) putstr(x)
	//#define MSG_OUTPUT(...) printf(__VA_ARGS__)

#define MSG_CMD_TYTES 20 //串口数据解析成函数名的最大长度
#define MSG_CMD_LEN 3	 //协议中命令的最大长度
#define MSG_ARG_LEN 30	 //协议中数据的最大长度
#define MSG_MAX_ARGS 3	 //一个协议帧包含的命令数量，如cal poi pow

	typedef struct
	{
		u8 cmd[MSG_CMD_TYTES];
		u8 data[2][30];
	} Cmd_Info_Type;

	typedef struct init_t
	{
		int (*msg_port_fun)(Cmd_Info_Type info);
		char name[MSG_CMD_TYTES];
	} _init_msg;

	enum MS
	{
		MS_EOK,
		MS_EFULL,
		MS_ERROR,
	};

#define _MSG_FUN(fn, level)                                 \
	__attribute__((used)) const _init_msg _imp_##fn         \
		__attribute__((section(".msg_section." level))) = { \
			.msg_port_fun = fn,                             \
			.name = #fn,                                    \
	}
#define IMPORT_MSG_FUN(fn) _MSG_FUN(fn, "0")

#ifdef __cplusplus
}
#endif

#endif
