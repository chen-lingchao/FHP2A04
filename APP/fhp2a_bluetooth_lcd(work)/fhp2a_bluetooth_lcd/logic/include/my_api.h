/*
 * @Description: 
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-17 09:54:11
 * @LastEditors: lzc
 * @LastEditTime: 2020-11-24 16:08:00
 */
#ifndef MY_API_H
#define MY_API_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "app_config.h"
union _fun_float2hex 
{ 
    long int m; 
    float f; 
};
float my_atof(const char *s);
void my_itoa(int n, char buf[]);
void my_ftoa(float f,char *buf);
void JumpToBL(uint32_t target_address);
#ifdef __cplusplus
 }
#endif

#endif
