/*
 * @Description: 
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2021-12-10 12:31:04
 * @LastEditors: lzc
 * @LastEditTime: 2022-03-10 15:02:30
 */
#ifndef MY_KEY_H
#define MY_KEY_H
#include "my_gpio.h"
#include "app_config.h"
#ifdef __cplusplus
extern "C"
{
#endif

    /*按键标志*/
    typedef enum
    {
        KEY_EMPTY,

        KEY_ONE_S,
        KEY_TWO_S,
        KEY_THREE_S,
        KEY_FOUR_S,
        KEY_FIVE_S,
        KEY_SIX_S,
        KEY_SEVEN_S,
        KEY_EIGHT_S,
        KEY_NINE_S,

        KEY_ONE_L,
        KEY_TWO_L,
        KEY_THREE_L,
        KEY_FOUR_L,
        KEY_FIVE_L,
        KEY_SIX_L,
        KEY_SEVEN_L,
        KEY_EIGHT_L,
        KEY_NINE_L,

        KEY_END,
    } Key_Press_Flag;

    /*声明函数*/
    u32 key_scan(void);
    u8 key_deal(u32 k);

#ifdef __cplusplus
}
#endif

#endif
