/*
 * @Description: my_agreement,协议处理H文件
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-24 08:53:41
 * @LastEditors: lzc
 * @LastEditTime: 2020-11-27 09:11:00
 */
#ifndef MY_AGREEMENT_H
#define MY_AGREEMENT_H

#ifdef __cplusplus

extern "C"
{
#endif

#include "app_config.h" 
#include "my_record.h" 
#include "my_display.h"
#include "my_lcd.h"
#include "my_usart.h"
#include "my_data.h"
#include "my_api.h"
#include "my_key.h"
#include "my_eep.h"
#include "my_adc.h"
#include "my_msg.h"
/**
 * @function: Agreement_Type
 * @description:  hex协议的结构体
 */
    typedef struct
    {
        volatile u8 *data; //协议的数据
        u8 status;         //协议处理的状态，完成之后才能发送应答
        u8 type;           //串口协议判断，字符串或者是hex的协议
    } Agreement_Type;

    /*
*状态枚举
*/
    enum Status
    {
        _agr_ready_status,   // 准备状态
        _agr_process_status, // 处理状态
        _agr_end_status,     // 完成状态
    };

    enum Type
    {
        _agr_hex_type, // hex协议
        _agr_str_type, // 字符串协议
    };

    enum AGR
    {
        AGR_OK,
        AGR_FULL,
        AGR_ERROR,
    };

    enum bit
    {
        _ZERO_BIT,
        _ONE_BIT,
        _TWO_BIT,
        _THREE_BIT,
        _FOUR_BIT,
        _FIVE_BIT,
        _SIX_BIT,
        _SEVEN_BIT,
        _EIGHT_BIT,
        _NINE_BIT,
        _TEN_BIT,
        _ELEVEN_BIT,
        _TWELVE_BIT,
    };

void agreement_parse_output(void);
void agreement_parse_change_type(u8 agreement_type);
u8 agreement_parse_return_type(void);

extern rt_mutex_t g_mutex_1;

#ifdef __cplusplus
}
#endif

#endif
