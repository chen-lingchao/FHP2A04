/*
 * @Description: 
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-17 09:54:11
 * @LastEditors: lzc
 * @LastEditTime: 2020-11-19 16:56:13
 */
#ifndef MY_THREAD_H
#define MY_THREAD_H
#include "app_config.h"
#ifdef __cplusplus
 extern "C" {
#endif

void my_thread_init(void);
void key_send_flag(u32 f);
void my_thread_resume_msg(void);

void my_thread_on(void);
void my_thread_off(void);

rt_uint8_t Get_communicate_thread_State(void);
#ifdef __cplusplus
}
#endif

#endif

