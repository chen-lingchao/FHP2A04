#ifndef MY_DISPLAY_H
#define MY_DISPLAY_H
#ifdef __cplusplus
 extern "C" {
#endif
#include "app_config.h"

void display_control_task(void);

/*记录相关*/
void display_history_start(void);
void display_history_stop(void);
void display_history_up(u8 flag);
void display_history_down(u8 flag);
void display_history_del_one(void);
void display_history_del_all(void);
void display_history_save_one(void);
/*参考值先关*/
void display_ref_set_data(void);
void display_ref_show_data(void);



/**/
extern volatile bool g_auto_off_flag;


#ifdef __cplusplus
}
#endif
#endif






