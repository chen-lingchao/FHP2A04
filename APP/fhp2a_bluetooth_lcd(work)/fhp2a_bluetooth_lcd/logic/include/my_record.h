/*
 * @Description:
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-17 09:54:11
 * @LastEditors: lzc
 * @LastEditTime: 2022-03-11 10:52:46
 */
#ifndef RECORD_H
#define RECORD_H
#include "app_config.h" 
#include "my_data.h"

typedef struct
{
	float optical_power;
	float ref_power;
	u16 freq;
	u8 unit;
	u8 wavelength;
} Record_Type;

void record_init(void);

void record_cal_coef_save(float *cal_coeff, int num);
void record_cal_linear_save(float *mw, int num1, int *vol, int num2);
int record_cal_read_all(float *mw, int byte_num1, int *vol, int byte_num2, float *coeff, int byte_num3);
void record_cal_clr_all(int num1, int num2, int num3);

u16 _return_liner_power_eeprom_addr(void);
u16 _return_liner_vol_eeprom_addr(void);
u16 _return_liner_coeff_eeprom_addr(void);

/*记录函数声明*/
u8 record_history_save(Record_Type *ptr);
Record_Type record_history_read(u16 index);
u8 record_history_del_single(u16 index);
void record_history_del_all(void);
u16 record_history_amount(void);
float record_ref_get(u8 offset);
void record_ref_set(float reff, u8 offset);
void record_ref_del_all(void);
void record_ref_del_current(u8 offect);
u16 record_off_time_get(void);
void record_off_time_set(u16 second);

bool record_bt_get(void);
void record_bt_set(bool flag);
bool record_bt_get_mac(u8 *buf);
bool record_bt_set_mac(const u8 *buf);

void record_PowerON_Wave_set(u32 Wave);
u32 record_PowerON_Wave_get(void);

char record_Seting_Wave_get(void);
void record_Seting_Wave_set_980nm(char wave_980_status);
void record_Seting_Wave_set_1270nm(char wave_1270_status);
void record_Seting_Wave_set_1577nm(char wave_1577_status);
void record_Seting_Wave_set_1650nm(char wave_1650_status);
void record_Seting_Wave_set(Wave_Enum wave, char wave_status);

bool record_baud_get(void);
void record_baud_set(bool flag);
#endif
