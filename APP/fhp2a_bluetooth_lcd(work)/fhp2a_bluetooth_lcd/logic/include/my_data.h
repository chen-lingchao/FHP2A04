#ifndef MY_DATA_H
#define MY_DATA_H

#include "app_config.h"
#ifdef __cplusplus
extern "C"
{
#endif

#define CH_MAX_NUM (7) /*最大档位*/
#define WAVE_LINE (_wave_1550)

	/*标记串口控制的状态*/
	typedef struct
	{
		bool start; //校准模式就绪标记
		bool done;	//波长切换就绪标记
	} Msg_Status_Type;

	/*电池负号枚举*/
	typedef enum
	{
		_bat_empty,
		_bat_one,
		_bat_two,
		_bat_three,
		_bat_max,
	} Bat_Enum;

	/*
*波长枚举
*/
	typedef enum
	{
		_wave_850,
		_wave_980,
		_wave_1270,
		_wave_1300,
		_wave_1310,
		_wave_1490,
		_wave_1550,
		_wave_1577,
		_wave_1625,
		_wave_1650,
		_wave_max,
	} Wave_Enum;

	/*频率枚举*/
	typedef enum
	{
		_freq_850,
		_freq_1300,
		_freq_1310,
		_freq_1490,
		_freq_1550,
		_freq_1625,
		_freq_max,
	} Freq_Enum;

	/*
*功率单位枚举
*/
	typedef enum
	{
		_unit_mw,
		_unit_db,
		_unit_dbm,
		_unit_max,
	} Unit_Enum;
	/*
*功率范围枚举
*/
	typedef enum
	{
		_optical_low,
		_optical_high,
		_optical_normal,
		_optical_max,
	} Optical_Enum;
	/*
*工作模式枚举,每种状态都要互斥
*/
	typedef enum
	{
		_mode_single,
		_mode_twin,
		_mode_history,
		_mode_cal,
		_mode_brief,
		_mode_max,
	} Work_Mode_Enum;

	/*
*显示标志，用于短暂显示的内容标记
*/
	typedef enum
	{
		_dis_ref_get,  //查看参考值
		_dis_ref_set,  //设置参考值
		_dis_his_one,  //删除一条历史
		_dis_his_all,  //删除全部历史
		_dis_his_save, //保存一条历史
		_dis_max,
	} Dis_Flag_Enum;

	typedef struct
	{
		bool on;
		bool hz;
	} W_Vfl_Type;

	typedef struct
	{
		W_Vfl_Type vfl; //红光
		bool bt;
		bool wifi;
		bool buzz; //蜂鸣器
		bool led;  //led灯光
	} W_Module_Type;

	typedef struct
	{
		Work_Mode_Enum mode_re;
		Work_Mode_Enum mode;  //工作状态的标记，包括single，twin，cal，history，brief
		Dis_Flag_Enum brief;  //短暂显示标记,用于显示Dis_Flag_Enum中的内容
		Msg_Status_Type cal;  //校准模式状态的标记
		W_Module_Type module; //模块工作状态标记
	} W_Dis_Type;

	typedef struct
	{
		Bat_Enum bat;
		Wave_Enum wave;
		Freq_Enum freq;
		Optical_Enum opt;
		Unit_Enum unit;
	} W_Data_Type; //数据

	typedef struct
	{
		W_Dis_Type work;
		W_Data_Type data;
	} Sys_Flag_Type;

	u8 get_wave_data(void);
	float data_optical_get(u8 unit);
	void data_vol_get(u32 *vol);
	void data_task(void);
	void data_init(void);
	void data_ref_get(float *ref, u8 wave);
	void data_ref_set(float ref, u8 wave);
	u16 data_freq_get(u16 freq);

	/*校准数据操作*/
	void data_cal_linear_set(u8 point, float power);
	void hex_data_cal_linear_set(u8 point, float power);
	void data_cal_coeff_set(float power);
	void hex_data_cal_coeff_set(float data);
	void data_cal_linear_save(void);
	void data_ch_set(u8 ch);
	u8 data_ch_get(void);

	u16 _return_liner_vol_eeprom_size(void);
	u16 _return_liner_pow_eeprom_size(void);
	void _clr_liner_eeprom(void);
	//外部变量
	extern volatile Sys_Flag_Type g_sys_flag;
	extern char g_Wave_Setting;
#ifdef __cplusplus
}
#endif

#endif
