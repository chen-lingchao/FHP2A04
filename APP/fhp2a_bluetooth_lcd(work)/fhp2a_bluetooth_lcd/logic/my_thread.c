/**
* \file my_thread.c
* \brief 线程创建
* \author  mirlee
* \version 0.1
* \date
*/
#include "my_key.h"
#include "my_lcd.h"
#include "my_data.h"
#include "my_display.h"
#include "my_thread.h"
#include "my_usart.h"
#include "my_msg.h"
#include "my_agreement.h"

static rt_thread_t s_key_deal_thread;
static rt_thread_t s_communicate_thread;
#if !MY_DEBUG
static rt_thread_t s_lcd_display_thread;
static bool msg_exist = false;
#endif

rt_mutex_t g_mutex_1;

/**
 * @brief 按键任务，其中扫描部分20ms执行一次
 * @param
 * @return
 */
static void key_deal_thread(void *parameter)
{
    u32 flag = 0;
    while (1)
    {
        flag = key_scan();
        if (flag)
        {
            if (g_sys_flag.work.mode != _mode_max)
                key_deal(flag);
        }
        scan_off_key();
        rt_thread_mdelay(20);
    }
}
#if !MY_DEBUG
//怎么协调同时的操作

/**
 * @function: communicate_thread
 * @description: 协议线程
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static void communicate_thread(void *parameter)
{
    rt_base_t level;
    while (1)
    {
        if (g_msg_rt_info.end == 1)
        {
            if (g_msg_rt_info.dev == _msg_dev_bt)
            {
                msg_parse((const char *)g_msg_buf);
                memset((void *)g_msg_buf, 0, strlen((const char *)g_msg_buf));
            }
            else if (g_msg_rt_info.dev == _msg_dev_us)
            {
                // TODO ：串口2 协议处理以及IAP的程序。
                agreement_parse_output();
            }
            rt_thread_suspend(rt_thread_self());
            level = rt_hw_interrupt_disable();
            g_msg_rt_info.num = 0;
            g_msg_rt_info.end = 0;
            rt_schedule();
            rt_hw_interrupt_enable(level);
        }
        else
        {
            rt_thread_suspend(rt_thread_self());
            rt_schedule();
        }
    }
}


u8 temp_data_debug = 0;
static void lcd_display_thread(void *parameter)
{
    while (1)
    {
        display_control_task();
        rt_thread_mdelay(20);
        temp_data_debug = data_ch_get();
        if (msg_exist == false)
        {
            /*通信线程*/
            msg_exist = true;
            s_communicate_thread = rt_thread_create("com", communicate_thread, RT_NULL,
                                                    1024, 3, 5);
            if (s_communicate_thread != RT_NULL)
            {
                rt_thread_startup(s_communicate_thread);
            }
        }
    }
}

#endif
__inline void my_thread_resume_msg(void)
{
    rt_thread_resume(s_communicate_thread);
}

void my_thread_init(void)
{
    /*创建互斥量*/
    g_mutex_1 = rt_mutex_create("dis", RT_IPC_FLAG_FIFO);
    if (g_mutex_1 == RT_NULL)
        while (1);
    /*按键线程*/
    s_key_deal_thread = rt_thread_create("key_d", key_deal_thread, RT_NULL,
                                         512, 2, 5);
    if (s_key_deal_thread != RT_NULL)
    {
        rt_thread_startup(s_key_deal_thread);
    }


    /*lcd显示线程*/
    s_lcd_display_thread = rt_thread_create("lcd_d", lcd_display_thread, RT_NULL,
                                            512, 4, 5);
    if (s_lcd_display_thread != RT_NULL)
    {
        rt_thread_startup(s_lcd_display_thread);
    }
#if !MY_DEBUG
#endif
}


void my_thread_on(void)
{

    /*lcd显示线程*/
    s_lcd_display_thread = rt_thread_create("lcd_d", lcd_display_thread, RT_NULL,
                                            512, 4, 5);
    if (s_lcd_display_thread != RT_NULL)
    {
        rt_thread_startup(s_lcd_display_thread);
    }
#if !MY_DEBUG
#endif
}

void my_thread_off(void)
{
    rt_thread_delete(s_lcd_display_thread);
    rt_thread_delete(s_communicate_thread);
}


/**
 * @function:Get_communicate_thread_State
 * @description:获取通讯线程状态
 * @param {*}
 * @return {*}
 * @author: lzc
 */
rt_uint8_t Get_communicate_thread_State(void)
{
    return s_communicate_thread->stat;
}
