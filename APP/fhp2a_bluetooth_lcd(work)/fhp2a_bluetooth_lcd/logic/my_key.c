/**
 * \file my_key.c
 * \brief 按键扫描处理
 * \author  mirlee
 * \version 0.1
 * \date
 */
#include "my_lcd.h"
#include "my_gpio.h"
#include "my_key.h"
#include "my_data.h"
#include "my_display.h"
#include "my_usart.h"
#include "my_bt.h"
#include "my_tim.h"

//蜂鸣器响的时间
#define BUZZ_L_T 100
#define BUZZ_S_T 40
//
extern rt_mutex_t g_mutex_1;

#define C1 0         //空闲
#define C2 1         //消抖
#define C3 2         //短按键
#define C4 3         //长按键
#define C5 4         //连续按键
#define T1 2         //短按时间,20ms一次
#define T2 75        //长按时间,1.5s
#define CC_S 0xFC00  // 10个电平，留几位用于标记长按和连续按
#define CC_S1 0xEC00 //连续按标记
typedef struct
{
    u16 cnt;   //计时
    u16 state; //状态
    u16 up;    //弹起按键
    u16 down;  //按下按键
    u16 cur;   //当前键值
    u16 pre;   //上次键值
} Key_Info;
static Key_Info s_key_info;
#if MY_DEBUG
static char tempc = 0;
#endif

/*按键电平分析,支持组合键，输入按键电平，每一位对应一个按键电平*/
static u16 key_parse(u16 CC)
{
    u16 R = 0;
    /*2.获取电平*/
    s_key_info.cur = CC & ~CC_S; //获取按键电平,按键值通过CC输入
    /*3.开始分析*/
    switch (s_key_info.state)
    {
    case C1:
        /*4.此次电平状态和上次状态对比*/
        if (s_key_info.pre != s_key_info.cur)
        {
            //有新电平,则消抖
            s_key_info.cnt = 0;
            s_key_info.state = C2; //消抖状态
        }
        else if (s_key_info.down)
        {
            //没有新电平,且当前已有按键处于按下状态
            s_key_info.cnt = 0;
            s_key_info.state = C4; //长按
        }
        break;

    case C2: //消抖
        if (s_key_info.pre != s_key_info.cur)
        {
            s_key_info.cnt++;
            if (s_key_info.cnt >= T1) //消抖OK
            {
                s_key_info.state = C3; //短按键
            }
        }
        else //消抖失败
        {
            s_key_info.state = C1;
        }
        break;
    case C3: //短按键
        //获得弹起的按键
        s_key_info.up = (s_key_info.pre ^ s_key_info.cur) & s_key_info.pre; // up等于之前的按键
        //按下的按键
        s_key_info.down |= (s_key_info.pre ^ s_key_info.cur) & s_key_info.cur; // down等于当前的按键
        R = s_key_info.up & s_key_info.down;                                   //按下的和弹起的是否一致
        //按下的位置弹起了，清除相应标记
        s_key_info.down &= ~s_key_info.up;
        s_key_info.state = C1;
        //当前键值覆盖上次键值
        s_key_info.pre = s_key_info.cur;
        break;
    case C4: //长按键
        s_key_info.cnt++;
        if (s_key_info.pre != s_key_info.cur)
        {
            //有按键变化，则退出长按键检测
            s_key_info.state = C1;
        }
        else if (s_key_info.cnt >= T2)
        {
            R = s_key_info.down | CC_S; //区别长按键和短按键
            s_key_info.down = 0;
            s_key_info.cnt = 0;
            s_key_info.state = C5;
        }
        break;
    case C5:
        //长按触发后，进入连续按判断
        s_key_info.cnt++;
        if (s_key_info.pre == s_key_info.cur)
        {
            if (s_key_info.cnt >= 25)
            {
                s_key_info.cnt = 0;
                R = s_key_info.cur | CC_S1;
            }
        }
        else
        {
            s_key_info.state = C1;
        }
        break;
    }
    return R;
}

/**
 * @brief 按键扫描，20ms执行一次
 *   按键序号对应的硬件位置
        //         4    7
        //    2    5    8
        //    3    6    9
        //键位规划--暂时未使用
        //    电源      波长    CW       单位
        //    历史      参考     背光
        //      TWIN
        //拓展:  1.设置自动关机时间
                2.设置参考值
                3.设置开机波长
                4.设置上下限
                5.设置TWIN模式频率
 * @param
 * @return
 */
u32 key_scan(void)
{
    u8 temp;
    switch (key_parse(scan_key_x_y()))
    {
    case 0x0001:
        temp = KEY_ONE_S;
        break;
    case 0x0002:
        temp = KEY_TWO_S;
        break;
    case 0x0004:
        temp = KEY_THREE_S;
        break;
    case 0x0008:
        temp = KEY_FOUR_S;
        break;
    case 0x0010:
        temp = KEY_FIVE_S;
        break;
    case 0x0020:
        temp = KEY_SIX_S;
        break;
    case 0x0040:
        temp = KEY_SEVEN_S;
        break;
    case 0x0080:
        temp = KEY_EIGHT_S;
        break;
    case 0x0100:
        temp = KEY_NINE_S;
        break;
    case 0xFC02:
        temp = KEY_TWO_L;
        break;
    case 0xFC04:
        temp = KEY_THREE_L;
        break;
    case 0xFC08:
        temp = KEY_FOUR_L;
        break;
    case 0xFC10:
        temp = KEY_FIVE_L;
        break;
    case 0xFC20:
        temp = KEY_SIX_L;
        break;
    case 0xFC40:
        temp = KEY_SEVEN_L;
        break;
    case 0xFC80:
        temp = KEY_EIGHT_L;
        break;
    default:
        temp = 0;
        break;
    }
    return temp;
}

/**
 * @brief 按键处理
 * @param
 * @return
 */
u8 key_deal(u32 k)
{
    refresh_second_tick();
    switch (k)
    {
    case KEY_TWO_S: // history
        if ((g_sys_flag.work.mode != _mode_history) && (g_sys_flag.work.mode != _mode_cal))
        {
            display_history_start();
        }
        else
        {
            display_history_stop();
        }
        buzz_on(BUZZ_S_T);
        break;
    case KEY_THREE_S:
        g_sys_flag.work.module.buzz = !g_sys_flag.work.module.buzz;
        buzz_on(BUZZ_S_T);

        break;
    case KEY_FOUR_S: // wavelength
#if MY_DEBUG
        /*档位减一*/
        tempc = tempc <= 0 ? 0 : --tempc;
        set_pin_channel(tempc);
        lcd_display_wavelength(tempc, false);
        lcd_refresh_buf();
#else
        if (g_sys_flag.work.mode == _mode_single)
        {
            g_sys_flag.data.wave = g_sys_flag.data.wave >= (_wave_max - 1) ? _wave_850 : ++g_sys_flag.data.wave;
            // 如果设置无980波长，同时此时按键切换至980则跳至下一波长
            if (((g_Wave_Setting & 0x08) >> 3) != 1 && g_sys_flag.data.wave == _wave_980)
            {
                g_sys_flag.data.wave ++;
            } 
            // 如果设置无1270波长，同时此时按键切换至1270则跳至下一波长
            if (((g_Wave_Setting & 0x04) >> 2) != 1 && g_sys_flag.data.wave == _wave_1270)
            {
                g_sys_flag.data.wave++;
            }
            // 如果设置无1577波长，同时此时按键切换至1577则跳至下一波长
            if (((g_Wave_Setting & 0x02) >> 1) != 1 && g_sys_flag.data.wave == _wave_1577)
            {
                g_sys_flag.data.wave++;
            }
            // 如果设置无1650波长，同时此时按键切换至1650则跳至850
            if (((g_Wave_Setting & 0x01) >> 0) != 1 && g_sys_flag.data.wave == _wave_1650)
            {
                g_sys_flag.data.wave = _wave_850;
            }
            g_sys_flag.work.mode = _mode_single;
        }
        else if (g_sys_flag.work.mode == _mode_twin)
        {
            g_sys_flag.work.mode = _mode_single;
        }
        else if (g_sys_flag.work.mode == _mode_history)
        {
            if (g_sys_flag.work.mode == _mode_brief)
                break;
            display_history_up(0);
        }
#endif
        buzz_on(BUZZ_S_T);
        break;
    case KEY_FIVE_S: // ref
#if MY_DEBUG
        /*档位加一*/
        tempc = tempc >= 7 ? 7 : ++tempc;
        set_pin_channel(tempc);
        lcd_display_wavelength(tempc, false);
        lcd_refresh_buf();
#else
        if (g_sys_flag.work.mode == _mode_single || g_sys_flag.work.mode == _mode_twin)
        {
            if (g_sys_flag.work.mode == _mode_brief)
                break;
            rt_mutex_take(g_mutex_1, RT_WAITING_FOREVER);
            g_sys_flag.work.brief = _dis_ref_get;
            g_sys_flag.work.mode_re = g_sys_flag.work.mode;
            g_sys_flag.work.mode = _mode_brief;
            rt_mutex_release(g_mutex_1);
        }
        else if (g_sys_flag.work.mode == _mode_history)
        {
            if (g_sys_flag.work.mode == _mode_brief)
                break;
            display_history_down(0);
        }
#endif
        buzz_on(BUZZ_S_T);
        break;
    case KEY_SIX_S:
#if VFL_USE
        g_sys_flag.work.module.vfl.on = !g_sys_flag.work.module.vfl.on;
        if (g_sys_flag.work.module.vfl.on)
        {
            set_pin_vfl(true);
        }
        else
        {
            set_pin_vfl(false);
            g_sys_flag.work.module.vfl.hz = 0;
        }
#endif
        buzz_on(BUZZ_S_T);
        break;
    case KEY_SEVEN_S: // unit
        if (g_sys_flag.work.mode == _mode_single || g_sys_flag.work.mode == _mode_twin)
        {
            g_sys_flag.data.unit = g_sys_flag.data.unit >= (_unit_max - 1) ? _unit_mw : ++g_sys_flag.data.unit;
        }
        else if (g_sys_flag.work.mode == _mode_history)
        {
            if (g_sys_flag.work.mode == _mode_brief)
                break;
            rt_mutex_take(g_mutex_1, RT_WAITING_FOREVER);
            g_sys_flag.work.brief = _dis_his_one;
            g_sys_flag.work.mode_re = g_sys_flag.work.mode;
            g_sys_flag.work.mode = _mode_brief;
            rt_mutex_release(g_mutex_1);
        }
        buzz_on(BUZZ_S_T);
        break;
    case KEY_EIGHT_S: // back light
        lcd_back_light(1);
        buzz_on(BUZZ_S_T);
        break;
    case KEY_NINE_S:
        buzz_on(BUZZ_S_T);
#if BT_USE
        my_bt_init();
#endif
        break;
    case KEY_TWO_L: // history long
        if ((g_sys_flag.work.mode == _mode_single) || (g_sys_flag.work.mode == _mode_twin))
        {
            if (g_sys_flag.data.opt == _optical_normal)
            {
                if (g_sys_flag.work.mode == _mode_brief)
                    break;
                /*保存当前状态*/
                rt_mutex_take(g_mutex_1, RT_WAITING_FOREVER);
                g_sys_flag.work.brief = _dis_his_save;
                g_sys_flag.work.mode_re = g_sys_flag.work.mode;
                g_sys_flag.work.mode = _mode_brief;
                rt_mutex_release(g_mutex_1);
            }
        }
        buzz_on(BUZZ_L_T);
        break;
    case KEY_THREE_L:

        break;
    case KEY_FOUR_L: // wavelength long
        if (g_sys_flag.work.mode == _mode_single)
        {
            g_sys_flag.work.mode = _mode_twin;
        }
        else if (g_sys_flag.work.mode == _mode_twin)
        {
            g_sys_flag.work.mode = _mode_single;
        }
        else if (g_sys_flag.work.mode == _mode_history)
        {
            if (g_sys_flag.work.mode == _mode_brief)
                break;
            display_history_up(1);
        }
        buzz_on(BUZZ_L_T);
        break;
    case KEY_FIVE_L: // ref long
        if (g_sys_flag.work.mode == _mode_single || g_sys_flag.work.mode == _mode_twin)
        {
            if (g_sys_flag.work.mode == _mode_brief)
                break;
            rt_mutex_take(g_mutex_1, RT_WAITING_FOREVER);
            g_sys_flag.work.brief = _dis_ref_set;
            g_sys_flag.work.mode_re = g_sys_flag.work.mode;
            g_sys_flag.work.mode = _mode_brief;
            rt_mutex_release(g_mutex_1);
        }
        else if (g_sys_flag.work.mode == _mode_history)
        {
            if (g_sys_flag.work.mode == _mode_brief)
                break;
            display_history_down(1);
        }
        buzz_on(BUZZ_L_T);
        break;
    case KEY_SIX_L:
#if VFL_USE
        if (!g_sys_flag.work.module.vfl.on)
            break;
        buzz_on(BUZZ_L_T);
        g_sys_flag.work.module.vfl.hz = !g_sys_flag.work.module.vfl.hz;
#endif
        break;
    case KEY_SEVEN_L: // unit long
        if (g_sys_flag.work.mode == _mode_history)
        {
            if (g_sys_flag.work.mode == _mode_brief)
                break;
            rt_mutex_take(g_mutex_1, RT_WAITING_FOREVER);
            g_sys_flag.work.brief = _dis_his_all;
            g_sys_flag.work.mode_re = g_sys_flag.work.mode;
            g_sys_flag.work.mode = _mode_brief;
            rt_mutex_release(g_mutex_1);
        }
        buzz_on(BUZZ_L_T);
        break;
    case KEY_EIGHT_L:
        // TODO:增加清除当前参考值的功能
        break;
    }
    return 0;
}
