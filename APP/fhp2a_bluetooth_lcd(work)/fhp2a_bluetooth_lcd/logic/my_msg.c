/**
 * \file my_msg.c
 * \brief 协议
 * \author  mirlee
 * \version 0.3
 * \date
 * 命令的具体格式-参考《仪表通信指令》文档,已上传SVN
 *------------------------------------------------------------
 * 2020-06-26  改为注册制,
 *             使用IMPORT_MSG_FUN()宏定义声明新加的命令和函数.
 *             例------>
 *             命令: set wavelength[1490]\n
 *             函数: static int set_wav(Cmd_Info_Type info){}
 *             声明: IMPORT_MSG_FUN(set_wav);
 *------------------------------------------------------------
 * 2020-07-30  修正[msg_split()]不能准确判断两个字符命令的bug
 */

#include "my_msg.h"
#include "my_eep.h"
#include "my_key.h"
#include "my_usart.h"
#include "my_api.h"
#include "my_adc.h"
#include "my_data.h"
#include "my_gpio.h"
#include "my_lcd.h"
#include "my_record.h"

const uint32_t BL_FLASH_ADDRESS = 0X08000000; // BL代码在Flash上的起始地址

static const char COMPILE_DATE[] = __DATE__; // Year,Month,Date
static const char s_cMonth[] = "JanFebMarAprMayJunJulAugSepOctNovDec";
//
static char SoftVer_date_buf[12] = {0};
static char HardVer_date_buf[12] = {"2020-08-21\n"};
/*********************************static data**********************************/
static const char *const wave_str_buf[_wave_max] =
    {
        "850nm",
        "980nm",
        "1270nm",
        "1300nm",
        "1310nm",
        "1490nm",
        "1550nm",
        "1577nm",
        "1625nm",
        "1650nm",
};
static const char *const wave_str_buf_num[_wave_max] =
    {
        "850",
        "980",
        "1270",
        "1300",
        "1310",
        "1490",
        "1550",
        "1577",
        "1625",
        "1650",
};
static Cmd_Info_Type s_cmd_info;

/*********************************static code**********************************/
static int start(Cmd_Info_Type info)
{
    return MS_EOK;
}
_MSG_FUN(start, "0");

static int end(Cmd_Info_Type info)
{
    return MS_EOK;
}
_MSG_FUN(end, "0.end");

/**
 * @brief  convert __DATE__ to yymmdd
 * @param  const char* p_str,Pre-Processing __DATE__
 *         data as "Aug 27 2013" "Aug  1 2013"
 * @param  char* yymmdd,lenght 3 Bytes for year,month,day
 * @retval None
 */
static void getYYMMDDFromStr(const char *p_str, char *yymmdd)
{
    char month = 0;
    char lenght = strlen(p_str);

    if (lenght != 11)
    {
        return;
    }

    for (month = 0; month < 12; month++)
    {
        if (0 == memcmp(&s_cMonth[month * 3], p_str, 3))
        {
            month++;
            break;
        }
    }

    *yymmdd++ = (p_str[9] - '0') * 10 + (p_str[10] - '0');
    *yymmdd++ = month;
    if ((p_str[4] < '0') || (p_str[4] > '3')) // over 01~31
    {
        *yymmdd = p_str[5] - '0';
    }
    else
    {
        *yymmdd = (p_str[4] - '0') * 10 + (p_str[5] - '0');
    }
}

/**
 * @function: msg_split
 * @description: 字符分割
 * @param {*}
 * @return {*}
 * @author: lzc
 */
char old_temp_data = '1';
static int msg_split(const char *data, Cmd_Info_Type *info_buf)
{
    u8 position = 0;
    u8 cmd_num = 0;
    u8 cmd_len = 0;
    u8 cmd_position = 0;
    u8 data_num = 0;
    u8 data_len = 0;
    u8 msg_len = 0;
    u8 get_cmd_step = 0;
    const char *ptr;

    rt_enter_critical();
    memset(info_buf, 0, sizeof(Cmd_Info_Type));
    msg_len = strlen(data) + 1;
    ptr = data;

    while (position < msg_len)
    {
        switch (get_cmd_step)
        {
        case 0: //命令
            info_buf->cmd[cmd_len] = *ptr;
            cmd_len++;
            if (cmd_len - cmd_position - 1 >= MSG_CMD_LEN)
            {
                info_buf->cmd[cmd_len] = '_';
                cmd_position = cmd_len;
                get_cmd_step = 1; //一个命令只接受三个字符
                break;
            }
            if (*(ptr + 1) == '[')
            {
                info_buf->cmd[cmd_len] = '_';
                cmd_position = cmd_len;
                ptr++;
                get_cmd_step = 2;
            }
            else if (*(ptr + 1) == ' ')
            {
                info_buf->cmd[cmd_len] = '_';
                cmd_position = cmd_len;
                get_cmd_step = 1; //一个命令只接受三个字符
            }
            else if (*(ptr + 1) == '\0')
            {
                info_buf->cmd[cmd_len] = '\0';
                get_cmd_step = 1; //一个命令只接受三个字符
            }
            break;
        case 1: //忽略部分
            if (*ptr == ' ')
            {
                get_cmd_step = 0;
                cmd_num++;
                cmd_len++;
                if (cmd_num >= MSG_MAX_ARGS) //命令太多
                {
                    return MS_EFULL;
                }
            }
            else if (*ptr == '[')
            {
                // 蓝牙接收数据错误的处理
                // 没有接收到数据区 即 get history[]
                if (*(ptr + 1) == ']')
                {
                    get_cmd_step = 2;
                    data_len = 0;
                    info_buf->data[data_num][data_len] = old_temp_data + 1;
                    data_len++;
                }
                else
                {
                    get_cmd_step = 2;
                    data_len = 0;
                }
            }
            else if (*ptr == '\0')
            {
                info_buf->cmd[cmd_len] = '\0';
                return MS_EOK;
            }
            // 蓝牙接收数据错误的处理
            // 没有接收到数据区 即 get historyx]
            else if (*(ptr) >= '0' && *(ptr) <= '9')
            {
                get_cmd_step = 2;
                data_len = 0;
                info_buf->data[data_num][data_len] = *ptr;
                data_len++;
            }
            break;
        case 2: //参数
            // 右方括号
            if (*ptr == ']')
            {
                if (*(ptr + 1) == '\0') //结束
                {
                    info_buf->data[data_num][data_len] = '\0';
                    info_buf->cmd[cmd_len] = '\0';
                    return MS_EOK;
                }
                else if (*(ptr + 1) == ' ')
                {
                    ptr++;
                    get_cmd_step = 0;
                    cmd_num++;
                    cmd_len++;
                    data_len = 0;
                    data_num++;
                    if (cmd_num >= MSG_MAX_ARGS) //命令太多
                    {
                        return MS_EFULL;
                    }
                }
                else
                {
                    //多接受一条蓝牙数据，清除对应数据源
                    memset((void *)g_msg_buf, 0, sizeof(&g_msg_buf));
                    return MS_EOK;
                }
            }
            // 左方括号 以及 数据
            else
            {
                // 先判断是否是数字
                if (*(ptr) >= '0' && *(ptr) <= '9')
                {
                    old_temp_data = *(ptr);
                }
                info_buf->data[data_num][data_len] = *ptr;
                if (data_len++ >= (MSG_ARG_LEN - 1))
                {
                    info_buf->data[data_num][data_len] = '\0';
                    return MS_EFULL; //参数太长
                }
            }
            break;
        }
        ptr++;
        position++;
        rt_exit_critical();
    }

    return MS_EOK;
}

/**
 * @function: msg_parse
 * @brief: msg消息解析
 * @param {const char *data}
 * @return {*}
 * @author: lzc
 */
int msg_parse(const char *data)
{
    rt_err_t err;
    const _init_msg *init_msg_p = NULL;
    memset((u8 *)&s_cmd_info, 0, sizeof(Cmd_Info_Type));
    err = msg_split(data, &s_cmd_info);
    if (err != MS_EOK)
    {
        MSG_OUTPUT("err! msg isn't legal !\n");
        return -1;
    }
    // 接收处理
    for (init_msg_p = &_imp_start; init_msg_p < &_imp_end; init_msg_p++)
    {
        if (s_cmd_info.cmd[0] == '*') //此处是进行获取版本号以及设备类型的兼容
        {
            for (int i = 0; i < 4; i++)
            {
                s_cmd_info.cmd[i] = s_cmd_info.cmd[i + 1];
            }
        }
        // 完整性判断
        if (strcmp((const char *)(init_msg_p->name), (const char *)(&s_cmd_info.cmd[0])) == 0)
        {
            init_msg_p->msg_port_fun(s_cmd_info);
            MSG_OUTPUT("\n"); //## \n
            return MS_EOK;
        }
    }
    //错误命令，需要抛弃.返回上次的数据
    // if (strcmp((const char *)(init_msg_p->name), (const char *)(&s_cmd_info.cmd[0])) != 0)
    //{
    //    return MS_EOK;
    //}
    return MS_EOK;
}

/********************************************************************************************/
/******************************************user code*****************************************/
//////////////////////////////////////////通用命令/////////////////////////////////////////////
/**
 * @function: idn
 * @description: 查询仪表型号
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int idn(Cmd_Info_Type info)
{
    MSG_OUTPUT("FHP2A04");
    return MS_EOK;
}
IMPORT_MSG_FUN(idn);

/**
 * @function: rst
 * @description: 软复位
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int rst(Cmd_Info_Type info)
{
    MSG_OUTPUT("ok");
    JumpToBL(BL_FLASH_ADDRESS);
    // NVIC_SystemReset();
    return MS_EOK;
}
IMPORT_MSG_FUN(rst);

/**
 * @function: ver
 * @description: 查询版本
 * @param {*}
 * @return {*}
 * @author: lzc
 */
// NOTE: 此处有溢出风险，进行修复
static int ver(Cmd_Info_Type info)
{
    char year, month, day = 0;
    MSG_OUTPUT("\nSoftVer:V0.3.8 ");
    // 合成数据
    getYYMMDDFromStr(COMPILE_DATE, (char *)&SoftVer_date_buf[2]);
    my_itoa(SoftVer_date_buf[2], &year);
    my_itoa(SoftVer_date_buf[3], &month);
    my_itoa(SoftVer_date_buf[4], &day);
    // 输出
    MSG_OUTPUT("20");
    MSG_OUTPUT(&year);
    MSG_OUTPUT("-");
    MSG_OUTPUT(&month);
    MSG_OUTPUT("-");
    MSG_OUTPUT(&day);

    MSG_OUTPUT("\nHardVer:V1.0.0 ");
    MSG_OUTPUT(HardVer_date_buf);

    return MS_EOK;
}
IMPORT_MSG_FUN(ver);

//////////////////////////////////////////读取命令/////////////////////////////////////////////

/**
 * @function: get_wav
 * @description: 读取当前波长
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int get_wav(Cmd_Info_Type info)
{
    MSG_OUTPUT(wave_str_buf[g_sys_flag.data.wave]);
    return MS_EOK;
}
IMPORT_MSG_FUN(get_wav);

/**
 * @function: get_pow
 * @description: 读取当前波长的功率
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int get_pow(Cmd_Info_Type info)
{
    float power = 0;
    char buf[10];
    power = data_optical_get(_unit_dbm);
    my_ftoa(power, buf);
    MSG_OUTPUT(buf);
    return MS_EOK;
}
IMPORT_MSG_FUN(get_pow);

/**
 * @function: get_vol
 * @description: 获取电压值
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int get_vol(Cmd_Info_Type info)
{

    u16 temp_val[1] = {0};
    // ADC_VAL
    temp_val[0] = get_vol_data();
    MSG_OUTPUT((const char *)temp_val);
    return MS_EOK;
}
IMPORT_MSG_FUN(get_vol);

/**
 * @function: get_his
 * @description: 获取历史记录
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int get_his(Cmd_Info_Type info)
{
    int temp = 0;
    Record_Type record;
    char buf[10];

    temp = atoi((const char *)(info.data[0]));
    if (temp == 0) //返回记录数量
    {
        old_temp_data = '1';
        my_itoa(record_history_amount(), buf);
        MSG_OUTPUT(buf);
    }
    else
    {
        if (temp > 0 && temp <= record_history_amount())
        {
            record = record_history_read(temp);
            MSG_OUTPUT(wave_str_buf[record.wavelength]); //波长
            MSG_OUTPUT("\n");
            my_ftoa(record.optical_power, buf);
            MSG_OUTPUT(buf); //功率
            MSG_OUTPUT("\n");
            my_ftoa(record.ref_power, buf);
            MSG_OUTPUT(buf); //参考值
            MSG_OUTPUT("\n");
            my_itoa(record.unit, buf);
            MSG_OUTPUT(buf); //单位,对应先关标志值
            MSG_OUTPUT("\n");
            if (record.freq > 0 && record.freq <= 3000)
            {
                my_itoa(record.freq, buf);
                MSG_OUTPUT(buf);
                MSG_OUTPUT("\n");
            }
        }
        else
        {
            MSG_OUTPUT("err");
        }
    }
    return MS_EOK;
}
IMPORT_MSG_FUN(get_his);

/**
 * @function: get_mac
 * @description: 获取mac地址
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int get_mac(Cmd_Info_Type info)
{
    char buf[20];
    record_bt_get_mac((u8 *)buf);
    MSG_OUTPUT(buf);
    return MS_EOK;
}
IMPORT_MSG_FUN(get_mac);

//////////////////////////////////////////设置命令/////////////////////////////////////////////

/**
 * @function: set_wav
 * @description: 设置当前波长
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_wav(Cmd_Info_Type info)
{
    for (int i = 0; i < _wave_max; i++)
    {
        if (rt_strcmp(wave_str_buf[i], (const char *)info.data[0]) == 0)
        {
            g_sys_flag.data.wave = (Wave_Enum)i;
            MSG_OUTPUT(wave_str_buf[g_sys_flag.data.wave]);
            return 0;
        }
    }
    MSG_OUTPUT("err");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_wav);

/**
 * @function: set_wav_sta
 * @description: 配置当前波长（980nm ...）set wave[] status[]
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_wav_sta(Cmd_Info_Type info)
{
    for (int i = 0; i < _wave_max; i++)
    {
        if (rt_strcmp(wave_str_buf_num[i], (const char *)info.data[0]) == 0)
        {
            g_sys_flag.data.wave = (Wave_Enum)i; 
            if (g_sys_flag.data.wave == _wave_980 || g_sys_flag.data.wave == _wave_1270 ||
                g_sys_flag.data.wave == _wave_1577 || g_sys_flag.data.wave == _wave_1650)
            {
                record_Seting_Wave_set(g_sys_flag.data.wave, atoi((const char *)(info.data[1]))); 
                g_Wave_Setting = record_Seting_Wave_get();
                MSG_OUTPUT("ok");
            }
            else
            {
                MSG_OUTPUT("err");
            } 
            return 0;
        }
    }
    MSG_OUTPUT("err");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_wav_sta);

/**
 * @function: set_pow
 * @description: 设置当前功率（整偏）
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_pow(Cmd_Info_Type info)
{
    float power = 0;
    if (g_sys_flag.work.mode == _mode_single)
    {
        power = atof((char *)info.data[0]);
        data_cal_coeff_set(power);
        MSG_OUTPUT("ok");
    }
    else
    {
        MSG_OUTPUT("err");
    }
    return MS_EOK;
}
IMPORT_MSG_FUN(set_pow);

/**
 * @function: set_his_res
 * @description: 清除所有记录
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_his_res(Cmd_Info_Type info)
{
    record_history_del_all();
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_his_res);

/**
 * @function: set_cal_res
 * @description: 清除所有系数
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_cal_res(Cmd_Info_Type info)
{
    _clr_liner_eeprom();
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_cal_res);

/**
 * @function: set_coe_res
 * @description: 清除波长系数
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_coe_res(Cmd_Info_Type info)
{
    u32 ff = 0xffffffff;
    eeprom_write_page_bytes(_return_liner_coeff_eeprom_addr(), (u8 *)&ff, _wave_max);
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_coe_res);

/**
 * @function: set_lin_res
 * @description: 清除线性系数
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_lin_res(Cmd_Info_Type info)
{
    _clr_liner_eeprom();
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_lin_res);

/**
 * @function: set_btn
 * @description: 设置蓝牙名称
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_btn(Cmd_Info_Type info)
{
    char bt_name_buf[30] = {0};
    char start[10] = {"AT+NAME="};
    char end[2] = {"\r\n"};
    char result_buf[50] = {0};
    char bt_name_lens = 0;
    for (char i = 0; i < sizeof(bt_name_buf); i++)
    {
        bt_name_buf[i] = info.data[0][i];
    }
    bt_name_lens = strlen(bt_name_buf);
    // 合成发送改变名称的字符串
    rt_memset(result_buf, 0, sizeof(result_buf));
    strcat(result_buf, start);
    strcat(result_buf, bt_name_buf);
    strcat(result_buf, end);
    if (bt_name_lens > 0)
    {
        // 蓝牙设定名字
        // TODO: 增加蓝牙设定名字接口,把蓝牙名称增加到30个字节.已经完成。
        // 先关闭蓝牙的电源
        set_pin_bt(false);
        // 清除标志位
        record_bt_set(RT_FALSE);
        // 打开电源
        set_pin_bt(true);
        rt_thread_mdelay(3000);
        //改名称,可配置
        for (char i = 0; i < strlen(result_buf); i++)
        {
            putchar_usart1(result_buf[i]);
        }
        g_msg_rt_info.num = 0;
        g_msg_rt_info.end = 0;
        rt_thread_mdelay(3000);
        // 关闭蓝牙电源
        set_pin_bt(false);
        record_bt_set(RT_TRUE);
    }
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_btn);

#if VFL_USE
/**
 * @function: set_vfl
 * @brief: 设置红光源
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_vfl(Cmd_Info_Type info)
{
    if (info.data[0][0] == '1')
    {
        g_sys_flag.work.module.vfl.on = !g_sys_flag.work.module.vfl.on;
        if (g_sys_flag.work.module.vfl.on)
        {
            set_pin_vfl(true);
        }
        else
        {
            set_pin_vfl(false);
            g_sys_flag.work.module.vfl.hz = 0;
        }
    }
    else if (info.data[0][0] == '2')
    {
        if (!g_sys_flag.work.module.vfl.on)
            return MS_ERROR;
        g_sys_flag.work.module.vfl.hz = !g_sys_flag.work.module.vfl.hz;
    }
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_vfl);
#endif

/**
 * @function: set_led
 * @description: 设置背光
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int set_led(Cmd_Info_Type info)
{
    lcd_back_light(1);
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(set_led);

//////////////////////////////////////////校准命令/////////////////////////////////////////////
/**
 * @function: cal_sta
 * @description: 开始校准
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int cal_sta(Cmd_Info_Type info)
{
    rt_base_t level;
    level = rt_hw_interrupt_disable();
    g_sys_flag.work.mode = _mode_cal;
    g_sys_flag.work.cal.start = 0;
    rt_hw_interrupt_enable(level);
    while (g_sys_flag.work.cal.start == 0)
    {
        rt_thread_mdelay(10);
    }
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(cal_sta);

/**
 * @function: cal_sto
 * @description: 结束校准
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int cal_sto(Cmd_Info_Type info)
{
    rt_base_t level;
    level = rt_hw_interrupt_disable();
    g_sys_flag.work.mode = _mode_single;
    rt_hw_interrupt_enable(level);
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(cal_sto);
/**
 * @function: cal_sav
 * @description: 保存校准值
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int cal_sav(Cmd_Info_Type info)
{
    data_cal_linear_save();
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(cal_sav);

/**
 * @function: cal_lev
 * @description: 校准模式校准
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int cal_lev(Cmd_Info_Type info)
{
    int temp = 0;
    if ((g_sys_flag.work.cal.start == 1) && (g_sys_flag.work.mode == _mode_cal))
    {
        temp = atoi((const char *)(info.data[0]));
        if (temp >= 0 && temp <= CH_MAX_NUM)
        {
            data_ch_set(temp);
            MSG_OUTPUT((const char *)info.data[0]);
        }
        else
        {
            MSG_OUTPUT("err");
        }
    }
    else
    {
        MSG_OUTPUT("err");
    }
    return MS_EOK;
}
IMPORT_MSG_FUN(cal_lev);
/**
 * @brief 校准点,具有确定性
 * @param
 * @return
 */
static int cal_poi_pow(Cmd_Info_Type info)
{
    rt_base_t level;
    int temp = 0;
    float mw;
    if ((g_sys_flag.work.cal.start == 1) && (g_sys_flag.work.mode == _mode_cal))
    {
        level = rt_hw_interrupt_disable();
        /*1.刷新数据*/
        temp = atoi((const char *)(info.data[0]));
        if (temp == 1 || temp == 2)
        {
            mw = pow(10, atof((const char *)(info.data[1])) / 10.0);
            data_cal_linear_set(temp, mw);
            /*2.返回*/
            rt_hw_interrupt_enable(level);
            MSG_OUTPUT("ok");
        }
        else
        {
            MSG_OUTPUT("err");
        }
    }
    else
    {
        MSG_OUTPUT("err");
    }
    return MS_EOK;
}
IMPORT_MSG_FUN(cal_poi_pow);

//////////////////////////////////////////系统命令/////////////////////////////////////////////

/**
 * @function: sys_off
 * @description: 自动关机
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int sys_off(Cmd_Info_Type info)
{
    // 关闭自动关机
    if (info.data[0][0] == '0')
    {
        record_off_time_set(AUTO_OFF_FLAG);
        rt_thread_mdelay(100);
        // 显示的标志变化
        MSG_OUTPUT("ok");
    }
    else
    {
        record_off_time_set(AUTO_OFF_TIME);
        rt_thread_mdelay(100);
        // 显示的标志变化
        MSG_OUTPUT("ok");
    }
    return MS_EOK;
}
IMPORT_MSG_FUN(sys_off);

/**
 * @function: sys_upg
 * @description: 系统升级
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int sys_upg(Cmd_Info_Type info)
{
    rt_hw_interrupt_disable();
    // 1.解锁flash
    if (READ_BIT(FLASH->CR, FLASH_CR_LOCK) != RESET)
    {
        /* Authorize the FLASH Registers access */
        WRITE_REG(FLASH->KEYR, FLASH_KEY1);
        WRITE_REG(FLASH->KEYR, FLASH_KEY2);
        /* Verify Flash is unlocked */
        if (READ_BIT(FLASH->CR, FLASH_CR_LOCK) != RESET)
        {
            return MS_ERROR;
        }
    }
    // 2.写升级标志
    /* Proceed to program the new data */
    SET_BIT(FLASH->CR, FLASH_CR_PG);
    /* Write data in the address */
    *(__IO uint16_t *)MBL_UP_FLAG_ADR = (u16)(MBL_UP_FLAG_VAL & 0xffff);
    SET_BIT(FLASH->CR, FLASH_CR_PG);
    /* Write data in the address */
    *(__IO uint16_t *)(MBL_UP_FLAG_ADR + 2) = (u16)(MBL_UP_FLAG_VAL >> 16);
    MSG_OUTPUT("ok\n");

    // NVIC_SystemReset();
    JumpToBL(BL_FLASH_ADDRESS);
    return MS_EOK;
}
IMPORT_MSG_FUN(sys_upg);

/**
 * @function: sys_buz
 * @description: 蜂鸣器
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static int sys_buz(Cmd_Info_Type info)
{
    // 关闭蜂鸣器
    if (info.data[0][0] == '0')
    {
        g_sys_flag.work.module.buzz = 1;
        MSG_OUTPUT("ok");
    }
    else
    {
        g_sys_flag.work.module.buzz = 0;
        MSG_OUTPUT("ok");
    }
    return MS_EOK;
}
IMPORT_MSG_FUN(sys_buz);

//////////////////////////////////////////按键命令/////////////////////////////////////////////
/**
 * @function:key_set
 * @brief:模拟按键按下
 * @param {*}
 * @return {*}
 * @author: lzc
 */
u32 key_val = 0;
static int key_set(Cmd_Info_Type info)
{
    if (info.data[0][0] != 0x00 && g_sys_flag.work.mode != _mode_max)
    {
        // 单击
        if (info.data[0][0] == 's')
        {
            key_val = atoi((const char *)(info.data[0] + 1));
            key_deal(key_val);
        }
        // 长按
        else if (info.data[0][0] == 'l')
        {
            key_val = atoi((const char *)(info.data[0] + 1)) + 9;
            key_deal(key_val);
        }
    }
    MSG_OUTPUT("ok");
    return MS_EOK;
}
IMPORT_MSG_FUN(key_set);
