/**
 * \file my_display.c
 * \brief 协调其他模块,集中判断显示内容
 * \author  mirlee
 * \version 0.1
 * \date
 */
#include "my_gpio.h"
#include "my_tim.h"
#include "my_data.h"
#include "my_lcd.h"
#include "my_record.h"
#include "my_display.h"

//
extern rt_mutex_t g_mutex_1;

volatile bool g_auto_off_flag;
static const u16 c_wave_num[_wave_max] = {850, 980, 1270, 1300, 1310, 1490, 1550, 1577, 1625, 1650};
static u16 s_dis_record_num = 0; //显示的记录序号
static Record_Type s_record_buf;

/**
 * @brief 显示光功率相关,包括功率值,单位,频率等
 * @param
 * @return
 */
static void display_optical_data(u8 flag)
{
    float power = 0;
    /*波长*/
    lcd_display_wavelength(c_wave_num[g_sys_flag.data.wave], true);
    /*功率*/
    power = data_optical_get(g_sys_flag.data.unit);
    if (flag == _mode_single)
        lcd_display_power(power, g_sys_flag.data.unit, g_sys_flag.data.opt, 4);
    else if (flag == _mode_twin)
        lcd_display_power(power, g_sys_flag.data.unit, g_sys_flag.data.opt, 3);
    else if (flag == _mode_cal)
        lcd_display_power(power, g_sys_flag.data.unit, g_sys_flag.data.opt, 1);
    /*频率*/
}

/**
 * @brief 显示电池,usb,适配器,AUTO-OFF
 *        scan_charge_battery  50ms执行一次
 * @param
 * @return
 */
static void display_sign_flag(void)
{
    u8 usb_sta = 0, bat_sta = 0;
    scan_charge_battery(&usb_sta, &bat_sta);
    lcd_display_battery(bat_sta);
    lcd_display_usb(usb_sta);
    //检测和显示AUTO-OFF
    lcd_display_shutdown(g_auto_off_flag);
    scan_off_time();
    //显示蜂鸣器
    lcd_display_buzz(!g_sys_flag.work.module.buzz);
    //显示红光标志
    lcd_display_vfl(g_sys_flag.work.module.vfl.on);
    //显示蓝牙 wifi标志
    lcd_display_bluetooth(g_sys_flag.work.module.bt);
}

static void display_cal_data(void)
{
    // u32 temp;
    // data_vol_get(&temp);
    // lcd_display_vol(temp);
    display_optical_data(_mode_cal);
    lcd_display_count(data_ch_get(), false, false);
}

/***********************显示历史记录相关*************************/
void display_history_start(void)
{
    if (record_history_amount() >= 1)
    {
        g_sys_flag.work.mode = _mode_history;
        s_dis_record_num = 1;
        /*读记录到缓存*/
        s_record_buf = record_history_read(s_dis_record_num);
    }
}

void display_history_stop(void)
{
    /*清除一下count位置*/
    lcd_display_count(0, false, false);
    g_sys_flag.work.mode = _mode_single;
}
/**
 * @brief 记录加1，或加100.
 * @param flag: 0:加1; 1:加100.
 * @return
 */
void display_history_up(u8 flag)
{
    if (flag == 0)
    {
        if (s_dis_record_num < record_history_amount())
        {
            s_dis_record_num++;
        }
    }
    else
    {
        if (s_dis_record_num + 100 < record_history_amount())
        {
            s_dis_record_num += 100;
        }
        else
        {
            s_dis_record_num = record_history_amount();
        }
    }
    /*读记录到缓存*/
    s_record_buf = record_history_read(s_dis_record_num);
}

void display_history_down(u8 flag)
{
    if (flag == 0)
    {
        if (s_dis_record_num > 1)
        {
            s_dis_record_num--;
        }
    }
    else
    {
        if (s_dis_record_num > 100)
        {
            s_dis_record_num -= 100;
        }
        else
        {
            s_dis_record_num = 1;
        }
    }
    /*读记录到缓存*/
    s_record_buf = record_history_read(s_dis_record_num);
}

void display_history_del_one(void)
{
    u16 temp = 0;
    u8 work_temp = 0;
    if (s_dis_record_num == record_history_amount())
    {
        temp = s_dis_record_num;
        s_dis_record_num--;
    }
    else
    {
        temp = s_dis_record_num;
    }
    //删除一条,显示横线，保持1s
    lcd_display_line(1, 0);
    lcd_display_count(temp, false, true);
    lcd_refresh_buf();
    record_history_del_single(temp);
    rt_thread_mdelay(1000); //显示1s
    if (record_history_amount() == 0)
    {
        work_temp = _mode_single;
        s_dis_record_num = 0;
        lcd_display_count(0, false, false);
        lcd_display_line(1, 1);
        lcd_refresh_buf();
    }
    else
    {
        work_temp = _mode_history;
        s_record_buf = record_history_read(s_dis_record_num); //读记录到缓存
    }

    lcd_display_line(0, 0);
    lcd_refresh_buf();
    //阻塞
    g_sys_flag.work.brief = _dis_max;
    g_sys_flag.work.mode = (Work_Mode_Enum)work_temp;
}

void display_history_del_all(void)
{
    lcd_display_line(1, 1);
    lcd_refresh_buf();
    record_history_del_all();
    rt_thread_mdelay(1000); //显示2s
    lcd_display_count(0, false, false);
    lcd_display_line(0, 0);
    lcd_refresh_buf();
    //阻塞
    g_sys_flag.work.brief = _dis_max;
    g_sys_flag.work.mode = _mode_single;
}
/**
 * @brief 保存当前状态到eep
 *        包括功率值,单位,频率等
 * @param
 * @return
 */
void display_history_save_one(void)
{
    if (g_sys_flag.data.opt == _optical_normal)
    {
        s_record_buf.optical_power = data_optical_get(g_sys_flag.data.unit);
        s_record_buf.wavelength = g_sys_flag.data.wave;
        data_ref_get(&s_record_buf.ref_power, s_record_buf.wavelength);
        s_record_buf.unit = g_sys_flag.data.unit;
        s_record_buf.freq = data_freq_get(g_optical_freq_num);
        if (record_history_save(&s_record_buf) != 0)
        {
            /*显示横线*/
            lcd_display_count(0, false, true);
            lcd_display_line(0, 1);
            lcd_refresh_buf();
        }
        else
        {
            /*显示序号*/
            s_dis_record_num = record_history_amount();
            lcd_display_count(s_dis_record_num, false, true);
            lcd_refresh_buf();
        }
        rt_thread_mdelay(1000); //延时使保存标志显示1s
        lcd_display_count(0, false, false);
        lcd_refresh_buf();
        //阻塞
        g_sys_flag.work.brief = _dis_max;
        g_sys_flag.work.mode = g_sys_flag.work.mode_re;
    }
}

/**
 * @brief 显示记录,包括波长，功率值,单位,频率,序号等
 *        100ms执行一次
 * @param
 * @return
 */
static void display_history_data(void)
{
    /*显示波长*/
    lcd_display_wavelength(c_wave_num[s_record_buf.wavelength], true);
    /*功率*/
    lcd_display_power(s_record_buf.optical_power, s_record_buf.unit, _optical_normal, 0);
    /*频率*/
    // lcd_display_count(s_record_buf.freq);
    /*序号*/
    lcd_display_count(s_dis_record_num, false, true);
}

/***********************显示参考值相关*************************/
/**
 * @brief 设置当前示数为参考值
 *        同时切换单位为db
 * @param
 * @return
 */
void display_ref_set_data(void)
{
    float temp = 0;
    temp = data_optical_get(_unit_dbm);
    data_ref_set(temp, g_sys_flag.data.wave);
    lcd_display_power(temp, _unit_db, _optical_normal, 2);
    lcd_refresh_buf();
    rt_thread_mdelay(1000);
    //阻塞
    g_sys_flag.work.brief = _dis_max;
    g_sys_flag.work.mode = g_sys_flag.work.mode_re;
}
/**
 * @brief 显示参考值
 * @param
 * @return
 */
void display_ref_show_data(void)
{
    float temp = 0;
    temp = record_ref_get(g_sys_flag.data.wave);
    lcd_display_power(temp, _unit_db, _optical_normal, 2);
    lcd_refresh_buf();
    rt_thread_mdelay(1000);
    //阻塞
    g_sys_flag.work.brief = _dis_max;
    g_sys_flag.work.mode = g_sys_flag.work.mode_re;
}

void display_control_task(void)
{
    u16 temp_f = 0;
    u8 usb, bat;
    rt_mutex_take(g_mutex_1, RT_WAITING_FOREVER);
    switch (g_sys_flag.work.mode)
    {
    case _mode_single:
        display_sign_flag();
        display_optical_data(g_sys_flag.work.mode);
        temp_f = data_freq_get(g_optical_freq_num);
        if ((temp_f > 260 && temp_f < 280) || (temp_f > 990 && temp_f < 1010) || (temp_f > 1990 && temp_f < 2010))
        {
            lcd_display_count(temp_f, (temp_f > 0), false);
        }
        else
            lcd_display_count(0, (temp_f > 0), false);
        lcd_refresh_buf();
        data_task();
        break;
    case _mode_twin:
        display_sign_flag();
        display_optical_data(g_sys_flag.work.mode);
        temp_f = data_freq_get(g_optical_freq_num);
        if ((temp_f > 260 && temp_f < 280) || (temp_f > 990 && temp_f < 1010) || (temp_f > 1990 && temp_f < 2010))
        {
            lcd_display_count(temp_f, (temp_f > 0), false);
        }
        else
            lcd_display_count(0, (temp_f > 0), false);
        lcd_refresh_buf();
        data_task();
        break;
    case _mode_history:
        display_sign_flag();
        display_history_data();
        lcd_refresh_buf();
        rt_thread_mdelay(60);
        break;
    case _mode_cal:
        display_sign_flag();
        data_task();
        display_cal_data();
        lcd_refresh_buf();
        break;
    case _mode_brief:
        display_sign_flag();
        switch (g_sys_flag.work.brief)
        {
        case _dis_ref_get:
            display_ref_show_data();
            break;
        case _dis_ref_set:
            display_ref_set_data();
            break;
        case _dis_his_one:
            display_history_del_one();
            break;
        case _dis_his_all:
            display_history_del_all();
            break;
        case _dis_his_save:
            display_history_save_one();
            break;
        case _dis_max:
            break;
        }
        break;
    case _mode_max:
        my_lcd_clear();
        rt_thread_mdelay(60);
        scan_charge_battery(&usb, &bat);
        if (usb)
        {
            lcd_display_battery(bat);
            lcd_display_usb(usb);
        }
        lcd_refresh_buf();
        break;
    }
    rt_mutex_release(g_mutex_1);
}
