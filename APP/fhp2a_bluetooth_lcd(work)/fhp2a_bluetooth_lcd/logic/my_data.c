/**
 * \file my_data.c
 * \brief 功率计算
 * \author  mirlee
 * \version 0.1
 * \date
 */
#include "my_tim.h"
#include "my_gpio.h"
#include "my_adc.h"
#include "my_record.h"
#include "my_data.h"
#include "my_api.h"
#include "my_usart.h"
/****************************预定义阈值等参数**********************************/

/*换挡电压上下限*/
#if defined(MY_DEVICE_A)
#define POWER_LOW_850 (3.98e-7)
#define POWER_LOW (1e-7)
#define POWER_HIGH (1e1)

#define ADC_LOW ((int)180)   // 1档 -10dbm   0.108    -9dbm 0.136   -3dbm  0.538
#define ADC_HIGH ((int)1800) // 1档 0dbm   1.077     1dbm 1.353
#elif defined(MY_DEVICE_B)
#define POWER_LOW_850 (3.98e-5)
#define POWER_LOW (1e-5)
#define POWER_HIGH (3.981e2)

#define ADC_LOW ((float)0.1)  // 1档 -10dbm   0.108    -9dbm 0.136   -3dbm  0.538
#define ADC_HIGH ((float)1.7) // 1档 0dbm   1.077     1dbm 1.353
#else
#error please define device type
#endif

/****************************#默认校准数据**********************************/
#if defined(MY_DEVICE_A)

//  7dbm        3dbm        -3dbm       -7dbm
//-13dbm    -17dbm   -23dbm  -27dbm
//-33dbm    -37dbm   -43dbm  -47dbm
//-53dbm    -57dbm   -63dbm  -67dbm
static const float s_cal_power_default[16] = {5.012e0, 1.995e0, 5.012e-1, 1.995e-1,
                                              5.012e-2, 1.995e-2, 5.012e-3, 1.995e-3,
                                              5.012e-4, 1.995e-4, 5.012e-5, 1.995e-5,
                                              5.012e-6, 1.995e-6, 5.012e-7, 1.995e-7};

static const int s_cal_voltage_default[16] = {1765, 706, 1590, 637,
                                              1323, 527, 1298, 516,
                                              1285, 512, 1413, 564,
                                              1290, 509, 1271, 484};
//////////////                                            850nm  980nm  1270nm 1300nm 1310nm  1490nm 1550nm 1577nm 1625nm 1650nm
static const float s_cal_coefficient_default[_wave_max] = {4.901, 1.000, 1.166, 1.166, 1.130, 1.000, 1.00, 1.166, 1.067, 1.166};

#elif defined(MY_DEVICE_B)
static const float s_cal_power_default[16];
static const int s_cal_voltage_default[16];
static const float s_cal_coefficient_default[_wave_max];
#else
#error please define device type
#endif
/****************************缓存校准数据**********************************/
ALIGN(4)
static float s_cal_power[16];
ALIGN(4)
static int s_cal_voltage[16];
ALIGN(4)
static float s_cal_coefficient[_wave_max];
/****************************标志和变量**********************************/
ALIGN(4)
static float s_optical_power_data;
ALIGN(4)
static float s_optical_ref_data[_wave_max];
ALIGN(4)
static int s_optical_vol_data;
ALIGN(4)
static u8 s_optical_ch_num;
/****************************外部变量**********************************/
ALIGN(8)
volatile Sys_Flag_Type g_sys_flag;
char g_Wave_Setting = 0; // 全局波长设置
/**
 * @brief 由电压计算功率
 * @param
 * @return
 */
static __inline float calculate_vol_2_power(u32 vol, u8 ch, u8 wave)
{
    float y, x;
    float p, v;
    float power_value = 0;
    y = s_cal_power[ch * 2 + 1] - s_cal_power[ch * 2];
    x = s_cal_voltage[ch * 2 + 1] - s_cal_voltage[ch * 2];
    p = s_cal_power[ch * 2 + 1];
    v = s_cal_voltage[ch * 2 + 1];
    power_value = (y / x * (vol - v) + p) * s_cal_coefficient[wave];
    if (power_value < 0)
        power_value = POWER_LOW;
    return power_value;
}
/**
 * @brief 调整合适的档位,获取电压,计算功率
 * @param
 * @return
 */
static float calculate_power_data(u8 wav)
{
    u32 vol_optical = 0;
    u32 vol_temp = 0;
    u32 temp = 0;
    u16 times = 0;
    u8 optical_ch_high_flag = 0;
    u8 optical_ch_low_flag = 0;
    char buf[5];
    while (1)
    {
        times++;
        rt_thread_delay(1);
        temp = get_vol_data();

        vol_temp += temp;
        if (times >= 50)
        {
            times = 0;
            vol_optical = (u32)((float)vol_temp / (float)50.0);
            my_itoa(vol_optical, buf);
            vol_temp = 0;
            //校准模式跳过切档
            if (g_sys_flag.work.mode == _mode_cal)
            {
                s_optical_vol_data = vol_optical;
                g_sys_flag.work.cal.start = 1;
                goto get_power_data_end;
            }

            //判断档位要避免陷入死循环
            if ((vol_optical < ADC_LOW) && (s_optical_ch_num < CH_MAX_NUM))
            {
                if (optical_ch_high_flag == 1)
                {
                    goto get_power_data_end;
                }
                optical_ch_low_flag = 1;
                s_optical_ch_num++;
                set_pin_channel(s_optical_ch_num);
            }
            else if ((vol_optical > ADC_HIGH) && (s_optical_ch_num > 0))
            {
                if (optical_ch_low_flag == 1)
                {
                    goto get_power_data_end;
                }
                optical_ch_high_flag = 1;
                s_optical_ch_num--;
                set_pin_channel(s_optical_ch_num);
            }
            else
            {
                goto get_power_data_end;
            }
        }
    }

get_power_data_end:
    return calculate_vol_2_power(vol_optical, s_optical_ch_num, wav);
}

static u8 s_opm_tick;
static float power_temp = 0;
void data_task(void)
{

    if (g_sys_flag.work.mode != _mode_history)
    {
        s_opm_tick++;
        if (s_opm_tick < 5)
        {
            power_temp += calculate_power_data(g_sys_flag.data.wave);
        }
        else
        {
            s_optical_power_data = power_temp / 5.0;
            power_temp = 0;
            s_opm_tick = 0;
            if (g_sys_flag.data.wave == _wave_850)
            {
                if (s_optical_power_data > POWER_HIGH)
                    g_sys_flag.data.opt = _optical_high;
                else if (s_optical_power_data < POWER_LOW_850)
                    g_sys_flag.data.opt = _optical_low;
                else
                    g_sys_flag.data.opt = _optical_normal;
            }
            else
            {
                if (s_optical_power_data > POWER_HIGH)
                    g_sys_flag.data.opt = _optical_high;
                else if (s_optical_power_data < POWER_LOW)
                    g_sys_flag.data.opt = _optical_low;
                else
                    g_sys_flag.data.opt = _optical_normal;
            }
        }
    }
}

/**
 * @brief 初始化数据处理，档位初始化，校准数据初始化，波长初始化
 */
void data_init(void)
{
    int flag;
    g_sys_flag.work.mode = _mode_max;
    g_sys_flag.data.unit = _unit_dbm;
    //档位初始化,gpio初始化时，会把档位切换到3
    s_optical_ch_num = 3;
    set_pin_channel(s_optical_ch_num);
    //开机波长初始化
    //读取eeprom内保存的波长
    g_sys_flag.data.wave = (Wave_Enum)record_PowerON_Wave_get();
    //读取设置的波长（980/1270/1577/1650） 
    g_Wave_Setting = record_Seting_Wave_get();
    // rt_kprintf("Wave settings is %d\n", g_Wave_Setting);
    //读取参考值
    for (int i = 0; i < _wave_max; i++)
    {
        s_optical_ref_data[i] = record_ref_get(i);
    }
    //校准数据初始化

    flag = record_cal_read_all((float *)&s_cal_power[0], sizeof(s_cal_power),
                               (int *)&s_cal_voltage[0], sizeof(s_cal_voltage),
                               (float *)&s_cal_coefficient[0], sizeof(s_cal_coefficient));

    if (flag == -1 || flag == -3)
    {
        rt_memcpy(((u8 *)&s_cal_power[0]), (u8 *)&s_cal_power_default[0], sizeof(s_cal_power));
        rt_memcpy((u8 *)&s_cal_voltage[0], (u8 *)&s_cal_voltage_default[0], sizeof(s_cal_voltage));
#if defined(MY_DEVICE_B)
#error 添加大信号
#endif
    }
    if (flag == -2 || flag == -3)
    {
        rt_memcpy((u8 *)&s_cal_coefficient[0], (u8 *)&s_cal_coefficient_default[0], sizeof(s_cal_coefficient));
    }
}
/**
 * @function: data_optical_get
 * @brief: 获取功率值并且进行处理
 * @param {u8} unit 单位
 * @return {float} temp 功率的结果
 * @author: lzc
 */
float data_optical_get(u8 unit)
{
    float temp = 0;
    switch (unit)
    {
    case _unit_mw:
        temp = s_optical_power_data;
        break;
    case _unit_db:
        temp = 10.0 * log10(s_optical_power_data);
        temp -= s_optical_ref_data[g_sys_flag.data.wave];
        break;
    case _unit_dbm:
        temp = 10.0 * log10(s_optical_power_data);
        break;
    case _unit_max:
        break;
    }
    return temp;
}

void data_vol_get(u32 *vol)
{
    *vol = s_optical_vol_data;
}

void data_ref_get(float *ref, u8 wave)
{
    *ref = s_optical_ref_data[wave];
}
/**
 * @brief 设置某个波长的参考值，同时保存
 * @param ref:单位为dbm
 * @return
 */
void data_ref_set(float ref, u8 wave)
{
    s_optical_ref_data[wave] = ref;
    record_ref_set(ref, wave);
}
/**
 * @brief 判断频率的信息
 * @param 关于光维老光源产品TWIN模式频率:
        850: 220Hz
        1300:200Hz
        1310:180Hz
        1490:140Hz
        1550:100Hz
        1625:60Hz
 * @return
 */
u16 data_freq_get(u16 freq)
{
    u16 temp = 0;
    /*判断TWIN模式频率*/
    if (g_sys_flag.work.mode == _mode_twin)
    {
        if (g_optical_freq_num > 210 && g_optical_freq_num < 230) // 850
        {
            g_sys_flag.data.wave = _wave_850;
        }
        else if (g_optical_freq_num > 190 && g_optical_freq_num < 210) // 1300
        {
            g_sys_flag.data.wave = _wave_1300;
        }
        else if (g_optical_freq_num > 170 && g_optical_freq_num < 190) // 1310
        {
            g_sys_flag.data.wave = _wave_1310;
        }
        else if (g_optical_freq_num > 130 && g_optical_freq_num < 150) // 1490
        {
            g_sys_flag.data.wave = _wave_1490;
        }
        else if (g_optical_freq_num > 90 && g_optical_freq_num < 110) // 1550
        {
            g_sys_flag.data.wave = _wave_1550;
        }
        else if (g_optical_freq_num > 50 && g_optical_freq_num < 70) // 1625
        {
            g_sys_flag.data.wave = _wave_1625;
        }
    }
    else
    {
        temp = g_optical_freq_num;
        if ((temp > 3000) || (temp < 200))
        {
            temp = 0;
        }
        else if ((temp > 1950) && (temp < 2050))
        {
            temp = 2000;
        }
        else if ((temp > 950) && (temp < 1050))
        {
            temp = 1000;
        }
        else if ((temp > 260) && (temp < 280))
        {
            temp = 270;
        }
    }
    return temp;
}
/**
 * @brief 切换档位
 * @param ch: 档位，从0开始
 * @return
 */
void data_ch_set(u8 ch)
{
    s_optical_ch_num = ch;
    set_pin_channel(s_optical_ch_num);
}

/**
 * @function: data_ch_get
 * @description: 获取档位值
 * @param {*}
 * @return {u8 ch_num}
 * @author: lzc
 */
u8 data_ch_get(void)
{
    return s_optical_ch_num;
}

/**
 * @function: data_cal_linear_save
 * @description: 保存线性的校准
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void data_cal_linear_save(void)
{
    record_cal_linear_save(s_cal_power, sizeof(s_cal_power), s_cal_voltage, sizeof(s_cal_voltage));
    record_cal_coef_save(s_cal_coefficient, sizeof(s_cal_coefficient));
}

/**
 * @function: data_cal_coeff_set
 * @description: 波长校准
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void data_cal_coeff_set(float dbm)
{
    float mw;
    mw = pow(10.0, dbm / 10.0);
    s_cal_coefficient[g_sys_flag.data.wave] = mw * s_cal_coefficient[g_sys_flag.data.wave] / data_optical_get(_unit_mw);
    // 存储 整偏值
}

/**
 * @function: hex_data_cal_coeff_set
 * @description: 波长校准
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void hex_data_cal_coeff_set(float data)
{
    float power_f = 0;
    power_f = data_optical_get(_unit_mw);
    s_cal_coefficient[g_sys_flag.data.wave] = data * s_cal_coefficient[g_sys_flag.data.wave] / power_f;
    // 存储 整偏值
}

/**
 * @brief 刷新校准值，
 * @param power: 单位为mw
 * @return
 */
void data_cal_linear_set(u8 point, float power)
{
    int vol_temp = 0;
    s_cal_power[s_optical_ch_num * 2 + point - 1] = power;
    for (u8 i = 0; i < 10; i++)
    {
        vol_temp += get_vol_data();
    }
    vol_temp /= 10;
    s_cal_voltage[s_optical_ch_num * 2 + point - 1] = vol_temp;
}

/**
 * @brief 刷新校准值，
 * @param power: 单位为mw
 * @return
 */
void hex_data_cal_linear_set(u8 point, float power)
{
    int vol_temp = 0;
    s_cal_power[s_optical_ch_num * 2 + point - 1] = power;
    for (u8 i = 0; i < 10; i++)
    {
        vol_temp += get_vol_data();
    }
    vol_temp /= 10;
    s_cal_voltage[s_optical_ch_num * 2 + point - 1] = vol_temp;
}

/**
 * @function:_return_liner_vol_eeprom_addr
 * @description:返回线性的电压值存贮地址
 * @param {*}
 * @return {u16}
 * @author: lzc
 */
u16 _return_liner_vol_eeprom_size(void)
{
    u16 temp = 0;
    temp = sizeof(s_cal_voltage);
    return temp;
}

/**
 * @function:_return_liner_vol_eeprom_addr
 * @description:返回线性的电压值存贮地址
 * @param {*}
 * @return {u16}
 * @author: lzc
 */
u16 _return_liner_pow_eeprom_size(void)
{
    u16 temp = 0;
    temp = sizeof(s_cal_power);
    return temp;
}

/**
 * @function:_return_liner_vol_eeprom_addr
 * @description:返回线性的电压值存贮地址
 * @param {*}
 * @return {u16}
 * @author: lzc
 */
void _clr_liner_eeprom(void)
{
    record_cal_clr_all(sizeof(s_cal_power), sizeof(s_cal_voltage), sizeof(s_cal_coefficient));

    rt_memcpy(((u8 *)&s_cal_power[0]), (u8 *)&s_cal_power_default[0], sizeof(s_cal_power));
    rt_memcpy((u8 *)&s_cal_voltage[0], (u8 *)&s_cal_voltage_default[0], sizeof(s_cal_voltage));
    rt_memcpy((u8 *)&s_cal_coefficient[0], (u8 *)&s_cal_coefficient_default[0], sizeof(s_cal_coefficient));
}
