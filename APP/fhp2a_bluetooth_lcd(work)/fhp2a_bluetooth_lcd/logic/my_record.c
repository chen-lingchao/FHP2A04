/**
 * \file my_record.c
 * \brief 存储数据处理
 * \author  mirlee
 * \version 0.1
 * \date
 */

#include "my_eep.h"
#include "my_record.h" 
/*默认参数*/
#define BUF_DEFAULT_FFFF 0xffff
/*默认参数*/
#define BUF_DEFAULT_8F (0xffffffffUL)
/*最大记录数*/
#define RECORD_AMOUNTS (999UL)
/*记录数量占字节数*/
#define RECORD_AMOUNTS_BYTES 2
/*记录缓存结构体大小*/
#define RECORD_SIZE_BYTES 12
/********************************地址定义*******************************************/
/*检索信息存储起始地址*/
#define RECORD_FAT_ADDR (0x02)
/*记录存储起始地址*/
#define RECORD_DATA_ADDR (RECORD_FAT_ADDR + RECORD_AMOUNTS_BYTES * RECORD_AMOUNTS)
/*记录数量存储起始地址*/
#define RECORD_AMOUNT_ADDR (RECORD_DATA_ADDR + RECORD_SIZE_BYTES * RECORD_AMOUNTS)
/*参考值存储位置*/
#define REF_DATA_ADDR (RECORD_AMOUNT_ADDR + 0x03)

/*校准数据存储地址---30k字节开始--2000记录*/
#define CAL_POWER_ADR (REF_DATA_ADDR + 0x19)
#define CAL_VOL_ADR (CAL_POWER_ADR + 0x40) // 0x40
#define CAL_COEFF_ADR (CAL_VOL_ADR + 0x40) // 0x40

/*背光数据保存地址*/
#define BACK_LIGHT_ADR (CAL_COEFF_ADR + 0x30)
/*自动关机时间地址*/
#define AUTO_OFF_ADR (BACK_LIGHT_ADR + 0x4)
/*开机波长保存*/
#define WAV_OFF_ADR (AUTO_OFF_ADR + 0x4)
/*蓝牙模块初始化标志*/
/*只需初始化一次，留30字节，用于存MAC地址*/
#define BT_SET_ADR (WAV_OFF_ADR + 0x4)
/*首次开机标志标志*/
#define DEV_SET_ADR (BT_SET_ADR + 0x4)

#define DEV_TEST_ADR (DEV_SET_ADR + 0x10)

/*波长设置 增加波长  980nm 1表示有 0表示没有*/
#define WAVE_SETTING_980nm_ADR (DEV_TEST_ADR + 0x01)
/*波长设置 增加波长  1270nm 1表示有 0表示没有*/
#define WAVE_SETTING_1270nm_ADR (WAVE_SETTING_980nm_ADR + 0x01)
/*波长设置 增加波长  1577nm 1表示有 0表示没有*/
#define WAVE_SETTING_1577nm_ADR (WAVE_SETTING_1270nm_ADR + 0x01)
/*波长设置 增加波长  1650nm 1表示有 0表示没有*/
#define WAVE_SETTING_1650nm_ADR (WAVE_SETTING_1577nm_ADR + 0x01)

/*红光源的PWM值 用于调节红光大小 2字节*/
#define VFL_PWM_SETTING_ADR (WAVE_SETTING_1650nm_ADR + 0x01)

/*本地系数地址 本地校准 10 * 4 = 40字节*/
#define LOCAL_COEFFICIENT_SETTING_ADR (VFL_PWM_SETTING_ADR + 0x02)

static u16 record_fat[RECORD_AMOUNTS];
static u16 record_amount;
static volatile Record_Type s_record_data;

/**
 * @brief 保存记录
 * @param
 * @return
 */
u8 record_history_save(Record_Type *ptr)
{
    if (record_amount >= RECORD_AMOUNTS)
        return 1;
    for (u16 i = 0; i < RECORD_AMOUNTS; i++)
    {
        if (record_fat[i] == BUF_DEFAULT_FFFF)
        {
            record_fat[i] = record_amount + 1;
            rt_thread_mdelay(5); // 换页必要延时
            /*存储记录检索信息*/
            eeprom_write_page_bytes(i * RECORD_AMOUNTS_BYTES + RECORD_FAT_ADDR, (u8 *)&record_fat[i], RECORD_AMOUNTS_BYTES);
            /*存储记录*/
            eeprom_write_page_bytes(i * RECORD_SIZE_BYTES + RECORD_DATA_ADDR, (u8 *)ptr, RECORD_SIZE_BYTES);
            record_amount++;
            /*把记录数存储在固定位置*/
            eeprom_write_page_bytes(RECORD_AMOUNT_ADDR, (uint8_t *)&record_amount, RECORD_AMOUNTS_BYTES);
            rt_thread_mdelay(5);
            break;
        }
    }
    return 0;
}

/**
 * @brief 读取一条记录
 * @param index:从1开始
 * @return
 */
Record_Type record_history_read(u16 index)
{
    u16 i;
    Record_Type record_t;
    for (i = 0; i < RECORD_AMOUNTS; i++)
    {
        if (record_fat[i] == index)
        {
            eeprom_read_bytes(i * RECORD_SIZE_BYTES + RECORD_DATA_ADDR, (u8 *)&record_t, RECORD_SIZE_BYTES);
            break;
        }
    }
    return record_t;
}
/**
 * @brief 删除一条记录
 * @param index:从1开始
 * @return
 */
u8 record_history_del_single(u16 index)
{

    u16 i;
    if (index > record_amount)
        return 1;
    for (i = 0; i < RECORD_AMOUNTS; i++)
    {
        if (record_fat[i] == index)
            record_fat[i] = BUF_DEFAULT_FFFF;
        else if ((record_fat[i] > index) && (record_fat[i] != BUF_DEFAULT_FFFF))
            record_fat[i]--;
    }
    /*存储记录检索信息*/
    eeprom_write_page_bytes(RECORD_FAT_ADDR, (u8 *)&record_fat[0], RECORD_AMOUNTS * RECORD_AMOUNTS_BYTES);
    record_amount--;
    eeprom_write_page_bytes(RECORD_AMOUNT_ADDR, (u8 *)&record_amount, RECORD_AMOUNTS_BYTES);
    return 0;
}
/**
 * @brief 删除一组记录
 * @param
 * @return
 */
void record_history_del_group(u16 start, u16 end)
{

    u16 i, j;
    if (start > end)
        return;
    if (end > record_amount)
        end = record_amount;
    for (j = start; j <= end; j++)
    {
        for (i = 0; i < RECORD_AMOUNTS; i++)
        {
            if (record_fat[i] == j)
                record_fat[i] = BUF_DEFAULT_FFFF;
            else if ((record_fat[i] > j) && (record_fat[i] != BUF_DEFAULT_FFFF))
                record_fat[i]--;
        }
    }
    // save_record_fat();
    // record_index--;
    record_amount -= (end - start + 1);
    eeprom_write_page_bytes(RECORD_AMOUNT_ADDR, (u8 *)&record_amount, RECORD_AMOUNTS_BYTES);
}
/**
 * @brief 删除全部记录
 * @param
 * @return
 */
void record_history_del_all(void)
{
    u16 i;

    for (i = 0; i < RECORD_AMOUNTS; i++)
    {
        record_fat[i] = BUF_DEFAULT_FFFF;
    }
    /*存储记录检索信息*/
    eeprom_write_page_bytes(RECORD_FAT_ADDR, (u8 *)&record_fat[0], RECORD_AMOUNTS * RECORD_AMOUNTS_BYTES);
    record_amount = 0;
    eeprom_write_page_bytes(RECORD_AMOUNT_ADDR, (u8 *)&record_amount, RECORD_AMOUNTS_BYTES);
}

/**
 * @brief 开机初始化时调用
 *              加载 record_fat[]
 *              加载 record_amount
 * @param
 * @return
 */
void record_history_load_fat(void)
{
    eeprom_read_bytes(RECORD_FAT_ADDR, (u8 *)&record_fat[0], RECORD_AMOUNTS_BYTES * RECORD_AMOUNTS);
    eeprom_read_bytes(RECORD_AMOUNT_ADDR, (u8 *)&record_amount, RECORD_AMOUNTS_BYTES);
    /*若器件是新的，则读出的记录数应为 0xffff*/
    if (record_amount > RECORD_AMOUNTS)
    {
        record_amount = 0;
        eeprom_write_bytes(RECORD_AMOUNT_ADDR, (u8 *)&record_amount, RECORD_AMOUNTS_BYTES);
        memset(record_fat, 0xffff, RECORD_AMOUNTS_BYTES * RECORD_AMOUNTS);
        eeprom_write_page_bytes(RECORD_FAT_ADDR, (u8 *)&record_fat[0], RECORD_AMOUNTS_BYTES * RECORD_AMOUNTS);
    }
}
u16 record_history_amount(void)
{
    return record_amount;
}
/**********************************************************************************************/
/*
void set_eep_all_0xff(void)
{
    u16 i;
    u16 DA = 0xffff;
    u32 DD = 0xffffffff;

    memset(record_fat, 0xff, RECORD_VARIABLE_BYTES * RECORD_AMOUNTS);
    eeprom_write_bytes(RECORD_FAT_ADDR, (uint8_t *)&record_fat, RECORD_VARIABLE_BYTES * RECORD_AMOUNTS);

    eeprom_write_page_bytes(RECORD_AMOUNT_ADDR, (uint8_t *)&DA, RECORD_VARIABLE_BYTES);
    //清参考值存储位置
    for (i = 0; i < WAVE_AMOUNT; i++)
    {
        eeprom_write_page_bytes(REF_DATA_ADDR + i * 4, (uint8_t *)&DD, 4);
    }
    //清校准数据存储位置
    for (i = 0; i < WAVE_AMOUNT; i++)
    {
        eeprom_write_page_bytes(CAL_POWER_ADR + i * 4 * 16, (u8 *)&DD, 4);
        eeprom_write_page_bytes(CAL_VOL_ADR + i * 4 * 16, (u8 *)&DD, 4);
    }
    for (i = 0; i < WAVE_AMOUNT; i++)
    {
        eeprom_write_page_bytes(CAL_COEFF_ADR + i * 4, (u8 *)&DD, 4);
    }
}
*/
/**
 * @brief 保存校准系数,用于校准命令
 * @param num:字节数
 * @return
 */
void record_cal_coef_save(float *cal_coeff, int num)
{
    rt_delay_ms(100); // 尝试增加延时
    eeprom_write_page_bytes(CAL_COEFF_ADR, (u8 *)cal_coeff, num);
}

/**
 * @brief 保存校准数据,用于校准命令
 * @param float *mw:校准数据, num1:字节数
 * @return
 */
void record_cal_linear_save(float *mw, int num1, int *vol, int num2)
{
    rt_delay_ms(100); // 尝试增加延时
    eeprom_write_page_bytes(CAL_POWER_ADR, (u8 *)mw, num1);
    rt_delay_ms(100);
    eeprom_write_page_bytes(CAL_VOL_ADR, (u8 *)vol, num2);
    rt_delay_ms(100);
}

/**
 * @brief 读取校准数据,用于校准命令
 * @param
 * @return 0:校准数据正常，-1:没有线性数据,-2:没有校准系数，-3:线性和校准系数都没有
 */
int record_cal_read_all(float *mw, int byte_num1, int *vol, int byte_num2, float *coeff, int byte_num3)
{
    u8 i;
    u32 cal_vol_buf;
    u32 cal_coef_buf;
    int flag = 0;
    eeprom_read_bytes(CAL_POWER_ADR, (u8 *)mw, byte_num1);
    rt_delay_ms(100);
    eeprom_read_bytes(CAL_VOL_ADR, (u8 *)vol, byte_num2);
    rt_delay_ms(100);
    eeprom_read_bytes(CAL_COEFF_ADR, (u8 *)coeff, byte_num3);
    rt_delay_ms(100);
    for (i = 0; i < (byte_num2 / 4); i++)
    {
        eeprom_read_bytes(CAL_VOL_ADR + i * 4, (u8 *)&cal_vol_buf, 4);
        if (cal_vol_buf == 0xffffffff)
        {
            flag = -1;
            break;
        }
    }
    for (i = 0; i < (byte_num3 / 4); i++)
    {
        eeprom_read_bytes(CAL_COEFF_ADR + i * 4, (u8 *)&cal_coef_buf, 4);
        if (cal_coef_buf == 0xffffffff)
        {
            flag = flag == -1 ? -3 : -2;
            break;
        }
    }
    return flag;
}

/**
 * @brief 重置校准数据，把程序里默认的校准数据读入
 * @param num1:cal power 字节数, num2:cal vol 字节数, num3: cal coeff 字节数
 * @return
 */
void record_cal_clr_all(int num1, int num2, int num3)
{
    u32 ff = 0xffffffff;
    eeprom_write_page_bytes(CAL_POWER_ADR, (u8 *)&ff, num1);
    eeprom_write_page_bytes(CAL_VOL_ADR, (u8 *)&ff, num2);
    eeprom_write_page_bytes(CAL_COEFF_ADR, (u8 *)&ff, num3);
}
/**
 * @brief 获取当前的背光等级
 * @param void
 * @return 背光等级,0-100
 */

u8 record_back_light_get(void)
{
    u8 level;
    eeprom_read_bytes(BACK_LIGHT_ADR, (uint8_t *)&level, 1);
    if (level == 0xff)
    {
        level = 5;
        eeprom_write_page_bytes(BACK_LIGHT_ADR, (u8 *)&level, 1);
    }
    return level;
}

/**
 * @brief 设置当前的背光等级
 * @param 背光等级,0-100
 * @return
 */
void record_back_light_set(u8 level)
{
    if (level > 100)
        return;
    eeprom_write_page_bytes(BACK_LIGHT_ADR, (u8 *)&level, 1);
}
/**
 * @brief 设置自动关机时间
 * @param second:自动关机时间,单位秒
 *        second=0xfedc;//关闭自动关机
 * @return
 */
void record_off_time_set(u16 second)
{
    eeprom_write_page_bytes(AUTO_OFF_ADR, (u8 *)&second, 2);
}
/**
 * @brief 获取自动关机时间
 * @param
 * @return 自动关机时间,单位秒
 */
u16 record_off_time_get(void)
{
    u16 second;
    eeprom_read_bytes(AUTO_OFF_ADR, (u8 *)&second, 2);
    if (second == 0xffff)
    {
        second = 10 * 60;
        eeprom_write_bytes(AUTO_OFF_ADR, (u8 *)&second, 2);
    }
    return second;
}

/**
 * @brief 获取ref值
 * @param wave:波长序号应该从零开始
 * @return
 */
float record_ref_get(u8 offset)
{
    float temp = 0;
    u32 test = 0;
    eeprom_read_bytes(REF_DATA_ADDR + offset * 4, (u8 *)&test, 4);
    if (test == 0xffffffff)
    {
        eeprom_write_page_bytes(REF_DATA_ADDR + offset * 4, (u8 *)&temp, 4);
        return temp;
    }
    else
    {
        eeprom_read_bytes(REF_DATA_ADDR + offset * 4, (u8 *)&temp, 4);
        return temp;
    }
}
bool _wave_clear_ref = RT_FALSE;
u8 _wave_clear_val = 0;
void record_ref_set(float reff, u8 offset)
{
    float defult_data = 0.00;
    eeprom_write_page_bytes(REF_DATA_ADDR + offset * 4, (u8 *)&reff, 4);
    if (_wave_clear_ref)
    {
        _wave_clear_ref = RT_FALSE;
        eeprom_write_bytes((REF_DATA_ADDR + _wave_clear_val * 4), (u8 *)&defult_data, 4);
    }
}
/**
 * @function: record_ref_del_all
 * @description: 删除所有参考值
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void record_ref_del_all(void)
{
    float defult_data = 0.00;
    float _out_value = 0;
    rt_enter_critical();
    for (char i = 0; i < _wave_max; i++)
    {
        record_ref_set(defult_data, i);
        rt_delay_ms(10);
        eeprom_read_bytes(REF_DATA_ADDR + i * 4, (u8 *)&_out_value, 4);
        rt_delay_ms(10);
        // 第一次判断
        if (_out_value != defult_data)
        {
            record_ref_set(defult_data, i);
            rt_delay_ms(10);
            eeprom_read_bytes(REF_DATA_ADDR + i * 4, (u8 *)&_out_value, 4);
            rt_delay_ms(10);
            // 第二次判断
            if (_out_value != defult_data)
            {
                // 记录没有删除的参考值
                _wave_clear_val = i;
                _wave_clear_ref = true;
            }
        }
    }
    rt_exit_critical();
}

/**
 * @function: record_ref_del_current
 * @description: 删除当前参考值
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void record_ref_del_current(u8 offect)
{
    float defult_data = 0.00;
    float _out_value = 0;
    rt_enter_critical();
    eeprom_read_bytes(REF_DATA_ADDR + offect * 4, (u8 *)&_out_value, 4);
    rt_delay_ms(10);
    // 开始判断
    while (_out_value != defult_data)
    {
        record_ref_set(defult_data, offect);
        rt_delay_ms(10);
        eeprom_read_bytes(REF_DATA_ADDR + offect * 4, (u8 *)&_out_value, 4);
        rt_delay_ms(10);
        // 第一次判断
        if (_out_value != defult_data)
        {
            record_ref_set(defult_data, offect);
            rt_delay_ms(10);
            eeprom_read_bytes(REF_DATA_ADDR + offect * 4, (u8 *)&_out_value, 4);
            rt_delay_ms(10);
            // 第二次判断
            if (_out_value != defult_data)
            {
                // 记录没有删除的参考值
                _wave_clear_val = offect;
                _wave_clear_ref = true;
                record_ref_set(defult_data, offect);
            }
        }
        rt_exit_critical();
    }
}
///////////////////////////////////////////////////////////////////////

u32 record_PowerON_Wave_get(void)
{
    u32 temp = 0;
    eeprom_read_bytes(WAV_OFF_ADR, (u8 *)&temp, 1);
    // 都不是对应波长 则没有进行过写入或者错误数据
    if (temp != _wave_850 && temp != _wave_1300 &&
        temp != _wave_1490 && temp != _wave_1550 &&
        temp != _wave_1625 && temp != _wave_980 &&
        temp != _wave_1270 && temp != _wave_1577 && temp != _wave_1650)
    {
        // 给默认值
        temp = _wave_1550;
        eeprom_write_bytes(WAV_OFF_ADR, (u8 *)&temp, 1);
        return temp;
    }
    return temp;
}

void record_PowerON_Wave_set(u32 Wave)
{
    u32 temp = 0;
    eeprom_write_bytes(WAV_OFF_ADR, (u8 *)&Wave, 1);
    eeprom_read_bytes(WAV_OFF_ADR, (u8 *)&temp, 1);
    while (Wave != temp)
    {
        eeprom_write_bytes(WAV_OFF_ADR, (u8 *)&Wave, 1);
    }
}

char record_Seting_Wave_get(void)
{
    char temp = 0;
    char result = 0;
    eeprom_read_bytes(WAVE_SETTING_980nm_ADR, (u8 *)&temp, 1);
    // 都不是对应波长 则没有进行过写入或者错误数据
    if (temp != 0 && temp != 1)
    {
        // 给默认值
        temp = 0;
        eeprom_write_bytes(WAVE_SETTING_980nm_ADR, (u8 *)&temp, 1);
    }
    result |= temp << 3;

    eeprom_read_bytes(WAVE_SETTING_1270nm_ADR, (u8 *)&temp, 1);
    // 都不是对应波长 则没有进行过写入或者错误数据
    if (temp != 0 && temp != 1)
    {
        // 给默认值
        temp = 0;
        eeprom_write_bytes(WAVE_SETTING_1270nm_ADR, (u8 *)&temp, 1);
    }
    result |= temp << 2;

    eeprom_read_bytes(WAVE_SETTING_1577nm_ADR, (u8 *)&temp, 1);
    // 都不是对应波长 则没有进行过写入或者错误数据
    if (temp != 0 && temp != 1)
    {
        // 给默认值
        temp = 0;
        eeprom_write_bytes(WAVE_SETTING_1577nm_ADR, (u8 *)&temp, 1);
    }
    result |= temp << 1;

    eeprom_read_bytes(WAVE_SETTING_1650nm_ADR, (u8 *)&temp, 1);
    // 都不是对应波长 则没有进行过写入或者错误数据
    if (temp != 0 && temp != 1)
    {
        // 给默认值
        temp = 0;
        eeprom_write_bytes(WAVE_SETTING_1650nm_ADR, (u8 *)&temp, 1);
    }
    result |= temp << 0;
    return result;
}



void record_Seting_Wave_set_980nm(char wave_980_status)
{
    eeprom_write_bytes(WAVE_SETTING_980nm_ADR, (u8 *)&wave_980_status, 1);
}

void record_Seting_Wave_set_1270nm(char wave_1270_status)
{
    eeprom_write_bytes(WAVE_SETTING_1270nm_ADR, (u8 *)&wave_1270_status, 1);
}

void record_Seting_Wave_set_1577nm(char wave_1577_status)
{
    eeprom_write_bytes(WAVE_SETTING_1577nm_ADR, (u8 *)&wave_1577_status, 1);
}

void record_Seting_Wave_set_1650nm(char wave_1650_status)
{
    eeprom_write_bytes(WAVE_SETTING_1650nm_ADR, (u8 *)&wave_1650_status, 1);
}

void record_Seting_Wave_set(Wave_Enum wave, char wave_status)
{
    if (wave == _wave_980)
    {
        record_Seting_Wave_set_980nm(wave_status);
    }
    if (wave == _wave_1270)
    {
        record_Seting_Wave_set_1270nm(wave_status);
    }
    if (wave == _wave_1577)
    {
        record_Seting_Wave_set_1577nm(wave_status);
    }
    if (wave == _wave_1650)
    {
        record_Seting_Wave_set_1650nm(wave_status);
    }
}

bool record_bt_get()
{
    u32 temp = 0;
    eeprom_read_bytes(BT_SET_ADR, (u8 *)&temp, 4);
    if (temp != 0x1234UL)
    {
        temp = BUF_DEFAULT_8F;
        eeprom_write_bytes(BT_SET_ADR, (u8 *)&temp, 4);
        return false;
    }
    return true;
}

void record_bt_set(bool flag)
{
    u32 temp = 0x1234UL;
    if (flag == true)
    {
        eeprom_write_bytes(BT_SET_ADR, (u8 *)&temp, 4);
    }
    else
    {
        temp = BUF_DEFAULT_8F;
        eeprom_write_bytes(BT_SET_ADR, (u8 *)&temp, 4);
    }
}
/**
 * @function:record_baud_set
 * @brief:波特率的设置
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void record_baud_set(bool flag)
{
    u32 temp = 0x1234UL;
    if (flag == true)
    {
        eeprom_write_bytes(BT_SET_ADR + 34, (u8 *)&temp, 4);
    }
    else
    {
        temp = BUF_DEFAULT_8F;
        eeprom_write_bytes(BT_SET_ADR + 34, (u8 *)&temp, 4);
    }
}
/**
 * @function: record_baud_get
 * @brief:true 表示9600
 * @param {*}
 * @return {*}
 * @author: lzc
 */
bool record_baud_get()
{
    u32 temp = 0;
    eeprom_read_bytes(BT_SET_ADR + 34, (u8 *)&temp, 4);
    if (temp != 0x1234UL)
    {
        temp = BUF_DEFAULT_8F;
        eeprom_write_bytes(BT_SET_ADR + 34, (u8 *)&temp, 4);
        return false;
    }
    return true;
}

void record_bt_name_set(char *name)
{
}

void record_bt_name_get(char *name)
{
}

bool record_bt_get_mac(u8 *buf)
{
    u32 temp = 0;
    u32 i = 0;
    eeprom_read_bytes(BT_SET_ADR, (u8 *)&temp, 4);
    if (temp == 0x1234UL)
    {
        for (i = 0; i < 30; i++)
        {
            eeprom_read_bytes(BT_SET_ADR + 4 + i, (u8 *)&buf[i], 1);
            if (buf[i] == '\0')
                break;
        }
        if (i >= 30)
            return false;
        else
            return true;
    }
    else
        return false;
}

bool record_bt_set_mac(const u8 *buf)
{
    u32 i = 0;
    u8 temp = 'X';
    u8 temp1 = '\0';
    for (i = 0; i < 20; i++)
    {
        eeprom_write_bytes(BT_SET_ADR + 4 + i, (u8 *)&buf[i], 1);
        if (buf[i] == '\0')
            break;
    }
    if (i >= 20)
    {
        for (i = 0; i < 10; i++)
        {
            eeprom_write_bytes(BT_SET_ADR + 4 + i, &temp, 1);
        }
        eeprom_write_bytes(BT_SET_ADR + 4 + i, &temp1, 1);
        return false;
    }
    else
        return true;
}

/**
 * @brief 初始化相关数据，开机时调用
 * @param
 * @return
 */
void record_init(void)
{
    /*检测eeprom是否正常*/
    u8 data = 0x13;
    u8 temp = 0;
    eeprom_write_page_bytes(1, &data, 1);
    eeprom_read_bytes(1, &temp, 1);
    if (temp != 0x13)
        while (1)
            ;
    record_history_load_fat(); //刷新记录序号
}

/**
 * @function:_return_liner_power_eeprom_addr
 * @description:返回线性的功率值存贮地址
 * @param {*}
 * @return {u16}
 * @author: lzc
 */
u16 _return_liner_power_eeprom_addr(void)
{
    u16 temp = 0;
    temp = CAL_POWER_ADR;
    return temp;
}

/**
 * @function:_return_liner_vol_eeprom_addr
 * @description:返回线性的电压值存贮地址
 * @param {*}
 * @return {u16}
 * @author: lzc
 */
u16 _return_liner_vol_eeprom_addr(void)
{
    u16 temp = 0;
    temp = CAL_VOL_ADR;
    return temp;
}

/**
 * @function:_return_liner_coeff_eeprom_addr
 * @description:返回线性的波长校准存贮地址
 * @param {*}
 * @return {u16}
 * @author: lzc
 */
u16 _return_liner_coeff_eeprom_addr(void)
{
    u16 temp = 0;
    temp = CAL_COEFF_ADR;
    return temp;
}
