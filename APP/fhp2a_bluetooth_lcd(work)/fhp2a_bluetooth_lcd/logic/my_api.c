/*
 * @Description:
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-17 09:54:11
 * @LastEditors: lzc
 * @LastEditTime: 2020-12-21 11:05:04
 */
/**
* \file my_api.c
* \brief 一些库函数重写，或其他通用接口
* \author  mirlee
* \version 0.2
* \date   2020-05-10
* \note
*/

#include "my_api.h"

typedef void (*pFunction)(void); // 定义一个函数指针类型pFunction
pFunction Jump_To_BL_lication; // 定义一个函数指针变量
/**
 * @brief 浮点数转化字符串,只保留两位小数，其他舍去
 *         注意防止buf溢出！
 * @param f：浮点数
 * @return
 */
void my_ftoa(float f, char *buf)
{
    int f_i = f * (float)100.0;
    int div = 10000000;
    int state = 0;
    int j = 0;
    if (f_i > div)
        while (1);
    if (f < 0)
    {
        buf[j++] = '-';
        f_i = 0 - f_i;
    }
    for (; div >= 10; div /= 10)
    {
        buf[j++] = f_i / div + 48;
        f_i %= div;
        if ((buf[j - 1] == '0') && (state == 0))
            j = j - 1;
        else
            state = 1;
        if (div == 100)
        {
            if (j == 0 || (j == 1 && buf[0] == '-'))
            {
                buf[j++] = '0';
                state = 1;
            }
            buf[j++] = '.';
        }
        if (div == 10)
        {
            buf[j] = f_i + 48;
            buf[j + 1] = '\0';
        }
    }
}
/**
 * @brief 整数转为字符串
 *         注意防止buf溢出！
 * @param
 * @return
 */
void my_itoa(int n, char buf[])
{
    int f_i = n;
    int div = 10000000;
    int state = 0;
    int j = 0;
    if (f_i > div)
        while (1);
    if (f_i < 0)
    {
        buf[j++] = '-';
        f_i = 0 - f_i;
    }
    for (; div >= 10; div /= 10)
    {
        buf[j++] = f_i / div + 48;
        f_i %= div;
        if ((buf[j - 1] == '0') && (state == 0))
            j = j - 1;
        else
            state = 1;
        if (div == 10)
        {
            buf[j] = f_i + 48;
            buf[j + 1] = '\0';
        }
    }
}


/*
        +--------------------------------------------+
        |                                            |
        |      NONE_OS                               |
        |                   ISR                      |
        |                +---+---+                   |
        |                |   |   |                   |
        |                |   |   |                   |
        |     +---+------+   |   +----+----+         |
        |         |          |        |              |
        |         v          v        v              |
        |        MSP        MSP      MSP             |
        |                                            |
        |                                            |
        |      RTOS                                  |
        |                                            |
        |                    ISR                     |
        |                 +---+---+                  |
        |                 |   |   |                  |
        |                 |   |   |                  |
        |      +---+------+   |   +----+----+        |
        |          |          |        |             |
        |          v          v        v             |
        |         PSP        MSP      PSP            |
        |                                            |
        +--------------------------------------------+
*/
/**
 * @function:JumpToBL
 * @brief:从APP跳转到BL
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void JumpToBL(uint32_t target_address)
{

    // Deinit 所有可能导致问题的外设
    LL_USART_DeInit(USART1);// 用于清除之前的初始化状态

    LL_USART_DeInit(USART2);// 用于清除之前的初始化状态

    LL_ADC_DeInit(ADC1);

    LL_TIM_DeInit(TIM3);

    LL_TIM_DeInit(TIM6);

    LL_RCC_DeInit();

    LL_DMA_DeInit(DMA1, LL_DMA_CHANNEL_1);

    __disable_irq(); // 关闭总中断

    // 代码的flash起始地址加4，就是Reset Handler的函数地址
    uint32_t JumpAddress = *(__IO uint32_t *)(target_address + 4);

    Jump_To_BL_lication = (pFunction)JumpAddress; // 把这个地址转为函数指针

    // 带操作系统，需要使用PSP入栈，最后在使用MSP出栈
    __set_PSP(*(__IO uint32_t *)target_address); // 设置栈顶地址

    __set_CONTROL(0);

    __set_MSP(*(__IO uint32_t *)target_address); // 设置栈顶地址

    Jump_To_BL_lication(); // 开始运行APP的Reset Handler
}



