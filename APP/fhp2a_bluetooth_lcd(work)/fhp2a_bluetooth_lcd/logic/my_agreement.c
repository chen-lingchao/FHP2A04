/*
 * @Description: my_agreement,协议处理C文件
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-24 08:51:47
 * @LastEditors: lzc
 * @LastEditTime: 2020-12-16 17:29:15
 */
#include "my_agreement.h"
#include <math.h>

static Agreement_Type s_agreement;
static char Uart_hex_agr_TX_buf[13];
/*
 * 静态函数
*/
static double exp10(double x)
{
    double y;
    y = exp(x * log(10)); // a^x = b^(x * log_b(a))
    return (y);
}

static __attribute__ ((unused)) float dbW_2_W(double dbW) 
{
    double W = 0;
    W = exp10(dbW / 10);
    return W;
}
/**
 * @function: agreement_parse_check
 * @description: 协议处理 校验
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static char agreement_parse_check(void)
{
    s_agreement.data = g_rx_buf;
    s_agreement.status = _agr_ready_status;
    // 判断协议头
    if (*(s_agreement.data) == 0xaa)
    {
        return  AGR_OK;
    }
    return AGR_ERROR;
}

/**
 * @function: agreement_parse_process
 * @description: 协议解析的处理程序
 * @param {*}
 * @return {*}
 * @author: lzc
 */
volatile u32 temp = 0;
uint8_t *ptr ;
float tmp;
static void agreement_parse_process(void)
{
    // 使用联合体进行进制转换
    union _fun_float2hex float2hex;
    volatile float power = 0;
    volatile u32 hex_power_val = 0;
    // 校准相关
    volatile uint8_t *ptr = 0;
    volatile float _cal_pwoer = 0;
    // 存储的记录相关
    volatile u8  _result_record_H = 0;
    volatile u8  _result_record_L = 0;
    volatile u16 _result_record = 0;
    // EEPROM 存储以及读取相关
    volatile u16 _eep_addr = 0;
    volatile u8  _eep_byte_num = 0;
    volatile u8  _eep_temp_buf[8] = { 0 };
    rt_enter_critical();
    memset(Uart_hex_agr_TX_buf, 0X00, sizeof(Uart_hex_agr_TX_buf));
    switch (s_agreement.data[_ONE_BIT])
    {
    case 0x01:
        // 返回光功率的值（ADC）
        if (s_agreement.data[_TWO_BIT] == 0x00)
        {
            // 返回光功率的值（ADC）
            power = data_optical_get(_unit_mw);
            ptr = (rt_uint8_t *)&power;
            float2hex.f = power;
            hex_power_val = float2hex.m;
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x01;
            // 功率值
            Uart_hex_agr_TX_buf[_FIVE_BIT]  = *(ptr);
            Uart_hex_agr_TX_buf[_SIX_BIT]   = *(ptr + 1);
            Uart_hex_agr_TX_buf[_SEVEN_BIT] = *(ptr + 2);
            Uart_hex_agr_TX_buf[_EIGHT_BIT] = *(ptr + 3);

            // ADC_VAL
            temp = get_vol_data();
            Uart_hex_agr_TX_buf[_NINE_BIT] = (temp >> 8) & 0xff;
            Uart_hex_agr_TX_buf[_TEN_BIT]  = (temp >> 0) & 0xff;
            // 光信号频率
            // TODO : 还没有进行，不知道应该填什么
            Uart_hex_agr_TX_buf[_ELEVEN_BIT] = 0x01;
            // 功率值调整档位
            Uart_hex_agr_TX_buf[_TWELVE_BIT] = data_ch_get();
        }
        // 返回光功率的值（波长电量）
        else if (s_agreement.data[_TWO_BIT] == 0x80)
        {
            // 返回光功率的值（波长电量）
            power = data_optical_get(_unit_mw);
            ptr = (rt_uint8_t *)&power;
            float2hex.f = power;
            hex_power_val = float2hex.m;
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x01;
            Uart_hex_agr_TX_buf[_THREE_BIT] = 0x80;
            // 功率值
            Uart_hex_agr_TX_buf[_FIVE_BIT]  = *(ptr);
            Uart_hex_agr_TX_buf[_SIX_BIT]   = *(ptr + 1);
            Uart_hex_agr_TX_buf[_SEVEN_BIT] = *(ptr + 2);
            Uart_hex_agr_TX_buf[_EIGHT_BIT] = *(ptr + 3);
            // 模式
            temp = g_sys_flag.data.unit ;
            if (temp == _unit_mw)
            {
                // mw,uw,nw
                Uart_hex_agr_TX_buf[_NINE_BIT] = 0x00;
            }
            else if (temp == _unit_dbm)
            {
                // dbm
                Uart_hex_agr_TX_buf[_NINE_BIT] = 0x01;
            }
            else if (temp == _unit_db)
            {
                // db
                Uart_hex_agr_TX_buf[_NINE_BIT] = 0x02;
            }
            else if (g_sys_flag.work.mode == _mode_brief)
            {
                // REF
                Uart_hex_agr_TX_buf[_NINE_BIT] = 0x03;
            }
            // 波长
            //  _wave_850,  0
            //  _wave_1300, 1
            //  _wave_1310, 2
            //  _wave_1490, 3
            //  _wave_1550, 4
            //  _wave_1625, 5
            Uart_hex_agr_TX_buf[_TEN_BIT]  = g_sys_flag.data.wave;
            // 电池电量
            // _bat_empty,  0
            // _bat_one,    1
            // _bat_two,    2
            // _bat_three,  3
            Uart_hex_agr_TX_buf[_ELEVEN_BIT] = g_sys_flag.data.bat;
        }
        // 返回当前光功率的测量值以及参考功率
        else
        {
            // 返回当前光功率的测量值以及参考功率
            power = data_optical_get(_unit_mw);
            ptr = (rt_uint8_t *)&power;
            float2hex.f = power;
            hex_power_val = float2hex.m;
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x01;
            Uart_hex_agr_TX_buf[_THREE_BIT] = 0x01;

            // 功率值
            Uart_hex_agr_TX_buf[_FIVE_BIT]  = *(ptr);
            Uart_hex_agr_TX_buf[_SIX_BIT]   = *(ptr + 1);
            Uart_hex_agr_TX_buf[_SEVEN_BIT] = *(ptr + 2);
            Uart_hex_agr_TX_buf[_EIGHT_BIT] = *(ptr + 3);


            // 参考功率
            float _temp_ref_power = 0;
            _temp_ref_power = record_ref_get(g_sys_flag.data.wave);
            ptr = (rt_uint8_t *)&_temp_ref_power;
            float2hex.f = _temp_ref_power;
            hex_power_val = float2hex.m;

            Uart_hex_agr_TX_buf[_NINE_BIT]   = *(ptr);
            Uart_hex_agr_TX_buf[_TEN_BIT]    = *(ptr + 1);
            Uart_hex_agr_TX_buf[_ELEVEN_BIT] = *(ptr + 2);
            Uart_hex_agr_TX_buf[_TWELVE_BIT] = *(ptr + 3);

        }
        break;
    case 0x02:
        // 进入波长档位判断
        if (s_agreement.data[_TWO_BIT] == 0x01)
        {
            // 切换波长档位，从ROM读取参考功率
            if (s_agreement.data[_THREE_BIT] == 0x00)
            {
                // 协议头
                Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x02;

                g_sys_flag.data.wave = (Wave_Enum)(g_sys_flag.data.wave >= (_wave_max - 1) ? _wave_850 : ++g_sys_flag.data.wave);
            }
            // 切换波长档位，直接切换到_FOUR_BIT,从ROM读取参考功率
            else if (s_agreement.data[_THREE_BIT] == 0x01)
            {
                // 协议头
                Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x02;

                g_sys_flag.data.wave = (Wave_Enum)(g_sys_flag.data.wave > (_wave_max - 1) ? _wave_850 : g_rx_buf[_FOUR_BIT]);
            }
        }
        // 进入光功率单位判断
        else if (s_agreement.data[_TWO_BIT] == 0x02)
        {
            // 切换光功率单位
            if (s_agreement.data[_THREE_BIT] == 0x00)
            {
                // 协议头
                Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x02;

                g_sys_flag.data.unit = (Unit_Enum)( g_sys_flag.data.unit >= (_unit_max - 1) ? _unit_mw : ++g_sys_flag.data.unit);
            }
            // 切换光功率单位 ，直接切换到_FOUR_BIT
            else if (s_agreement.data[_THREE_BIT] == 0x01)
            {
                // 协议头
                Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x02;

                g_sys_flag.data.unit = (Unit_Enum)( g_sys_flag.data.unit > (_unit_max - 1) ? _unit_mw : g_rx_buf[_FOUR_BIT]);
            }
        }
        // 切换到查看参考值模式
        else if (s_agreement.data[_TWO_BIT] == 0x03)
        {
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x02;
            // 切换到查看参考值模式
            g_sys_flag.work.brief = _dis_ref_get;
            g_sys_flag.work.mode_re = g_sys_flag.work.mode;
            g_sys_flag.work.mode = _mode_brief;
        }
        // 设置当前值为参考值
        else if (s_agreement.data[_TWO_BIT] == 0x13)
        {
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x02;
            // 设置当前值为参考值
            g_sys_flag.work.brief = _dis_ref_set;
            g_sys_flag.work.mode_re = g_sys_flag.work.mode;
            g_sys_flag.work.mode = _mode_brief;
        }
        // 进入背光判断
        else if (s_agreement.data[_TWO_BIT] == 0x04)
        {
            // 背光开
            if (s_agreement.data[_FOUR_BIT])
            {
                Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT] = 0x02;
                lcd_back_light(1);
            }
            // 背光关
            else
            {
                Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT] = 0x02;
                lcd_back_light(0);
            }
        }
        break;
    case 0x03:
        // 进入校准模式
        if (s_agreement.data[_TWO_BIT] == 0x01)
        {
            g_sys_flag.work.mode = _mode_cal;
            g_sys_flag.work.cal.start = 0;
            // 判断是否符合规则
            if (s_agreement.data[_FOUR_BIT] < 8)
            {
                // 切换到对应收到的档位值，同时在0档时加偏压
                data_ch_set(s_agreement.data[_FOUR_BIT]);
            }
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x03;
            Uart_hex_agr_TX_buf[_TWO_BIT]   = 0x01;
            Uart_hex_agr_TX_buf[_THREE_BIT] = 0x01;
        }
        // 取相应档位进行校准
        else if (s_agreement.data[_TWO_BIT] == 0x02)
        {
            // 取相应档位的第一个点进行校准
            if (s_agreement.data[_THREE_BIT] == 0x00)
            {
                // 校准功率的计算
                ptr = (uint8_t *)&_cal_pwoer;

                *(ptr)   = s_agreement.data[_FIVE_BIT]  ;
                *(ptr + 1) = s_agreement.data[_SIX_BIT]   ;
                *(ptr + 2) = s_agreement.data[_SEVEN_BIT] ;
                *(ptr + 3) = s_agreement.data[_EIGHT_BIT] ;

                if ((g_sys_flag.work.cal.start == 1) && (g_sys_flag.work.mode == _mode_cal))
                {
                    hex_data_cal_linear_set(_ONE_BIT, _cal_pwoer);
                }
                Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT] =  0x03;
            }
            // 取相应档位的第二个点进行校准
            else if (s_agreement.data[_THREE_BIT] == 0x80)
            {
                // 校准功率的计算
                ptr = (uint8_t *)&_cal_pwoer;

                *(ptr)   = s_agreement.data[_FIVE_BIT]  ;
                *(ptr + 1) = s_agreement.data[_SIX_BIT]   ;
                *(ptr + 2) = s_agreement.data[_SEVEN_BIT] ;
                *(ptr + 3) = s_agreement.data[_EIGHT_BIT] ;

                if ((g_sys_flag.work.cal.start == 1) && (g_sys_flag.work.mode == _mode_cal))
                {
                    hex_data_cal_linear_set(_TWO_BIT, _cal_pwoer);
                }
                Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x03;
                Uart_hex_agr_TX_buf[_THREE_BIT] = 0x80;
            }
            //////////////////////////////////////////////
            // 取第七个档位的第一个点进行校准
            else if (s_agreement.data[_THREE_BIT] == 0xc0)
            {
                // 校准功率的计算
                ptr = (uint8_t *)&_cal_pwoer;

                *(ptr)   = s_agreement.data[_FIVE_BIT]  ;
                *(ptr + 1) = s_agreement.data[_SIX_BIT]   ;
                *(ptr + 2) = s_agreement.data[_SEVEN_BIT] ;
                *(ptr + 3) = s_agreement.data[_EIGHT_BIT] ;

                if ((g_sys_flag.work.cal.start == 1) && (g_sys_flag.work.mode == _mode_cal))
                {
                    hex_data_cal_linear_set(_ONE_BIT, _cal_pwoer);
                }
                Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x03;
                Uart_hex_agr_TX_buf[_THREE_BIT] = 0xc0;
            }
            // 取第七个档位的第二个点进行校准
            else if (s_agreement.data[_THREE_BIT] == 0x01)
            {
                // 校准功率的计算
                ptr = (uint8_t *)&_cal_pwoer;

                *(ptr)   = s_agreement.data[_FIVE_BIT]  ;
                *(ptr + 1) = s_agreement.data[_SIX_BIT]   ;
                *(ptr + 2) = s_agreement.data[_SEVEN_BIT] ;
                *(ptr + 3) = s_agreement.data[_EIGHT_BIT] ;

                if ((g_sys_flag.work.cal.start == 1) && (g_sys_flag.work.mode == _mode_cal))
                {
                    hex_data_cal_linear_set(_TWO_BIT, _cal_pwoer);
                }
                Uart_hex_agr_TX_buf[_ZERO_BIT]  = 0xaa;
                Uart_hex_agr_TX_buf[_ONE_BIT]   = 0x03;
                Uart_hex_agr_TX_buf[_THREE_BIT] = 0x01;
            }
        }
        // 输入波长校准系数
        else if (s_agreement.data[_TWO_BIT] == 0x03)
        {
            // 校准波长功率的计算
            ptr = (uint8_t *)&_cal_pwoer;

            *(ptr)   = s_agreement.data[_FIVE_BIT]  ;
            *(ptr + 1) = s_agreement.data[_SIX_BIT]   ;
            *(ptr + 2) = s_agreement.data[_SEVEN_BIT] ;
            *(ptr + 3) = s_agreement.data[_EIGHT_BIT] ;
            // 转换
            if (g_sys_flag.work.mode == _mode_single)
            {
                hex_data_cal_coeff_set(_cal_pwoer);
            }
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x03;
            Uart_hex_agr_TX_buf[_TWO_BIT]  = 0x03;
        }
        // 保存校准到EEPROM
        else if (s_agreement.data[_TWO_BIT] == 0x04)
        {
            // 保存数据到 eeprom
            data_cal_linear_save();
            // 退出校准模式
            g_sys_flag.work.mode = _mode_single;
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x03;
            Uart_hex_agr_TX_buf[_TWO_BIT]  = 0x04;

        }
        // 自动关机
        else if (s_agreement.data[_TWO_BIT] == 0x07)
        {
            // 自动关机 关闭
            if (s_agreement.data[_THREE_BIT] == 0x00)
            {
                record_off_time_set(AUTO_OFF_FLAG);
            }
            // 自动关机 开启
            else if (s_agreement.data[_THREE_BIT] == 0x01)
            {
                // 如果是发送的有包含时间数据
                if (s_agreement.data[_FOUR_BIT] != 0x00)
                {
                    u16 sys_power_down_time = 0;
                    // 数据合成
                    sys_power_down_time = (s_agreement.data[_FOUR_BIT] << 8) + s_agreement.data[_FIVE_BIT];
                    // 自动关闭的时间设置
                    record_off_time_set(sys_power_down_time);
                }
                else
                {
                    record_off_time_set(AUTO_OFF_TIME);
                }
            }
            // 协议头
            Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
            Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x03;
        }
        break;
    case 0x05:
        // 复位光功率参考值
        record_ref_del_all(); // 删除全部

        // record_ref_del_current( g_sys_flag.data.wave); // 删除当前
        // 协议头
        Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
        Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x05;

        break;
    case 0x10:
        // 删除 EEPROM 所有内容
        record_history_del_all();
        // 协议头
        Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
        Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x10;
        break;

    case 0x19:
        // 清空存储的功率记录
        record_history_del_all();
        // 协议头
        Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
        Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x19;
        break;

    case 0x20:
        // 读取EEPROM对应地址的值
        _eep_addr = s_agreement.data[_TWO_BIT] + (s_agreement.data[_THREE_BIT] << 8);
        _eep_byte_num = s_agreement.data[_FOUR_BIT];

        // 读取eeprom
        eeprom_read_bytes(_eep_addr, (u8 *)_eep_temp_buf, _eep_byte_num);
        // putstr((const char *)_eep_temp_buf);
        for (char i = 0; i < 8; i++)
        {
            Uart_hex_agr_TX_buf[_FIVE_BIT + i] = _eep_temp_buf[i];
        }

        // 协议头
        Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
        Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x20;
        Uart_hex_agr_TX_buf[_TWO_BIT]  = 0x08;
        break;

    case 0x21:
        // 写入EEPROM对应地址的值
        _eep_addr = s_agreement.data[_TWO_BIT] + (s_agreement.data[_THREE_BIT] << 8);
        _eep_byte_num = s_agreement.data[_FOUR_BIT];

        for (char i = 0; i < 8; i++)
        {
            _eep_temp_buf[i] = s_agreement.data[_FIVE_BIT + i] ;
        }
        // 写eeprom
        eeprom_write_page_bytes(_eep_addr, (u8 *)_eep_temp_buf, _eep_byte_num);
        rt_delay_ms(10);
        // 协议头
        Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
        Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x21;
        Uart_hex_agr_TX_buf[_TWO_BIT]  = 0x08;
        break;

    case 0x22:
        // 返回存储的功率记录数量
        _result_record = record_history_amount();
        _result_record_H = _result_record / 255;
        _result_record_L = _result_record % 255;
        // 协议头
        Uart_hex_agr_TX_buf[_ZERO_BIT] = 0xaa;
        Uart_hex_agr_TX_buf[_ONE_BIT]  = 0x22;
        // 记录数
        Uart_hex_agr_TX_buf[_FIVE_BIT] = _result_record_L;
        Uart_hex_agr_TX_buf[_SIX_BIT]  = _result_record_H;
        break;


    default:
        break;
    }
    rt_exit_critical();
    s_agreement.status = _agr_end_status;
}

/**
 * @function: agreement_parse_loading
 * @description: 加载数据
 * @param {*}
 * @return {*}
 * @author: lzc
 */
static void agreement_parse_loading(void)
{
    // 解析成功
    if (agreement_parse_check() == AGR_OK)
    {
        s_agreement.status = _agr_process_status;
        agreement_parse_process();
        if (s_agreement.status == _agr_end_status)
        {
            // 进行发送对应的协议应答(HEX协议)
            for (char i = 0; i < 13; i++)
            {
                putchar_usart2(Uart_hex_agr_TX_buf[i]);
            }
            // 清除rx_buf  清除数据
            memset((void *)g_rx_buf, 0, sizeof((const char *)g_rx_buf));
        }
        else
        {
            // 不是完成状态--处理状态
            if (s_agreement.status == _agr_process_status)
            {
                // 清除rx_buf  清除数据
                memset((void *)g_rx_buf, 0, sizeof((const char *)g_rx_buf));
            }
        }
    }
    else
    {
        // 解析失败 清除数据
        memset((void *)g_rx_buf, 0, sizeof((const char *)g_rx_buf));
    }
}

/*
 * 外部函数
*/

/**
 * @function: agreement_parse_output
 * @description: 外部调用
 * @param {*}
 * @return {*}
 * @author: lzc
 */
void agreement_parse_output(void)
{
    // hex协议
    if (s_agreement.type == _agr_hex_type)
    {
        agreement_parse_loading();
    }
    // 字符串协议
    else
    {
        msg_parse((const char *)g_rx_buf);
        memset((void *)g_rx_buf, 0, strlen((const char *)g_rx_buf));
    }
}

/**
 * @function:agreement_parse_return_type
 * @description:返回协议的类型
 * @param {*}
 * @return {u8 s_agreement.type}
 * @author: lzc
 */
u8 agreement_parse_return_type(void)
{
    return s_agreement.type;
}

/**
 * @function:agreement_parse_change_type
 * @description:改变协议的类型
 * @param {u8 agreement_type}
 * @return {*}
 * @author: lzc
 */
void agreement_parse_change_type(u8 agreement_type)
{
    s_agreement.type = agreement_type;
}
