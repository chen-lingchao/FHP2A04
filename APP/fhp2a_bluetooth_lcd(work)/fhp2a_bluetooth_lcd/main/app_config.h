/** 
* Copyright (c) 2020, 浙江光维通信技术有限公司
* \file app_config.h
* \brief 包含头文件,预定义一些内容
* \author  mirlee
* \version 0.1
* \date    
*/
#ifndef _APP_CONFIG_H
#define _APP_CONFIG_H
#include "rthw.h"
#include "stm32f0xx.h"
#include "string.h"
#include "math.h"
#include "stdlib.h"
#include "stdbool.h"
/* Includes ---------------------------*/
#include "stm32f0xx_ll_crs.h"
#include "stm32f0xx_ll_rcc.h"
#include "stm32f0xx_ll_bus.h"
#include "stm32f0xx_ll_system.h"
#include "stm32f0xx_ll_exti.h"
#include "stm32f0xx_ll_cortex.h"
#include "stm32f0xx_ll_utils.h"
#include "stm32f0xx_ll_pwr.h"
#include "stm32f0xx_ll_dma.h"
#include "stm32f0xx_ll_gpio.h"
#include "stm32f0xx_ll_adc.h"
#include "stm32f0xx_ll_tim.h"
#include "stm32f0xx_ll_usart.h"
#ifdef __cplusplus
 extern "C" {
#endif

/**/

#define MY_DEVICE_A

//peripheral
#define BT_USE   1
#define VFL_USE  1

#define MY_DEBUG 0

//自动关机标志,0xfedc代表关闭自动关机
#define AUTO_OFF_FLAG ((u16)(0xfedc))
#define AUTO_OFF_TIME ((u16)(60*10))//默认关机时间,单位秒

#define MBL_APP_EXEC_ADR      (0x08001a00UL)
#define MBL_UP_FLAG_ADR       (0x08001600UL)
#define MBL_UP_FLAG_VAL       (0x1234UL)



void rt_delay_ms(int ms);

#define assert_param(expr) ((expr) ? (void)0U : assert_failed((char *)__FILE__, __LINE__))
/* Exported functions ------------------*/
void assert_failed(char* file, uint32_t line);

typedef signed   char                    s8;      /**<  8bit integer type */
typedef signed   short                   s16;     /**< 16bit integer type */
typedef signed   long                    s32;     /**< 32bit integer type */
typedef unsigned char                    u8;     /**<  8bit unsigned integer type */
typedef unsigned short                   u16;    /**< 16bit unsigned integer type */
typedef unsigned long                    u32;    /**< 32bit unsigned integer type */



#ifdef __cplusplus
}
#endif

#endif

