/*
 * @Description:
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-27 14:27:48
 * @LastEditors: lzc
 * @LastEditTime: 2020-12-11 14:31:45
 */
#include "app_config.h"
#include "my_gpio.h"
#include "my_lcd.h"
#include "my_adc.h"
#include "my_tim.h"
#include "my_eep.h"
#include "my_usart.h"
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_0);

    if (LL_FLASH_GetLatency() != LL_FLASH_LATENCY_0)
    {
        //Error_Handler();
        while (1);
    }
    //LL_RCC_HSE_Enable();
    LL_RCC_HSI_Enable();
    while (LL_RCC_HSI_IsReady() != 1)
    {

    }

    /* Wait till HSE is ready */
    /*
    while(LL_RCC_HSE_IsReady() != 1)
    {

    }
    */
    LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI_DIV_2, LL_RCC_PLL_MUL_4);
    LL_RCC_PLL_Enable();

    /* Wait till PLL is ready */
    while (LL_RCC_PLL_IsReady() != 1)
    {

    }
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

    /* Wait till System clock is ready */
    while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
    {

    }
    // 中断向量表的偏移
    memcpy((unsigned long *)0x20000000, (unsigned long *)MBL_APP_EXEC_ADR, 0xB4);
    LL_SYSCFG_SetRemapMemory(LL_SYSCFG_REMAP_SRAM);
    LL_Init1msTick(16000000);
    LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
    LL_SetSystemCoreClock(16000000);
}


int my_board_init()
{
    my_gpio_init();
    my_lcd_init();
    my_adc_init();
    my_tim_init();
    eeprom_init();
    my_usart_init();
    return 0;
}

INIT_BOARD_EXPORT(my_board_init);
//INIT_COMPONENT_EXPORT(board_app_init);
