/*
 * @Description:
 * @Version: 1.0
 * @Autor: lzc
 * @Date: 2020-11-17 09:54:11
 * @LastEditors: lzc
 * @LastEditTime: 2021-12-15 17:16:32
 */
/**
 * Copyright (c) 2020, 浙江光维通信技术有限公司
 * \file main.c
 * \brief
 * \author  mirlee
 * \version 0.1
 * \date
 */

#include "my_thread.h"
#include "my_record.h"
#include "app_config.h"
#include "my_data.h"
#include "my_usart.h"
#include "my_api.h"
int main()
{
    data_init();
    record_init();
    my_thread_init();
    return 0;
}

/* 延时时间不能超过1 TICK */
void rt_hw_us_delay(rt_uint32_t us)
{
    // NOTE:采用Tick 太慢
    // rt_uint32_t delta;
    // /* 获得延时经过的tick */
    // us = us * ((SysTick->LOAD + 1) / (8000000 / RT_TICK_PER_SECOND));
    // /* 获得当前时间 */
    // delta = SysTick->VAL;
    // /* 循环获得当前时间 */
    // while (delta - SysTick->VAL < us)
    //     ;
    // delta++;
    rt_uint32_t delay = 0;
    delay = 2 * us;
    while (delay--)
    {
        ;
    }
}

void rt_delay_ms(int ms)
{
    int tick = 0;
    int tick_t = 0;
    tick = rt_tick_from_millisecond(ms);
    tick += rt_tick_get(); 
    while (tick_t <= tick)
    {
        tick_t = rt_tick_get();
    }
}

void assert_failed(char *file, uint32_t line)
{
    char buf[10];
    putstr("file:");
    putstr(file);
    putstr("line:");
    my_itoa(line, buf);
    putstr(buf);
}
