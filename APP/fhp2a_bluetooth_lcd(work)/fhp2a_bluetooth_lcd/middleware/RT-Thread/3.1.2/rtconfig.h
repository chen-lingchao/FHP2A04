
/* RT-Thread config file */

#ifndef __RTTHREAD_CFG_H__
#define __RTTHREAD_CFG_H__

#include "RTE_Components.h"

// <<< Use Configuration Wizard in Context Menu >>>
// <h>Basic Configuration
// <o>Maximal level of thread priority <8-256>
//	<i>Default: 32
#define RT_THREAD_PRIORITY_MAX 10

// <o>OS tick per second
//  <i>Default: 1000   (1ms)
#define RT_TICK_PER_SECOND 1000

// <o>Alignment size for CPU architecture data access
//	<i>Default: 4
#define RT_ALIGN_SIZE 4

// <o>the max length of object name<2-16>
//	<i>Default: 8
#define RT_NAME_MAX 8

// <c1>Using RT-Thread components initialization
//  <i>Using RT-Thread components initialization
#define RT_USING_COMPONENTS_INIT
// </c>

// <c1>Using user main
//  <i>Using user main
#define RT_USING_USER_MAIN
// </c>

// <o>the size of main thread<1-4086>
//	<i>Default: 512
#define RT_MAIN_THREAD_STACK_SIZE 512

// <c1>using tiny size of memory
//  <i>using tiny size of memory
#define RT_USING_TINY_SIZE
// </c>
// </h>

// <h>Debug Configuration
// <c1>enable kernel debug configuration
//  <i>Default: enable kernel debug configuration
#define RT_DEBUG
// </c>

// <o>enable components initialization debug configuration<0-1>
//  <i>Default: 0
//#define RT_DEBUG_INIT

// <c1>thread stack over flow detect
//  <i> Diable Thread stack over flow detect
#define RT_USING_OVERFLOW_CHECK
// </c>
// </h>

// <h>Hook Configuration
// <c1>using hook
//  <i>using hook
//#define RT_USING_HOOK
// </c>

// <c1>using idle hook
//  <i>using idle hook
//#define RT_USING_IDLE_HOOK
// </c>
// </h>

// <e>Software timers Configuration
// <i> Enables user timers
//#define RT_USING_TIMER_SOFT

// <o>The priority level of timer thread <0-31>
//  <i>Default: 4
#define RT_TIMER_THREAD_PRIO 0

// <o>The stack size of timer thread <0-8192>
//  <i>Default: 512
#define RT_TIMER_THREAD_STACK_SIZE 512

// <o>The soft-timer tick per second <0-1000>
//  <i>Default: 100
#define RT_TIMER_TICK_PER_SECOND 100
// </e>

// <h>IPC(Inter-process communication) Configuration
// <c1>Using Semaphore
//  <i>Using Semaphore
#define RT_USING_SEMAPHORE
// </c>

// <c1>Using Mutex
//  <i>Using Mutex
#define RT_USING_MUTEX
// </c>

// <c1>Using Event
//  <i>Using Event
//#define RT_USING_EVENT
// </c>

// <c1>Using MailBox
//  <i>Using MailBox
//#define RT_USING_MAILBOX
// </c>

// <c1>Using Message Queue
//  <i>Using Message Queue
//#define RT_USING_MESSAGEQUEUE
// </c>
// </h>

// <h>Memory Management Configuration
// <c1>Using Memory Pool Management
//  <i>Using Memory Pool Management
#define RT_USING_MEMPOOL
// </c>
// <c1>Dynamic Heap Management
//  <i>Dynamic Heap Management
#define RT_USING_HEAP
// </c>
// <c1>using small memory
//  <i>using small memory
#define RT_USING_SMALL_MEM
// </c>
// </h>

// <h>Console Configuration
// <c1>Using console
//  <i>Using console
#define RT_USING_CONSOLE
// </c>

// <o>the buffer size of console <1-1024>
//  <i>the buffer size of console
//  <i>Default: 128  (128Byte)
#define RT_CONSOLEBUF_SIZE 64

// <s>The device name for console
//  <i>The device name for console
//  <i>Default: uart2
#define RT_CONSOLE_DEVICE_NAME "uart1"
// </h>
//#define RTE_USING_DEVICE
#if defined(RTE_USING_DEVICE)
#define RT_USING_DEVICE
#define RT_USING_SERIAL
#define BSP_USING_UART1
#endif
//#define RTE_USING_FINSH
#define FINSH_USING_HISTORY
#if defined(RTE_USING_FINSH)

// <h>Finsh Configuration
// <c1>Using FinSh Shell
//  <i>Using FinSh Shell
//#define RT_USING_FINSH
// </c>

// <c1>Using Msh Shell
//  <i>Using Msh Shell
//#define FINSH_USING_MSH
// </c>

// <c1>Only using Msh Shell
//  <i>Only using Msh Shell
//#define FINSH_USING_MSH_ONLY
// </c>

// <o>the priority of finsh thread <1-7>
//  <i>the priority of finsh thread
//  <i>Default: 6
#define __FINSH_THREAD_PRIORITY     4
#define FINSH_THREAD_PRIORITY       (RT_THREAD_PRIORITY_MAX / 8 * __FINSH_THREAD_PRIORITY + 1)

// <o>the stack of finsh thread <1-4096>
//  <i>the stack of finsh thread
//  <i>Default: 4096  (4096Byte)
#define FINSH_THREAD_STACK_SIZE 4096

// <o>the history lines of finsh thread <1-32>
//  <i>the history lines of finsh thread
//  <i>Default: 5
#define FINSH_HISTORY_LINES	        5

// <o>FINSH_ARG_MAX <1-32>
//  <i>FINSH_ARG_MAX
//  <i>Default: 5
#define FINSH_ARG_MAX	        5
// <c1>Using symbol table in finsh shell
//  <i>Using symbol table in finsh shell
#define FINSH_USING_SYMTAB
// </c>
// </h>

#endif //RTE_USING_FINSH

//#define RTE_USING_ULOG
#if defined(RTE_USING_ULOG)

// <h>ulog Configuration
// <c1>Using ulog 
//  <i>Using ulog
//#define RT_USING_ULOG
// </c>

// <c1>Using ISR LOG
//  <i>Using ISR LOG
//#define ULOG_USING_ISR_LOG
// </c>

// <c1>Output Level Info
//  <i>Output Level Info
//#define ULOG_OUTPUT_LEVEL
// </c>

#endif //RTE_USING_ULOG
// <<< end of configuration section >>>


/* tools packages */

#if defined(RTE_USING_SYSVIEW)
#define PKG_USING_SYSTEMVIEW
#define PKG_SYSVIEW_APP_NAME "RT-Thread Trace"
#define PKG_SYSVIEW_DEVICE_NAME "Cortex-M"
#define PKG_SYSVIEW_TIMESTAMP_FREQ 0
#define PKG_SYSVIEW_CPU_FREQ 0
#define PKG_SYSVIEW_RAM_BASE 0x20000000
#define PKG_SYSVIEW_EVENTID_OFFSET 32
#define PKG_SYSVIEW_USE_CYCCNT_TIMESTAMP
#define PKG_SYSVIEW_SYSDESC0 "I#15=SysTick"
#define PKG_SYSVIEW_SYSDESC1 ""
#define PKG_SYSVIEW_SYSDESC2 ""

/* Segger RTT configuration */

#define PKG_SEGGER_RTT_MAX_NUM_UP_BUFFERS 3
#define PKG_SEGGER_RTT_MAX_NUM_DOWN_BUFFERS 3
#define PKG_SEGGER_RTT_BUFFER_SIZE_UP 1024
#define PKG_SEGGER_RTT_BUFFER_SIZE_DOWN 16
#define PKG_SEGGER_RTT_PRINTF_BUFFER_SIZE 64
#define PKG_SEGGER_RTT_AS_SERIAL_DEVICE
#define PKG_SERIAL_DEVICE_NAME "segger"
#define PKG_SEGGER_RTT_MODE_ENABLE_NO_BLOCK_SKIP
#define PKG_SEGGER_RTT_MAX_INTERRUPT_PRIORITY 0x20
/* end of Segger RTT configuration */

/* SystemView buffer configuration */

#define PKG_SEGGER_SYSVIEW_RTT_BUFFER_SIZE 1024
#define PKG_SEGGER_SYSVIEW_RTT_CHANNEL 1
#define PKG_SEGGER_SYSVIEW_USE_STATIC_BUFFER
/* end of SystemView buffer configuration */

/* SystemView Id configuration */

#define PKG_SEGGER_SYSVIEW_ID_BASE 0x10000000
#define PKG_SEGGER_SYSVIEW_ID_SHIFT 2
/* end of SystemView Id configuration */
#define PKG_USING_SYSTEMVIEW_LATEST_VERSION
/* end of tools packages */
#endif //RTE_USING_SYSVIEW

// <<< end of configuration section >>>
//#define RT_USING_DEVICE
//#define RT_USING_SERIAL
#endif

