/**
* \file boot_drv.h
* \brief 
*	
*	2.2020-01-16,
*   改为通用文件
* \author mirlee
* \version 0.2
* \date 2019-08-02
*/
#ifndef BOOT_DRV_H
#define BOOT_DRV_H

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

//#include "core_cm7.h"
/**
* \brief define
*/

#ifdef   STM32F030
#include "stm32f0xx.h"
#include "stm32f0xx_ll_rcc.h"
#include "stm32f0xx_ll_system.h"
#define   STM32F030x              1
#define   usart2                  1
#endif

#ifdef   STM32F103//大容量
#define   STM32F103x              1
#define   usart1                  1
#endif
#ifdef STM32F429
#define   STM32F429x              1
#define   usart1                  1
#endif
#ifdef NXPRT1052
#define   NXPRT1052x               1
#define   usart1                  1
#endif
#if   (STM32F030x)
#define MBL_FLASH_START       (0x08000000UL)
#define MBL_APP_WR_ADR        (0x08001a00UL)
#define MBL_APP_EXEC_ADR      (0x08001a00UL)
#define MBL_UP_FLAG_ADR       (0x08001600UL)
#define MBL_UP_FLAG_VAL       (0x1234UL)
#define MBL_FLASH_SIZE        (64*1024)
#define MBL_IMAGE_FREE_SIZE   (MBL_FLASH_SIZE+MBL_FLASH_START-MBL_APP_WR_ADR)
#define MBL_FLASH_PAGE_SIZE  1024	
#elif (STM32F103x)
#define MBL_FLASH_START       (0x08000000UL)
#define MBL_APP_WR_ADR        (0x08003000UL)
#define MBL_APP_EXEC_ADR      (0x08003000UL)
#define MBL_UP_FLAG_ADR       (0x08002000UL)
#define MBL_UP_FLAG_VAL       (0x1234UL)
#define MBL_FLASH_SIZE        (512*1024)
#define MBL_IMAGE_FREE_SIZE   (MBL_FLASH_SIZE+MBL_FLASH_START-MBL_APP_WR_ADR)
#define MBL_FLASH_PAGE_SIZE  2048
#elif (STM32F429x)
#define MBL_FLASH_START       (0x08000000UL)
#define MBL_APP_WR_ADR        (0x08008000UL)
#define MBL_APP_EXEC_ADR      (0x08008000UL)
#define MBL_UP_FLAG_ADR       (0x08004000UL)
#define MBL_UP_FLAG_VAL       (0x1234UL)
#define MBL_FLASH_SIZE        (1024*1024)
#define MBL_IMAGE_FREE_SIZE   (MBL_FLASH_SIZE+MBL_FLASH_START-MBL_APP_WR_ADR)
#elif (NXPRT1052x)
#define MBL_FLASH_START       (0x0UL)
#define MBL_APP_WR_ADR 		    (0x102000UL)
#define MBL_APP_EXEC_ADR      (0x60102000UL)
#define MBL_UP_FLAG_ADR       (0x08004000UL)//考虑存在eeprom中
#define MBL_UP_FLAG_VAL       (0x1234UL)
#define MBL_FLASH_SIZE        (1024*1024*8)
#define MBL_IMAGE_FREE_SIZE   (MBL_FLASH_SIZE+MBL_FLASH_START-MBL_APP_WR_ADR)
#define MBL_FLASH_PAGE_SIZE  4096
#endif

/***********************************************/
void mbl_print_str(char * str);
/*下面是需要实现的接口*/
int mbl_recv_byte(unsigned char *c, int timeout);
int mbl_send_byte(char c);
int mbl_flash_erase(unsigned long appAdr, unsigned long size);
int mbl_flash_write_word(unsigned long srcAdr, unsigned long *buf, unsigned int num, unsigned long appAdr);
int mbl_flash_lock_or(unsigned char status); 
/**********************************************************************/
/*error information*/
typedef enum {
	MBL_FLASH_ERASE_ERROR=-19,
	MBL_FLASH_WRITE_ERROR,
	MBL_YMODEM_ABORTED_ERROR,
	MBL_YMODEM_ABORTED_USER,
}MBL_ERROR;

/*         */
#if (STM32F103x)||(STM32F429x)
#define __I     volatile const
#define __IO     volatile
/* following defines should be used for structure members */
#define     __IM     volatile const      /*! Defines 'read only' structure member permissions */
#define     __OM     volatile            /*! Defines 'write only' structure member permissions */
#define     __IOM    volatile            /*! Defines 'read / write' structure member permissions */
#define uint32_t unsigned long
#define uint16_t unsigned short
#define uint8_t unsigned char
#define u32 unsigned long
#define u16 unsigned short
#define u8 unsigned char
#define SET_BIT(REG, BIT)     ((REG) |= (BIT))
#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))
#define READ_BIT(REG, BIT)    ((REG) & (BIT))
#define CLEAR_REG(REG)        ((REG) = (0x0))
#define WRITE_REG(REG, VAL)   ((REG) = (VAL))
#define READ_REG(REG)         ((REG))
#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))
#endif
//返回操作数二进制编码中第一个1的之前的0个数。如果操作数为0，返回32，
//如：操作数为0x1fffffff,返回值为3.
#define __CLZ                             __clz  
#define __RBIT                          __rbit  //Reverse bit order of value
#define POSITION_VAL(VAL)     (__CLZ(__RBIT(VAL))) 
/***************************************************************************************/
#if (STM32F030x)
#define FLASH_FLAG_BSY             FLASH_SR_BSY              /*!< FLASH Busy flag                          */ 
#define FLASH_FLAG_PGERR           FLASH_SR_PGERR            /*!< FLASH Programming error flag             */
#define FLASH_FLAG_WRPERR          FLASH_SR_WRPRTERR         /*!< FLASH Write protected error flag         */
#define FLASH_FLAG_EOP             FLASH_SR_EOP              /*!< FLASH End of Operation flag              */

#define FLASH_FLAG_WRPRTERR            ((uint32_t)0x00000010)  /*!< FLASH Write protected error flag */
#define FLASH_FLAG_OPTERR              ((uint32_t)0x00000001)  /*!< FLASH Option Byte error flag */  

#define FLASH_FLAG_BANK1_BSY                 FLASH_FLAG_BSY       /*!< FLASH BANK1 Busy flag*/
#define FLASH_FLAG_BANK1_EOP                 FLASH_FLAG_EOP       /*!< FLASH BANK1 End of Operation flag */
#define FLASH_FLAG_BANK1_PGERR               FLASH_FLAG_PGERR     /*!< FLASH BANK1 Program error flag */
#define FLASH_FLAG_BANK1_WRPRTERR            FLASH_FLAG_WRPRTERR  /*!< FLASH BANK1 Write protected error flag */  
#elif (STM32F103x)
typedef struct
{
  __IO uint32_t CRL;
  __IO uint32_t CRH;
  __IO uint32_t IDR;
  __IO uint32_t ODR;
  __IO uint32_t BSRR;
  __IO uint32_t BRR;
  __IO uint32_t LCKR;
} GPIO_TypeDef;
typedef struct
{
  __IO uint32_t CR;
  __IO uint32_t CFGR;
  __IO uint32_t CIR;
  __IO uint32_t APB2RSTR;
  __IO uint32_t APB1RSTR;
  __IO uint32_t AHBENR;
  __IO uint32_t APB2ENR;
  __IO uint32_t APB1ENR;
  __IO uint32_t BDCR;
  __IO uint32_t CSR;
} RCC_TypeDef;
typedef struct
{
  __IO uint16_t SR;
  uint16_t  RESERVED0;
  __IO uint16_t DR;
  uint16_t  RESERVED1;
  __IO uint16_t BRR;
  uint16_t  RESERVED2;
  __IO uint16_t CR1;
  uint16_t  RESERVED3;
  __IO uint16_t CR2;
  uint16_t  RESERVED4;
  __IO uint16_t CR3;
  uint16_t  RESERVED5;
  __IO uint16_t GTPR;
  uint16_t  RESERVED6;
} USART_TypeDef;
typedef struct
{
  __IO uint32_t EVCR;
  __IO uint32_t MAPR;
  __IO uint32_t EXTICR[4];
  uint32_t RESERVED0;
  __IO uint32_t MAPR2;  
} AFIO_TypeDef;
typedef struct
{
  __I  uint32_t CPUID;                        /*!< Offset: 0x00  CPU ID Base Register                                  */
  __IO uint32_t ICSR;                         /*!< Offset: 0x04  Interrupt Control State Register                      */
  __IO uint32_t VTOR;                         /*!< Offset: 0x08  Vector Table Offset Register                          */
  __IO uint32_t AIRCR;                        /*!< Offset: 0x0C  Application Interrupt / Reset Control Register        */
  __IO uint32_t SCR;                          /*!< Offset: 0x10  System Control Register                               */
  __IO uint32_t CCR;                          /*!< Offset: 0x14  Configuration Control Register                        */
  __IO uint8_t  SHP[12];                      /*!< Offset: 0x18  System Handlers Priority Registers (4-7, 8-11, 12-15) */
  __IO uint32_t SHCSR;                        /*!< Offset: 0x24  System Handler Control and State Register             */
  __IO uint32_t CFSR;                         /*!< Offset: 0x28  Configurable Fault Status Register                    */
  __IO uint32_t HFSR;                         /*!< Offset: 0x2C  Hard Fault Status Register                            */
  __IO uint32_t DFSR;                         /*!< Offset: 0x30  Debug Fault Status Register                           */
  __IO uint32_t MMFAR;                        /*!< Offset: 0x34  Mem Manage Address Register                           */
  __IO uint32_t BFAR;                         /*!< Offset: 0x38  Bus Fault Address Register                            */
  __IO uint32_t AFSR;                         /*!< Offset: 0x3C  Auxiliary Fault Status Register                       */
  __I  uint32_t PFR[2];                       /*!< Offset: 0x40  Processor Feature Register                            */
  __I  uint32_t DFR;                          /*!< Offset: 0x48  Debug Feature Register                                */
  __I  uint32_t ADR;                          /*!< Offset: 0x4C  Auxiliary Feature Register                            */
  __I  uint32_t MMFR[4];                      /*!< Offset: 0x50  Memory Model Feature Register                         */
  __I  uint32_t ISAR[5];                      /*!< Offset: 0x60  ISA Feature Register                                  */
} SCB_Type; 
typedef struct
{
  __IO uint32_t ACR;
  __IO uint32_t KEYR;
  __IO uint32_t OPTKEYR;
  __IO uint32_t SR;
  __IO uint32_t CR;
  __IO uint32_t AR;
  __IO uint32_t RESERVED;
  __IO uint32_t OBR;
  __IO uint32_t WRPR;
} FLASH_TypeDef;

/*stm32f10xx*/
#define PERIPH_BASE           ((uint32_t)0x40000000) /*!< Peripheral base address in the alias region */
#define APB1PERIPH_BASE       PERIPH_BASE
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)

#define RCC_APB2ENR_AFIOEN                   (0x1UL << 0U)  /*!< 0x00000001 *//*!< Alternate Function I/O clock enable */
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)

#define RCC_CFGR_PPRE1                       (0x7UL << 8U)                /*!< PRE1[2:0] bits (APB1 prescaler) */
#define RCC_CFGR_PPRE2                       (0x7UL << 11U)                 /*!< PRE2[2:0] bits (APB2 prescaler) */

#define AFIO_BASE             (APB2PERIPH_BASE + 0x00000000UL)
#define AFIO_MAPR_SWJ_CFG                    (0x7UL << 24U)            /*!< SWJ_CFG[2:0] bits (Serial Wire JTAG configuration) */
#define AFIO_MAPR_SWJ_CFG_JTAGDISABLE        (0x1UL <<25U) /*!< JTAG-DP Disabled and SW-DP Enabled */
#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800)
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOC_BASE            (APB2PERIPH_BASE + 0x1000)
#define GPIOD_BASE            (APB2PERIPH_BASE + 0x1400)
#define GPIOE_BASE            (APB2PERIPH_BASE + 0x1800)
#define GPIOF_BASE            (APB2PERIPH_BASE + 0x1C00)
#define GPIOG_BASE            (APB2PERIPH_BASE + 0x2000)
#define USART1_BASE           (APB2PERIPH_BASE + 0x3800)

#define FLASH_R_BASE          (AHBPERIPH_BASE + 0x00002000UL) /*!< Flash registers base address */
#define FLASH_ACR_LATENCY                   (0x7UL)              /*!< LATENCY[2:0] bits (Latency) */
#define FLASH_CR_LOCK                       (0x1U<<7U)                /*!< Lock */
#define FLASH_CR_PER                        (0x1U<<1U)                  /*!< Page Erase */
#define FLASH_CR_STRT                       (0x1U<<6U)                 /*!< Start */
#define FLASH_CR_PG                         (0x1U)                    /*!< Programming */
#define FLASH_OBR_OPTERR                    (0x1U)               /*!< Option Byte Error */

#define  FLASH_SR_BSY                        ((uint8_t)0x01)               /*!< Busy */
#define  FLASH_SR_PGERR                      ((uint8_t)0x04)               /*!< Programming Error */
#define  FLASH_SR_WRPRTERR                   ((uint8_t)0x10)               /*!< Write Protection Error */
#define  FLASH_SR_EOP                        ((uint8_t)0x20)               /*!< End of operation */

#define FLASH_FLAG_BSY             FLASH_SR_BSY              /*!< FLASH Busy flag                          */ 
#define FLASH_FLAG_PGERR           FLASH_SR_PGERR            /*!< FLASH Programming error flag             */
#define FLASH_FLAG_WRPERR          FLASH_SR_WRPRTERR         /*!< FLASH Write protected error flag         */
#define FLASH_FLAG_EOP             FLASH_SR_EOP              /*!< FLASH End of Operation flag              */

#define FLASH_FLAG_WRPRTERR            ((uint32_t)0x00000010)  /*!< FLASH Write protected error flag */
#define FLASH_FLAG_OPTERR              ((uint32_t)0x00000001)  /*!< FLASH Option Byte error flag */  

#define FLASH_FLAG_BANK1_BSY                 FLASH_FLAG_BSY       /*!< FLASH BANK1 Busy flag*/
#define FLASH_FLAG_BANK1_EOP                 FLASH_FLAG_EOP       /*!< FLASH BANK1 End of Operation flag */
#define FLASH_FLAG_BANK1_PGERR               FLASH_FLAG_PGERR     /*!< FLASH BANK1 Program error flag */
#define FLASH_FLAG_BANK1_WRPRTERR            FLASH_FLAG_WRPRTERR  /*!< FLASH BANK1 Write protected error flag */  
#elif (STM32F429x)
typedef struct
{
  __IO uint32_t MODER;    /*!< GPIO port mode register,               Address offset: 0x00      */
  __IO uint32_t OTYPER;   /*!< GPIO port output type register,        Address offset: 0x04      */
  __IO uint32_t OSPEEDR;  /*!< GPIO port output speed register,       Address offset: 0x08      */
  __IO uint32_t PUPDR;    /*!< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
  __IO uint32_t IDR;      /*!< GPIO port input data register,         Address offset: 0x10      */
  __IO uint32_t ODR;      /*!< GPIO port output data register,        Address offset: 0x14      */
  __IO uint32_t BSRR;     /*!< GPIO port bit set/reset register,      Address offset: 0x18      */
  __IO uint32_t LCKR;     /*!< GPIO port configuration lock register, Address offset: 0x1C      */
  __IO uint32_t AFR[2];   /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */
} GPIO_TypeDef;

typedef struct
{
  __IO uint32_t CR;            /*!< RCC clock control register,                                  Address offset: 0x00 */
  __IO uint32_t PLLCFGR;       /*!< RCC PLL configuration register,                              Address offset: 0x04 */
  __IO uint32_t CFGR;          /*!< RCC clock configuration register,                            Address offset: 0x08 */
  __IO uint32_t CIR;           /*!< RCC clock interrupt register,                                Address offset: 0x0C */
  __IO uint32_t AHB1RSTR;      /*!< RCC AHB1 peripheral reset register,                          Address offset: 0x10 */
  __IO uint32_t AHB2RSTR;      /*!< RCC AHB2 peripheral reset register,                          Address offset: 0x14 */
  __IO uint32_t AHB3RSTR;      /*!< RCC AHB3 peripheral reset register,                          Address offset: 0x18 */
  uint32_t      RESERVED0;     /*!< Reserved, 0x1C                                                                    */
  __IO uint32_t APB1RSTR;      /*!< RCC APB1 peripheral reset register,                          Address offset: 0x20 */
  __IO uint32_t APB2RSTR;      /*!< RCC APB2 peripheral reset register,                          Address offset: 0x24 */
  uint32_t      RESERVED1[2];  /*!< Reserved, 0x28-0x2C                                                               */
  __IO uint32_t AHB1ENR;       /*!< RCC AHB1 peripheral clock register,                          Address offset: 0x30 */
  __IO uint32_t AHB2ENR;       /*!< RCC AHB2 peripheral clock register,                          Address offset: 0x34 */
  __IO uint32_t AHB3ENR;       /*!< RCC AHB3 peripheral clock register,                          Address offset: 0x38 */
  uint32_t      RESERVED2;     /*!< Reserved, 0x3C                                                                    */
  __IO uint32_t APB1ENR;       /*!< RCC APB1 peripheral clock enable register,                   Address offset: 0x40 */
  __IO uint32_t APB2ENR;       /*!< RCC APB2 peripheral clock enable register,                   Address offset: 0x44 */
  uint32_t      RESERVED3[2];  /*!< Reserved, 0x48-0x4C                                                               */
  __IO uint32_t AHB1LPENR;     /*!< RCC AHB1 peripheral clock enable in low power mode register, Address offset: 0x50 */
  __IO uint32_t AHB2LPENR;     /*!< RCC AHB2 peripheral clock enable in low power mode register, Address offset: 0x54 */
  __IO uint32_t AHB3LPENR;     /*!< RCC AHB3 peripheral clock enable in low power mode register, Address offset: 0x58 */
  uint32_t      RESERVED4;     /*!< Reserved, 0x5C                                                                    */
  __IO uint32_t APB1LPENR;     /*!< RCC APB1 peripheral clock enable in low power mode register, Address offset: 0x60 */
  __IO uint32_t APB2LPENR;     /*!< RCC APB2 peripheral clock enable in low power mode register, Address offset: 0x64 */
  uint32_t      RESERVED5[2];  /*!< Reserved, 0x68-0x6C                                                               */
  __IO uint32_t BDCR;          /*!< RCC Backup domain control register,                          Address offset: 0x70 */
  __IO uint32_t CSR;           /*!< RCC clock control & status register,                         Address offset: 0x74 */
  uint32_t      RESERVED6[2];  /*!< Reserved, 0x78-0x7C                                                               */
  __IO uint32_t SSCGR;         /*!< RCC spread spectrum clock generation register,               Address offset: 0x80 */
  __IO uint32_t PLLI2SCFGR;    /*!< RCC PLLI2S configuration register,                           Address offset: 0x84 */
  __IO uint32_t PLLSAICFGR;    /*!< RCC PLLSAI configuration register,                           Address offset: 0x88 */
  __IO uint32_t DCKCFGR;       /*!< RCC Dedicated Clocks configuration register,                 Address offset: 0x8C */
} RCC_TypeDef;

typedef struct
{
  __IO uint32_t SR;         /*!< USART Status register,                   Address offset: 0x00 */
  __IO uint32_t DR;         /*!< USART Data register,                     Address offset: 0x04 */
  __IO uint32_t BRR;        /*!< USART Baud rate register,                Address offset: 0x08 */
  __IO uint32_t CR1;        /*!< USART Control register 1,                Address offset: 0x0C */
  __IO uint32_t CR2;        /*!< USART Control register 2,                Address offset: 0x10 */
  __IO uint32_t CR3;        /*!< USART Control register 3,                Address offset: 0x14 */
  __IO uint32_t GTPR;       /*!< USART Guard time and prescaler register, Address offset: 0x18 */
} USART_TypeDef;
typedef struct
{
  __IM  uint32_t CPUID;                  /*!< Offset: 0x000 (R/ )  CPUID Base Register */
  __IOM uint32_t ICSR;                   /*!< Offset: 0x004 (R/W)  Interrupt Control and State Register */
  __IOM uint32_t VTOR;                   /*!< Offset: 0x008 (R/W)  Vector Table Offset Register */
  __IOM uint32_t AIRCR;                  /*!< Offset: 0x00C (R/W)  Application Interrupt and Reset Control Register */
  __IOM uint32_t SCR;                    /*!< Offset: 0x010 (R/W)  System Control Register */
  __IOM uint32_t CCR;                    /*!< Offset: 0x014 (R/W)  Configuration Control Register */
  __IOM uint8_t  SHP[12U];               /*!< Offset: 0x018 (R/W)  System Handlers Priority Registers (4-7, 8-11, 12-15) */
  __IOM uint32_t SHCSR;                  /*!< Offset: 0x024 (R/W)  System Handler Control and State Register */
  __IOM uint32_t CFSR;                   /*!< Offset: 0x028 (R/W)  Configurable Fault Status Register */
  __IOM uint32_t HFSR;                   /*!< Offset: 0x02C (R/W)  HardFault Status Register */
  __IOM uint32_t DFSR;                   /*!< Offset: 0x030 (R/W)  Debug Fault Status Register */
  __IOM uint32_t MMFAR;                  /*!< Offset: 0x034 (R/W)  MemManage Fault Address Register */
  __IOM uint32_t BFAR;                   /*!< Offset: 0x038 (R/W)  BusFault Address Register */
  __IOM uint32_t AFSR;                   /*!< Offset: 0x03C (R/W)  Auxiliary Fault Status Register */
  __IM  uint32_t PFR[2U];                /*!< Offset: 0x040 (R/ )  Processor Feature Register */
  __IM  uint32_t DFR;                    /*!< Offset: 0x048 (R/ )  Debug Feature Register */
  __IM  uint32_t ADR;                    /*!< Offset: 0x04C (R/ )  Auxiliary Feature Register */
  __IM  uint32_t MMFR[4U];               /*!< Offset: 0x050 (R/ )  Memory Model Feature Register */
  __IM  uint32_t ISAR[5U];               /*!< Offset: 0x060 (R/ )  Instruction Set Attributes Register */
        uint32_t RESERVED0[5U];
  __IOM uint32_t CPACR;                  /*!< Offset: 0x088 (R/W)  Coprocessor Access Control Register */
} SCB_Type;

typedef struct
{
  __IO uint32_t ACR;      /*!< FLASH access control register,   Address offset: 0x00 */
  __IO uint32_t KEYR;     /*!< FLASH key register,              Address offset: 0x04 */
  __IO uint32_t OPTKEYR;  /*!< FLASH option key register,       Address offset: 0x08 */
  __IO uint32_t SR;       /*!< FLASH status register,           Address offset: 0x0C */
  __IO uint32_t CR;       /*!< FLASH control register,          Address offset: 0x10 */
  __IO uint32_t OPTCR;    /*!< FLASH option control register ,  Address offset: 0x14 */
  __IO uint32_t OPTCR1;   /*!< FLASH option control register 1, Address offset: 0x18 */
} FLASH_TypeDef;

/** 
  * @brief Power Control
  */

typedef struct
{
  __IO uint32_t CR;   /*!< PWR power control register,        Address offset: 0x00 */
  __IO uint32_t CSR;  /*!< PWR power control/status register, Address offset: 0x04 */
} PWR_TypeDef;
#define PERIPH_BASE           0x40000000UL //Peripheral base address in the alias region  
#define APB1PERIPH_BASE       PERIPH_BASE
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x00010000UL)
#define AHB1PERIPH_BASE       (PERIPH_BASE + 0x00020000UL)
#define AHB2PERIPH_BASE       (PERIPH_BASE + 0x10000000UL)

#define RCC_BASE               (0x40023800UL)  //AHB1
#define USART1_BASE            (0x40011000UL)  //APB2
#define GPIOA_BASE             (0x40020000UL)  //AHB1
#define AHBPERIPH_BASE        (0x40000000 + 0x20000)
#define PWR_BASE              (0x40000000UL + 0x7000UL)
#define PWR                   ((PWR_TypeDef *) PWR_BASE)
                                           
#define PWR_CR_VOS             (0x3UL << 14U)                        /*!< 0x0000C000 */
#define PWR_CR_VOS_0           0x00004000U                                     /*!< Bit 0 */
#define PWR_CR_VOS_1           0x00008000U                                     /*!< Bit 1 */                                       
#define PWR_CR_ODEN        (0x1UL << 16U)                       /*!< 0x00010000 */

#define RCC_CFGR_PPRE1                       (0x7UL << 10U)                /*!< PRE1[2:0] bits (APB1 prescaler) */
#define RCC_CFGR_PPRE2                       (0x7UL << 13U)                 /*!< PRE2[2:0] bits (APB2 prescaler) */ 

#define FLASH_R_BASE          (AHB1PERIPH_BASE + 0x3C00UL)
#define FLASH_ACR_LATENCY                           0xFUL   

#define FLASH_CR_PG                    0x00000001   //Flash programming activated.                      
#define FLASH_CR_SER                   0x00000002   //Sector Erase activated.                                        
#define FLASH_CR_MER1                  0x00000004   //Erase activated of bank 1 sectors.
#define FLASH_CR_SNB                   0x000000F8                        
#define FLASH_CR_SNB_0                 0x00000008             /*!< 0x00000008 */
#define FLASH_CR_SNB_1                 0x00000010             /*!< 0x00000010 */
#define FLASH_CR_SNB_2                 0x00000020             /*!< 0x00000020 */
#define FLASH_CR_SNB_3                 0x00000040             /*!< 0x00000040 */
#define FLASH_CR_SNB_4                 0x00000080             /*!< 0x00000080 */
#define FLASH_CR_PSIZE                 0x00000300                      
#define FLASH_CR_PSIZE_0               0x00000100            /*!< 0x00000100 */
#define FLASH_CR_PSIZE_1               0x00000200            /*!< 0x00000200 */
#define FLASH_CR_MER2                  0x00008000  ////Erase activated of bank 2 sectors.                   
#define FLASH_CR_STRT                  0x00010000  // Start                     
#define FLASH_CR_EOPIE                 0x01000000                      
#define FLASH_CR_LOCK                  0x80000000    

#define FLASH_SR_EOP                   0x00000001UL //End of operation                     
#define FLASH_SR_SOP                   0x00000002UL //Operation error                     
#define FLASH_SR_WRPERR                0x00000010UL //Write protection error                  
#define FLASH_SR_PGAERR                0x00000020UL //Programming alignment error                     
#define FLASH_SR_PGPERR                0x00000040UL //Programming parallelism error                   
#define FLASH_SR_PGSERR                0x00000080UL // Programming sequence error                   
#define FLASH_SR_RDERR                 0x00000100UL // Proprietary readout protection (PCROP) error                   
#define FLASH_SR_BSY                   0x00010000UL //Busy   

#define FLASH_FLAG_BSY             FLASH_SR_BSY              /*!< FLASH Busy flag                          */ 
#define FLASH_FLAG_PGERR           (FLASH_SR_PGAERR|FLASH_SR_PGPERR|FLASH_SR_PGSERR)/*!< FLASH Programming error flag  */
#define FLASH_FLAG_WRPERR          FLASH_SR_WRPERR         /*!< FLASH Write protected error flag         */
#define FLASH_FLAG_EOP             FLASH_SR_EOP              /*!< FLASH End of Operation flag              */  

#define FLASH_FLAG_BANK1_BSY                 FLASH_FLAG_BSY       /*!< FLASH BANK1 Busy flag*/
#define FLASH_FLAG_BANK1_EOP                 FLASH_FLAG_EOP       /*!< FLASH BANK1 End of Operation flag */
#define FLASH_FLAG_BANK1_PGERR               FLASH_FLAG_PGERR     /*!< FLASH BANK1 Program error flag */
#define FLASH_FLAG_BANK1_WRPRTERR            FLASH_FLAG_WRPERR    /*!< FLASH BANK1 Write protected error flag */          
#else
//#error err
#endif

/////////////////////////////
/*cm3,cm4*/
#if (STM32F103x || STM32F429x ||  STM32F030x)
#if (STM32F103x || STM32F429x)
#define SCS_BASE            (0xE000E000)                              /*!< System Control Space Base Address */
#define SCB_BASE            (SCS_BASE +  0x0D00)                      /*!< System Control Block Base Address */
#define SCB                 ((SCB_Type *)SCB_BASE)         /*!< SCB configuration struct          */
#define RCC_CR_HSITRIM                       (0x1FUL << 3U)                 /*!< Internal High Speed clock trimming */
#define RCC_CR_HSION                         (0x1UL << 0U)                  /*!< Internal High Speed clock enable */
#define RCC_CR_HSIRDY                        (0x1UL << 1U)                 /*!< Internal High Speed clock ready flag */
#define RCC_CFGR_HPRE                        (0xFUL << 4U)                 /*!< HPRE[3:0] bits (AHB prescaler) */
#define RCC_CFGR_SW                          (0x3UL << 0U)                    /*!< SW[1:0] bits (System clock Switch) */
#define RCC_CFGR_SWS                         (0x3UL << 2U)                  /*!< SWS[1:0] bits (System Clock Switch Status) */


#define FLASH_KEY1                          (0x45670123U)                     /*!< FPEC Key1 */
#define FLASH_KEY2                          (0xCDEF89ABU)                     /*!< FPEC Key2 */
#define OBR_REG_INDEX            						((uint32_t)1)

#define FLASH_FLAG_OPTVERR         ((OBR_REG_INDEX << 8 | FLASH_OBR_OPTERR)) /*!< Option Byte Error        */
#define FLASH_GET_FLAG(__FLAG__)        (((__FLAG__) == FLASH_FLAG_OPTVERR) ? \
                                            (FLASH->OBR & FLASH_OBR_OPTERR) : \
                                            (FLASH->SR & (__FLAG__)))
#endif
//************************************************************************************************
#if (usart1)

#define USARTxx                ((USART_TypeDef *) USART1_BASE)
#define GPIOxx                 ((GPIO_TypeDef *)   GPIOA_BASE)
#define RCCxx                  ((RCC_TypeDef *)      RCC_BASE)
#define AFIOxx                 ((AFIO_TypeDef *)    AFIO_BASE)
#ifndef STM32F030
#define FLASH                  ((FLASH_TypeDef *)FLASH_R_BASE)
#endif

#elif (usart2) 

#define USARTxx                ((USART_TypeDef *) USART2_BASE)
#define GPIOxx                 ((GPIO_TypeDef *)   GPIOA_BASE)
#define RCCxx                  ((RCC_TypeDef *)      RCC_BASE)
#define AFIOxx                 ((AFIO_TypeDef *)    AFIO_BASE)
#ifndef STM32F030
#define FLASH                  ((FLASH_TypeDef *)FLASH_R_BASE)
#endif

#endif


#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
/*end of file*/
