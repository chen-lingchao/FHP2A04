#include "fsl_lpuart.h"
#include "fsl_common.h"
#include "fsl_iomuxc.h"
#include "boot_drv.h"
 	 

static lpuart_config_t lpuart1_config;

int mbl_send_byte(char ch)
{ 	
	while((LPUART1->STAT&LPUART_STAT_TDRE_MASK)==0);
	LPUART1->DATA=(unsigned char)ch;	
	return 0;
}

int mbl_recv_byte(unsigned char *c, int timeout)
{
	int t;
	t=timeout*500;
	//RXNE
	while(t--)
	{
		if((LPUART1->STAT)&kLPUART_RxDataRegFullFlag)
		{
			*c=(unsigned char)LPUART1->DATA;//读取数据
			return 0;
		}
	}
	return -1;				
}


//获取LPUART的时钟源频率，我们前面设置的是80MHz
//返回值：LPUART时钟源频率,根据我们的只是一般是80Mhz
unsigned int LPUART_SrcFreqGet(void)
{
	uint32_t freq;

	if(CLOCK_GetMux(kCLOCK_UartMux)==0) 	//LPUART的时钟源选择PLL3/6
	{
			freq=(CLOCK_GetPllFreq(kCLOCK_PllUsb1)/6U)/(CLOCK_GetDiv(kCLOCK_UartDiv)+1U);
	}
	else									//LPUART的时钟源选择OSC
	{
			freq=CLOCK_GetOscFreq()/(CLOCK_GetDiv(kCLOCK_UartDiv)+1U);
	}
	return freq;
}

//初始化IO 串口1 
//bound:波特率
void LPUART1_Init(unsigned int bound)
{	
	unsigned int freq=0;							//串口的时钟源频率
	
	CLOCK_EnableClock(kCLOCK_Lpuart1);	//使能LPUART1时钟
	
	CLOCK_SetMux(kCLOCK_UartMux,0); 	//设置UART时钟源为PLL3 80Mhz，PLL3/6=480/6=80MHz
  CLOCK_SetDiv(kCLOCK_UartDiv,0); 	//设置UART时钟1分频，即UART时钟为80Mhz
	
	//LPUART1所使用的IO功能配置，即：从ALT0~ALT7选择合适的功能。
	IOMUXC_SetPinMux(IOMUXC_GPIO_AD_B0_12_LPUART1_TX,0U);	//GPIO_AD_B0_12设置为LPUART1_TX
	IOMUXC_SetPinMux(IOMUXC_GPIO_AD_B0_13_LPUART1_RX,0U);	//GPIO_AD_B0_13设置为LPUART1_RX

	//配置IO引脚GPIO_AD_B0_12和GPIO_AD_B0_13的功能
	//低转换速度,驱动能力为R0/6,速度为100Mhz，关闭开路功能，使能pull/keepr
	//选择keeper功能，下拉100K Ohm，关闭Hyst
	IOMUXC_SetPinConfig(IOMUXC_GPIO_AD_B0_12_LPUART1_TX,0x10B0u); 
	IOMUXC_SetPinConfig(IOMUXC_GPIO_AD_B0_13_LPUART1_RX,0x10B0u); 

	freq=LPUART_SrcFreqGet();	
	
	LPUART_GetDefaultConfig(&lpuart1_config); 				  //先设置为默认配置，后面在根据实际情况配置
	
	//初始化NXP官方提供的debug console，此函数会重新初始化LPUART1，但是我们后面会
	//重新显示的初始化一次LPUART1，DbgConsole_Init()主要是给那些想要使用NXP官方
	//调试功能的开发者使用的，不需要使用的话就可以将下面代码注释掉
	//DbgConsole_Init(BOARD_DEBUG_UART_BASEADDR,bound,BOARD_DEBUG_UART_TYPE,freq);
	
	lpuart1_config.baudRate_Bps=bound;									//波特率
	lpuart1_config.dataBitsCount=kLPUART_EightDataBits;	//8位
	lpuart1_config.stopBitCount=kLPUART_OneStopBit;			//1位停止位
	lpuart1_config.parityMode=kLPUART_ParityDisabled;		//无奇偶校验
	lpuart1_config.enableRx=true;												//使能接收
	lpuart1_config.enableTx=true;												//使能发送
	LPUART_Init(LPUART1,&lpuart1_config,freq);				  //初始化LPUART1 
}	











 




