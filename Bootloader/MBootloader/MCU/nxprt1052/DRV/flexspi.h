#ifndef _FLEXSPI_H
#define _FLEXSPI_H
#include "boot_drv.h"

#define FLEXSPI_FLASH_SIZE      0x2000              //Flash大小为8MB
#define FLASH_PAGE_SIZE         256                 //编程页大小：256字节
#define SECTOR_SIZE             0x1000              //扇区大小4096
#define FLEXSPI_FLASHID         0X16                //Flash ID

//命令序列
#define NOR_CMD_LUT_SEQ_IDX_READ_NORMAL 0
#define NOR_CMD_LUT_SEQ_IDX_READ_FAST 1
#define NOR_CMD_LUT_SEQ_IDX_READ_FAST_QUAD 2
#define NOR_CMD_LUT_SEQ_IDX_READSTATUS 3
#define NOR_CMD_LUT_SEQ_IDX_WRITEENABLE 4
#define NOR_CMD_LUT_SEQ_IDX_ERASESECTOR 5
#define NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_SINGLE 6
#define NOR_CMD_LUT_SEQ_IDX_PAGEPROGRAM_QUAD 7
#define NOR_CMD_LUT_SEQ_IDX_READID 8
#define NOR_CMD_LUT_SEQ_IDX_WRITESTATUSREG1 9
#define NOR_CMD_LUT_SEQ_IDX_WRITESTATUSREG2 10
#define NOR_CMD_LUT_SEQ_IDX_EXITQPI 11
#define NOR_CMD_LUT_SEQ_IDX_READSTATUSREG1 12
#define NOR_CMD_LUT_SEQ_IDX_READMANUID 13
#define NOR_CMD_LUT_SEQ_IDX_READSTATUSREG2 14

#define CUSTOM_LUT_LENGTH 60

void FlexSPI_FlashInit(void);
void FlexSPI_FlashWrite_Enable(void);
unsigned char FlexSPI_FlashRead_SR(unsigned char regno);
void FlexSPI_FlashWrite_SR(unsigned char regno,unsigned char regdata);
void FlexSPI_FlashWait_Busy(void);
void FlexSPI_QPI_Enable(void);
unsigned char FlexSPI_FlashID_Get(void);
unsigned char FlexSPI_FlashErase_Sector(unsigned int sectorno);
unsigned char FlexSPI_FlashPage_Write(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite);
void FlexSPI_FlashRead(unsigned char* pBuffer,unsigned int ReadAddr,unsigned short NumByteToRead);
void FlexSPI_Write_NoCheck(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite);
void FlexSPI_FlashWrite(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite);
void FlexSPI_Sector_Write(unsigned char* pBuffer,unsigned int setcorno);

unsigned char FlexSPI_FlashPage_Write_Word(unsigned int* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite);

#endif
