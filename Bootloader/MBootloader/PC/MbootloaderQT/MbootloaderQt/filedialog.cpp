#include "filedialog.h"
#include "ui_filedialog.h"
#include "ymodem.h"

FileDialog::FileDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileDialog)
{
    ui->setupUi(this);
    ui->start_upg->setEnabled(true);
    ui->open_file->setEnabled(true);
    connect(ui->start_upg, SIGNAL(clicked()), SLOT(start_upgrade_button()));
    connect(ui->open_file, SIGNAL(clicked()), SLOT(open_file_button()));
}

FileDialog::~FileDialog()
{
    delete ui;
}

QByteArray file_buf;
QByteArray file_name;
void FileDialog::start_upgrade_button()
{
  qDebug("start");

  ymodem_transmit((unsigned char *)file_buf.data(), (unsigned char *)file_name.data(), file_buf.size());
  qDebug("end");
}

void FileDialog::open_file_button()
{
  QString filePath = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "/home",
                                                    tr("BIN (*.bin)"));
  QFileInfo fileInfo = QFileInfo(filePath);
  QFile file(filePath);
  file_name = fileInfo.fileName().toLatin1();
  file.open(QIODevice::ReadOnly | QIODevice::ExistingOnly);
  file_buf.resize(file.size());
  file_buf = file.readAll();
  qDebug(filePath.toLatin1());
}
