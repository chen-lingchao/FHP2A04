#include "mainwindow.h"

QSerialPort *m_serial = nullptr;
static QByteArray s_data_buf;

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow),
  m_status(new QLabel),
  m_settings(new SettingsDialog),
  m_file(new FileDialog)
{
  m_serial = new QSerialPort;
  s_data_buf.resize(2048);
  s_data_buf.clear();
  ui->setupUi(this);
  ui->actionConnect->setEnabled(true);
  ui->actionDisconnect->setEnabled(false);
  ui->actionSetting->setEnabled(true);

  init_actions_connections();
//  connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::readData);
  connect(ui->upgradeButton, SIGNAL(clicked()), this, SLOT(upgrade_button()));
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::upgrade_button()
{
  if (m_serial->isOpen() == false)
  {
    QMessageBox::critical(this, tr("Error"), "串口未打开");
    return;
  }
  qDebug("upgrade");
//  m_serial->write("*ver\n");
  m_file->show();
}


void MainWindow::init_actions_connections()
{
  connect(ui->actionConnect, &QAction::triggered, this, &MainWindow::openSerialPort);
  connect(ui->actionDisconnect, &QAction::triggered, this, &MainWindow::closeSerialPort);
  connect(ui->actionSetting, &QAction::triggered, m_settings, &SettingsDialog::show);
}

void MainWindow::openSerialPort()
{
  const SettingsDialog::Settings p = m_settings->settings();
  m_serial->setPortName(p.name);
  m_serial->setBaudRate(p.baudRate);
  m_serial->setDataBits(p.dataBits);
  m_serial->setParity(p.parity);
  m_serial->setStopBits(p.stopBits);
  m_serial->setFlowControl(p.flowControl);
  if (m_serial->open(QIODevice::ReadWrite)) {
    ui->actionConnect->setEnabled(false);
    ui->actionDisconnect->setEnabled(true);
    ui->actionSetting->setEnabled(false);
    showStatusMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                      .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                      .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));
  } else {
    QMessageBox::critical(this, tr("Error"), m_serial->errorString());

    showStatusMessage(tr("Open error"));
  }
}

void MainWindow::closeSerialPort()
{
  if (m_serial->isOpen())
      m_serial->close();

  ui->actionConnect->setEnabled(true);
  ui->actionDisconnect->setEnabled(false);
  ui->actionSetting->setEnabled(true);
  showStatusMessage(tr("Disconnected"));
}

void MainWindow::showStatusMessage(const QString &message)
{
  m_status->setText(message);
}

void MainWindow::writeData(const QByteArray &data)
{
  m_serial->write(data);
}

void MainWindow::readData()
{
//  s_data_buf += m_serial->readAll();
  qDebug(s_data_buf);
}

int mbl_recv_byte(unsigned char *data, int timeout)
{
  if(m_serial->waitForReadyRead(timeout) == true)
  {
    m_serial->read((char *)data, 1);
    return 0;
  }
  return -1;
}

int mbl_send_byte(unsigned char data)
{
  if(m_serial->write((const char*)&data, 1) != 1)
  {
    if(m_serial->waitForBytesWritten() == true)
    {
      m_serial->write((const char*)&data, 1);
      return 0;
    }
    return -1;
  }
  return 0;
}

