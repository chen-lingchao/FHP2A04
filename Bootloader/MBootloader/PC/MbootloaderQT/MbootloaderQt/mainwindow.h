#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QSerialPort>
#include <QDebug>
#include <QMessageBox>
#include "ui_mainwindow.h"
#include "filedialog.h"
#include "settingsdialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void openSerialPort();
    void closeSerialPort();
    void writeData(const QByteArray &data);
    void readData();
    void upgrade_button();
private:
    Ui::MainWindow *ui;
    void init_actions_connections();
    void showStatusMessage(const QString &message);
    QLabel *m_status = nullptr;
    SettingsDialog *m_settings = nullptr;
    FileDialog *m_file = nullptr;

};
int mbl_recv_byte(unsigned char *data, int timeout);
int mbl_send_byte(unsigned char data);
#endif // MAINWINDOW_H
