#ifndef FILEDIALOG_H
#define FILEDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QSerialPort>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>

QT_BEGIN_NAMESPACE
namespace Ui {
class FileDialog;
}
QT_END_NAMESPACE

class FileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FileDialog(QWidget *parent = nullptr);
    ~FileDialog();

private:
    Ui::FileDialog *ui;

private slots:
    void start_upgrade_button();
    void open_file_button();
};

#endif // FILEDIALOG_H
